#ifndef _KRPC_RPC_RESPONSE_H
#define _KRPC_RPC_RESPONSE_H

struct rpc_response {
	int status;
	char *message;
	struct list *lines;
};

struct rpc_response *rpc_response_parse(char const *str);
void rpc_response_free(struct rpc_response *r);

#endif /* ndef _KRPC_RPC_RESPONSE_H */
