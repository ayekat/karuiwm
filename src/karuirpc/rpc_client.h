#ifndef _KRPC_RPC_CLIENT_H
#define _KRPC_RPC_CLIENT_H

struct rpc_client {
	int fd;
};

#include <karuirpc/rpc_response.h>

struct rpc_client *rpc_client_alloc(void);
void rpc_client_free(struct rpc_client *c);

int rpc_client_recv(struct rpc_client *c, struct rpc_response **response);
int rpc_client_send(struct rpc_client *c, char const *cmd);

#endif /* ndef _KPRC_RPC_CLIENT_H */
