#include <modules/statusbar/statusbar.h>
#include <modules/statusbar/mod_statusbar.h>
#include <karuiwm/bus.h>
#include <karuiwm/icon.h>
#include <karuiwm/utils.h>
#include <karuiwm/xfont.h>
#include <karuiwm/xoutput.h>
#include <karuiwm/xserver.h>
#include <karui/cstr.h>
#include <karui/log.h>
#include <karui/memory.h>
#include <string.h>

static void _bus_event(struct bus_subscription const *sub, void *data,
                       void *ctx);
static int _init_bus(struct statusbar *bar);

struct statusbar *
statusbar_alloc(struct monitor *mon)
{
	struct statusbar *bar;
	int long unsigned event_mask;

	bar = memory_alloc(sizeof(struct statusbar), "statusbar structure");
	if (!bar)
		goto error_alloc;

	bar->dim = (struct rectangle) {
		.pos = mon->dim.pos,
		.width = mon->dim.width,
		.height = statusbar.xfont->height + 2,
	};
	bar->buffer[0] = '\0';
	bar->monitor = mon;
	bar->workspace = bar->monitor->workspace;
	bar->focused = false;

	/* reserve space at top (TODO: use WM_STRUT instead) */
	bar->monitor->margins.top = bar->dim.height;
	if (monitor_arrange(mon) < 0)
		ERROR("Could not arrange monitor after setting top margin for status bar: %s",
		      log_str());

	/* window */
	event_mask = XWINDOW_EVENT_MASK_EXPOSURE
	           | XWINDOW_EVENT_MASK_WINDOW_ENTER;
	bar->xwindow = xserver_create_xwindow(bar->dim, 0, event_mask, true);
	if (!bar->xwindow) {
		log_set("Could not create X window: %s", log_str());
		goto error_window;
	}
	if (xwindow_map(bar->xwindow) < 0) {
		log_set("Could not map X window %lu: %s",
		        bar->xwindow->id, log_str());
		goto error_map;
	}

	/* pixmap */
	bar->pm = XCreatePixmap(xserver.display, xserver.root->id,
	                        bar->dim.width, bar->dim.height,
	                        xoutput.screen_depth);

	statusbar_redraw(bar);

	/* bus events */
	if (_init_bus(bar) < 0)
		goto error_bus;

	return bar;

 error_bus:
	log_push();
	if (xwindow_unmap(bar->xwindow) < 0)
		ERROR("Could not unmap statusbar X window %lu during error handling: %s",
		      bar->xwindow->id, log_str());
	log_pop();
 error_map:
	XFreePixmap(xserver.display, bar->pm);
	log_push();
	xserver_destroy_xwindow(bar->xwindow);
	log_pop();
 error_window:
	log_push();
	memory_free(bar);
	log_pop();
 error_alloc:
	return NULL;
}

void
statusbar_free(struct statusbar *bar)
{
	bus_disconnect(bar->bus.session);
	if (xwindow_unmap(bar->xwindow) < 0)
		WARN("Could not unmap statusbar X window %lu: %s",
		     bar->xwindow->id, log_str());
	xserver_destroy_xwindow(bar->xwindow);
	XFreePixmap(xserver.display, bar->pm);
	memory_free(bar);
}

void
statusbar_redraw(struct statusbar *bar)
{
	struct monitor *mon = bar->monitor;
	struct rectangle newdim;
	struct layout *l;
	uint32_t colour_fg = bar->focused ? statusbar.focused.colour_fg
	                                  : statusbar.unfocused.colour_fg;
	uint32_t colour_bg = bar->focused ? statusbar.focused.colour_bg
	                                  : statusbar.unfocused.colour_bg;

	/* bar dimension */
	if (bar->dim.x != mon->dim.x
	|| bar->dim.y != mon->dim.y
	|| bar->dim.width != mon->dim.width)
	{
		newdim = (struct rectangle) {
			.pos = mon->dim.pos,
			.width = mon->dim.width,
			.height = bar->dim.height,
		};
		if (xwindow_moveresize(bar->xwindow, newdim, false) < 0) {
			ERROR("Could not move-resize bar window %lu: %s",
			      bar->xwindow->id, log_str());
		} else {
			bar->dim = newdim;
			XFreePixmap(xserver.display, bar->pm);
			bar->pm = XCreatePixmap(xserver.display,
			                        xserver.root->id,
			                        bar->dim.width, bar->dim.height,
			                        xoutput.screen_depth);
		}
	}

	/* background */
	XSetForeground(xserver.display, xserver.graphics_context, colour_bg);
	XFillRectangle(xserver.display, bar->pm, xserver.graphics_context, 0, 0,
	               bar->dim.width, bar->dim.height);

	/* content */
	if (bar->workspace) {
		/* layout icon */
		l = bar->workspace->layout;
		if (icon_draw(l->icon, colour_fg, colour_bg) < 0)
			WARN("Could not draw layout icon to its pixmap: %s",
			     log_str());
		XCopyArea(xserver.display, l->icon->pixmap, bar->pm,
		          xserver.graphics_context,
		          0, 0, l->icon->w, l->icon->h, 2, 0);

		/* workspace name */
		sprintf(bar->buffer, "%s", bar->workspace->name);
		XSetForeground(xserver.display, xserver.graphics_context,
		               colour_fg);
		Xutf8DrawString(xserver.display, bar->pm,
		                statusbar.xfont->fontset,
		                xserver.graphics_context,
		                (int signed) (l->icon->w + 12),
		                (int signed) statusbar.xfont->ascent + 1,
		                bar->buffer, (int signed) strlen(bar->buffer));
	}

	/* map to bar window */
	XCopyArea(xserver.display, bar->pm, bar->xwindow->id,
	          xserver.graphics_context,
	          0, 0, bar->dim.width, bar->dim.height, 0, 0);
	XSync(xserver.display, False);
}

int
statusbar_set_focus(struct statusbar *bar, bool focus)
{
	if (bar->focused == focus)
		return 0;

	bar->focused = focus;
	statusbar_redraw(bar);

	return 0;
}

static void
_bus_event(struct bus_subscription const *sub, void *data, void *ctx)
{
	struct bus_channel *ch = sub->channel;
	struct statusbar *bar = ctx;
	struct monitor *mon;
	struct workspace *ws;
	struct xwindow *xwin;
	bool focus;

	if (ch == bus.monitor_focus) {
		mon = data;
		focus = mon == bar->monitor;
		DEBUG("Bus event '%s', %ssetting statusbar focus",
		      ch->name, focus ? "" : "un");
		if (statusbar_set_focus(bar, bar->monitor == mon) < 0)
			ERROR("Could not %sset statusbar focus: %s",
			      focus ? "" : "un", log_str());
	} else if (ch == bus.monitor_resize) {
		mon = data;
		if (mon != bar->monitor) {
			BUG("Statusbar received bus event for different monitor");
			return;
		}
		DEBUG("Bus event '%s', redrawing statusbar for new monitor size %ux%u%+d%+d",
		      ch->name,
		      mon->dim.width, mon->dim.height, mon->dim.x, mon->dim.y);
		statusbar_redraw(bar);
	} else if (ch == bus.monitor_workspace) {
		mon = data;
		if (mon != bar->monitor) {
			BUG("Statusbar received bus event for different monitor");
			return;
		}
		ws = mon->workspace;
		if (bus_subscription_set_data_mask(bar->bus.sub_ws_rename, ws) < 0)
			/* Not much we can do here */
			ERROR("Could not subscribe to workspace rename events for workspace '%s': %s",
			      ws->name, log_str());
		if (bus_subscription_set_data_mask(bar->bus.sub_ws_layout, ws) < 0)
			/* Not much we can do here */
			ERROR("Could not subscribe to workspace layout events for workspace '%s': %s",
			      ws->name, log_str());
		bar->workspace = mon->workspace;
		statusbar_redraw(bar);
	} else if (ch == bus.workspace_rename || ch == bus.workspace_layout) {
		ws = data;
		if (ws != bar->workspace) {
			BUG("Statusbar received bus event for different workspace '%s'",
			    ws->name);
			return;
		}
		statusbar_redraw(bar);
	} else if (ch == bus.x_window_notify_expose) {
		xwin = data;
		if (xwin != bar->xwindow) {
			BUG("Statusbar received bus event '%s' for different X window %lu; ignoring",
			    ch->name, xwin->id);
			return;
		}
		statusbar_redraw(bar);
	} else {
		BUG("Statusbar received bus event from unexpected channel '%s'",
		    ch->name);
	}
}

static int
_init_bus(struct statusbar *bar)
{
	int unsigned i;
	struct bus_subscription *dsub; /* dummy */
	struct {
		struct bus_channel *ch;
		struct bus_subscription **subptr;
		void const *evmask;
	} subs[] = {
		{ bus.monitor_focus, &dsub, BUS_SUBSCRIPTION_DATA_MASK_ALL },
		{ bus.monitor_resize, &dsub, bar->monitor },
		{ bus.monitor_workspace, &dsub, bar->monitor },
		{ bus.workspace_rename, &bar->bus.sub_ws_rename, bar->workspace },
		{ bus.workspace_layout, &bar->bus.sub_ws_layout, bar->workspace },
		{ bus.x_window_notify_expose, &dsub, bar->xwindow },
	};

	bar->bus.session = bus_connect("statusbar", _bus_event, bar);
	if (!bar->bus.session) {
		log_set("Could not open event bus session: %s", log_str());
		goto error_connect;
	}

	for (i = 0; i < LENGTH(subs); ++i) {
		*subs[i].subptr = bus_subscription_alloc(bar->bus.session,
		                                         subs[i].ch,
		                                         subs[i].evmask);
		if (!*subs[i].subptr) {
			log_set("Could not join event bus channel '%s': %s",
			        subs[i].ch->name, log_str());
			goto error_subscribe;
		}
	}

	return 0;

 error_subscribe:
	log_push();
	bus_disconnect(bar->bus.session);
	log_pop();
 error_connect:
	return -1;
}
