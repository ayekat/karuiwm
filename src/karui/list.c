#include <karui/list.h>
#include <karui/log.h>
#include <karui/memory.h>
#include <string.h>

struct list *
list_alloc(void)
{
	struct list *l;

	l = memory_alloc(sizeof(struct list), "list structure");
	if (!l)
		return NULL;
	if (list_init(l) < 0) {
		log_set("Could not initialise list: %s", log_str());
		memory_free(l);
		return NULL;
	}
	return l;
}

int
list_init(struct list *l)
{
	l->size = 0;
	l->elements = NULL;
	return 0;
}

void
list_deinit(struct list *l)
{
	if (l->size > 0)
		memory_free(l->elements);
	l->size = 0;
	l->elements = NULL;
}

void
list_free(struct list *l)
{
	list_deinit(l);
	memory_free(l);
}

void
list_append(struct list *l, void *e)
{
	list_insert(l, e, (int unsigned) l->size);
}

void
list_clear(struct list *l, void (*f)(void *e))
{
	int unsigned i;

	if (f)
		for (i = 0; i < l->size; ++i)
			f(l->elements[i]);
	memory_free(l->elements);
	l->elements = NULL;
	l->size = 0;
}

int
list_concatenate(struct list *l1, struct list *l2)
{
	if (memory_resize((void **) &l1->elements,
	                  (l1->size + l2->size) * sizeof(void *),
	                  "list elements") < 0) {
		log_set("Could not grow first list: %s", log_str());
		return -1;
	}
	(void) memcpy(l1->elements + l1->size, l2->elements,
	              l2->size * sizeof(void *));
	l1->size += l2->size;
	list_free(l2);
	return 0;
}

bool
list_contains(struct list *l, void const *e,
              bool (*f)(void const *le, void const *e))
{
	int unsigned i;

	for (i = 0; i < l->size; ++i)
		if (e == l->elements[i] || (f && f(l->elements[i], e)))
			return true;
	return false;
}

struct list *
list_copy(struct list const *l, list_copyfunc cf, list_delfunc df)
{
	struct list *l2;
	int unsigned i;
	void *e;

	l2 = list_alloc();
	if (!l2)
		goto error_list;

	for (i = 0; i < l->size; ++i) {
		if (cf) {
			e = cf(l->elements[i]);
			if (!e) {
				log_set("Could not copy element %u: %s",
				        i, log_str());
				goto error_copy;
			}
		} else {
			e = l->elements[i];
		}
		list_append(l2, e);
	}
	return l2;

 error_copy:
	log_push();
	list_clear(l2, df);
	list_free(l2);
	log_pop();
 error_list:
	return NULL;
}

void *
list_find(struct list const *l, void const *e,
          bool (*f)(void const *le, void const *e))
{
	int unsigned i;

	for (i = 0; i < l->size; ++i) {
		if (f) {
			if (f(l->elements[i], e))
				return l->elements[i];
		} else {
			if (l->elements[i] == e)
				return l->elements[i];
		}
	}
	return NULL;
}

int unsigned
list_find_index(struct list *l, void const *e,
                bool (*f)(void const *le, void const *e))
{
	int unsigned i;

	for (i = 0; i < l->size; ++i)
		if (e == l->elements[i] || (f && f(l->elements[i], e)))
			return i;
	FATAL_BUG("list_find_index() on element not contained in list");
}

void
list_insert(struct list *l, void *e, int unsigned index)
{
	int unsigned i;

	if (index > l->size)
		FATAL_BUG("list_insert() index %u out of bounds (list of size %zu)",
		          index, l->size);

	/* grow list */
	++l->size;
	if (memory_resize((void **) &l->elements, l->size * sizeof(void *),
	                  "list elements array") < 0)
		FATAL("Could not grow list to %zu elements: %s",
		      l->size, log_str());

	/* shift elements */
	for (i = (int unsigned) l->size - 1; i > index; --i)
		l->elements[i] = l->elements[i - 1];

	/* insert new element */
	l->elements[index] = e;
}

void
list_insert_sorted(struct list *l, void *e,
                   int (*f)(void const *e1, void const *e2))
{
	int unsigned i;

	for (i = 0; i < l->size && f(e, l->elements[i]) > 0; ++i);
	list_insert(l, e, i);
}

void
list_prepend(struct list *l, void *e)
{
	list_insert(l, e, 0);
}

void
list_push(struct list *l, void *e)
{
	int unsigned i;

	for (i = 0; i < l->size && l->elements[i] != e; ++i);
	if (i >= l->size)
		FATAL_BUG("Attempt to push non-existing element to top of list");

	for (; i > 0; --i)
		l->elements[i] = l->elements[i - 1];

	l->elements[0] = e;
}

void
list_remove(struct list *l, void const *e,
            bool (*f)(void const *le, void const *e))
{
	int unsigned i;

	/* determine index */
	for (i = 0; i < l->size; ++i)
		if (e == l->elements[i] || (f && f(l->elements[i], e)))
			break;
	if (i >= l->size)
		FATAL_BUG("Attempt to remove non-existing element from list");

	/* shift rest of list */
	--l->size;
	for (; i < l->size; ++i)
		l->elements[i] = l->elements[i + 1];

	/* resize array */
	if (memory_resize((void **) &l->elements, l->size * sizeof(void *),
	                  "list elements array") < 0)
		WARN("Could not shrink list to %zu elements: %s",
		     l->size, log_str());
}
