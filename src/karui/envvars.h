#ifndef _KARUI_ENVVARS_H
#define _KARUI_ENVVARS_H

int envvars_init(void);
void envvars_deinit(void);

char const *envvars_get(char const *name, char const *default_value);

#endif /* ndef _KARUI_ENVVARS_H */
