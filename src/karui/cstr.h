#ifndef _KARUI_CSTR_H
#define _KARUI_CSTR_H

#include <karui/list.h>
#include <stdbool.h>
#include <stdint.h>

void cstr_free(char *str);

char *cstr_dup(char const *str);
bool cstr_equals(char const *str1, char const *str2);
bool cstr_equals_caseless(char const *str1, char const *str2);
char const *cstr_find_char(char const *str, char c);
char *cstr_format(char const *format, ...);
char *cstr_join(struct list const *strs, char const *delim);
char *cstr_longest_prefix(struct list const *strs);
bool cstr_matches_wildcard(char const *str, char const *wildcard);
int cstr_parse_bool(bool *retval, char const *str);
int cstr_parse_colour(uint32_t *retval, char const *str);
int cstr_parse_float(float *retval, char const *str);
int cstr_parse_int(int *retval, char const *str, char **endptr);
int cstr_parse_int_signed(int *retval, char const *str, char **endptr);
int cstr_parse_int_unsigned(int unsigned *retval, char const *str,
                            char **endptr);
struct list *cstr_split(char const *str, char const *delims);
int cstr_split_once(char const *str, char const *delims,
                    char **first, char **second);
struct list *cstr_split_smart(char const *str);
bool cstr_starts_with(char const *str, char const *substr);

#endif /* ndef _KARUI_CSTR_H */
