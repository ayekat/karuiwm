#ifndef _KARUI_USERDIRS_H
#define _KARUI_USERDIRS_H

int userdirs_init(void);
void userdirs_deinit(void);

char const *userdirs_get_cachedir(void);
char const *userdirs_get_configdir(void);
struct list const *userdirs_get_configdirs(void);
char const *userdirs_get_datadir(void);
struct list const *userdirs_get_datadirs(void);
char const *userdirs_get_homedir(void);
char const *userdirs_get_libdir(void);
struct list const *userdirs_get_libdirs(void);
char const *userdirs_get_logdir(void);
char const *userdirs_get_runtimedir(void);
char const *userdirs_get_statedir(void);

#endif /* ndef _KARUI_USERDIRS_H */
