#include <karui/memory.h>
#include <karui/log.h>
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

void *
memory_alloc(size_t size, char const *name)
{
	void *mem;

	mem = malloc(size);
	if (!mem && size > 0)
		log_set("Could not allocate %zu bytes for %s: %s",
		        size, name, strerror(errno));
	return mem;
}

void
memory_free(void *mem)
{
	free(mem);
}

void *
memory_dup(void const *mem, size_t size, char const *name)
{
	void *ptr;

	if (size == 0) {
		log_set("Zero-length memory");
		return NULL;
	}
	assert(mem);

	ptr = malloc(size);
	if (!ptr) {
		log_set("Could not allocate %zu bytes for copy of %s: %s",
		        size, name, strerror(errno));
		return NULL;
	}
	(void) memcpy(ptr, mem, size);
	return ptr;
}

int
memory_resize(void **mem, size_t size, char const *name)
{
	void *ptr;

	assert(mem);

	ptr = realloc(*mem, size);
	if (!ptr && size > 0) {
		log_set("Could not resize %s to %zu bytes: %s",
		        name, size, strerror(errno));
		return -1;
	}
	*mem = ptr;
	return 0;
}
