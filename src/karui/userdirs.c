#include <karui/userdirs.h>
#include <karui/cstr.h>
#include <karui/envvars.h>
#include <karui/list.h>
#include <karui/log.h>
#include <errno.h>
#include <pwd.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

static struct {
	uid_t uid;
	char *cachedir;
	char *configdir;
	struct list *configdirs;
	char *datadir;
	struct list *datadirs;
	char *homedir;
	char *libdir;
	struct list *libdirs;
	char *logdir;
	char *runtimedir;
	char *statedir;
} _userdirs;

static int _init_cachedir(void);
static int _init_configdir(void);
static int _init_configdirs(void);
static int _init_datadir(void);
static int _init_datadirs(void);
static int _init_homedir(void);
static int _init_libdir(void);
static int _init_libdirs(void);
static int _init_logdir(void);
static int _init_runtimedir(void);
static int _init_statedir(void);
static int _init_uid(void);
static char *_xdgdir(char const *varname, char const *default_dir);
static struct list *_xdgdirs(char const *varname, char const *default_paths);

int
userdirs_init(void)
{
	if (_init_uid() < 0) {
		log_set("Could not initialise user ID: %s", log_str());
		goto error_uid;
	}
	if (_init_homedir() < 0) {
		log_set("Could not initialise home directory: %s", log_str());
		goto error_homedir;
	}
	if (_init_cachedir() < 0) {
		log_set("Could not initialise cache directory: %s", log_str());
		goto error_cachedir;
	}
	if (_init_configdir() < 0) {
		log_set("Could not initialise configuration directory: %s",
		        log_str());
		goto error_configdir;
	}
	if (_init_configdirs() < 0) {
		log_set("Could not initialise configuration directories: %s",
		        log_str());
		goto error_configdirs;
	}
	if (_init_datadir() < 0) {
		log_set("Could not initialise data directory: %s", log_str());
		goto error_datadir;
	}
	if (_init_datadirs() < 0) {
		log_set("Could not initialise data directories: %s", log_str());
		goto error_datadirs;
	}
	if (_init_libdir() < 0) {
		log_set("Could not initialise library directory: %s",
		        log_str());
		goto error_libdir;
	}
	if (_init_libdirs() < 0) {
		log_set("Could not initialise library directories: %s",
		        log_str());
		goto error_libdirs;
	}
	if (_init_logdir() < 0) {
		log_set("Could not initialise log directory: %s", log_str());
		goto error_logdir;
	}
	if (_init_runtimedir() < 0) {
		log_set("Could not initialise runtime directory: %s",
		        log_str());
		goto error_runtimedir;
	}
	if (_init_statedir() < 0) {
		log_set("Could not initialise state directory: %s", log_str());
		goto error_statedir;
	}
	return 0;

 error_statedir:
	cstr_free(_userdirs.runtimedir);
 error_runtimedir:
	cstr_free(_userdirs.logdir);
 error_logdir:
	list_clear(_userdirs.libdirs, (list_delfunc) cstr_free);
	list_free(_userdirs.libdirs);
 error_libdirs:
	cstr_free(_userdirs.libdir);
 error_libdir:
	list_clear(_userdirs.datadirs, (list_delfunc) cstr_free);
	list_free(_userdirs.datadirs);
 error_datadirs:
	cstr_free(_userdirs.datadir);
 error_datadir:
	list_clear(_userdirs.configdirs, (list_delfunc) cstr_free);
	list_free(_userdirs.configdirs);
 error_configdirs:
	cstr_free(_userdirs.configdir);
 error_configdir:
	cstr_free(_userdirs.cachedir);
 error_cachedir:
	cstr_free(_userdirs.homedir);
 error_homedir:
 error_uid:
	return -1;
}

void
userdirs_deinit(void)
{
	cstr_free(_userdirs.cachedir);
	cstr_free(_userdirs.configdir);
	list_clear(_userdirs.configdirs, (list_delfunc) cstr_free);
	list_free(_userdirs.configdirs);
	cstr_free(_userdirs.datadir);
	list_clear(_userdirs.datadirs, (list_delfunc) cstr_free);
	list_free(_userdirs.datadirs);
	cstr_free(_userdirs.homedir);
	cstr_free(_userdirs.libdir);
	list_clear(_userdirs.libdirs, (list_delfunc) cstr_free);
	list_free(_userdirs.libdirs);
	cstr_free(_userdirs.logdir);
	cstr_free(_userdirs.runtimedir);
	cstr_free(_userdirs.statedir);
}

char const *
userdirs_get_cachedir(void)
{
	return _userdirs.cachedir;
}

char const *
userdirs_get_configdir(void)
{
	return _userdirs.configdir;
}

struct list const *
userdirs_get_configdirs(void)
{
	return _userdirs.configdirs;
}

char const *
userdirs_get_datadir(void)
{
	return _userdirs.datadir;
}

struct list const *
userdirs_get_datadirs(void)
{
	return _userdirs.datadirs;
}

char const *
userdirs_get_homedir(void)
{
	return _userdirs.homedir;
}

char const *
userdirs_get_libdir(void)
{
	return _userdirs.libdir;
}

struct list const *
userdirs_get_libdirs(void)
{
	return _userdirs.libdirs;
}

char const *
userdirs_get_logdir(void)
{
	return _userdirs.logdir;
}

char const *
userdirs_get_runtimedir(void)
{
	return _userdirs.runtimedir;
}

char const *
userdirs_get_statedir(void)
{
	return _userdirs.statedir;
}

static int
_init_cachedir(void)
{
	_userdirs.cachedir = _xdgdir("XDG_CACHE_HOME", ".cache");
	return _userdirs.cachedir ? 0 : -1;
}

static int
_init_configdir(void)
{
	_userdirs.configdir = _xdgdir("XDG_CONFIG_HOME", ".config");
	return _userdirs.configdir ? 0 : -1;
}

static int
_init_configdirs(void)
{
	_userdirs.configdirs = _xdgdirs("XDG_CONFIG_DIRS", "/etc/xdg");
	return _userdirs.configdirs ? 0 : -1;
}

static int
_init_datadir(void)
{
	_userdirs.datadir = _xdgdir("XDG_DATA_HOME", ".local/share");
	return _userdirs.datadir ? 0 : -1;
}

static int
_init_datadirs(void)
{
	_userdirs.datadirs = _xdgdirs("XDG_DATA_DIRS",
	                              "/usr/local/share:/usr/share");
	return _userdirs.datadirs ? 0 : -1;
}

static int
_init_homedir(void)
{
	char const *env, *pwd_homedir;
	struct passwd *pwd;

	pwd = getpwuid(_userdirs.uid);
	if (pwd) {
		DEBUG("passwd entry home directory: %s", pwd->pw_dir);
		pwd_homedir = (char const *) pwd->pw_dir;
	} else {
		WARN("Could not get passwd entry: %s", strerror(errno));
		pwd_homedir = NULL;
	}

	env = envvars_get("HOME", pwd_homedir);
	if (!env) {
		log_set("Neither HOME nor passwd entry set");
		return -1;
	}
	if (!cstr_equals(env, pwd_homedir))
		WARN("Differing home directories ($HOME=%s, passwd=%s)",
		     env, pwd_homedir);

	_userdirs.homedir = cstr_dup(env);
	if (!_userdirs.homedir) {
		log_set("Could not copy environment value string: %s",
		        log_str());
		return -1;
	}
	return 0;
}

static int
_init_libdir(void)
{
	_userdirs.libdir = _xdgdir("XDG_LIB_HOME", ".local/lib");
	return _userdirs.libdir ? 0 : -1;
}

static int
_init_libdirs(void)
{
	_userdirs.libdirs = _xdgdirs("XDG_LIB_DIRS",
	                             "/usr/local/lib:/usr/lib");
	return _userdirs.libdirs ? 0 : -1;
}

static int
_init_logdir(void)
{
	_userdirs.logdir = _xdgdir("XDG_LOG_HOME", ".var/log");
	return _userdirs.logdir ? 0 : -1;
}

static int
_init_runtimedir(void)
{
	char *default_dir;

	default_dir = cstr_format("/run/user/%d", (int unsigned) _userdirs.uid);
	if (!default_dir) {
		log_set("Could not create default directory path string: %s",
		        log_str());
		return -1;
	}

	_userdirs.runtimedir = _xdgdir("XDG_RUNTIME_DIR", default_dir);
	if (_userdirs.runtimedir != default_dir)
		cstr_free(default_dir);
	return _userdirs.runtimedir ? 0 : -1;
}

static int
_init_statedir(void)
{
	_userdirs.statedir = _xdgdir("XDG_STATE_HOME", ".var/state");
	return _userdirs.statedir ? 0 : -1;
}

static int
_init_uid(void)
{
	/* getuid(3p):
	 * > The getuid() function shall always be successful and no return
	 * > value is reserved to indicate the error.
	 */
	_userdirs.uid = getuid();
	return 0;
}

static char *
_xdgdir(char const *varname, char const *default_dir)
{
	char const *env;
	char *default_dir_abs;

	if (default_dir[0] == '/')
		default_dir_abs = cstr_dup(default_dir);
	else
		default_dir_abs = cstr_format("%s/%s",
		                              _userdirs.homedir, default_dir);
	if (!default_dir_abs) {
		log_set("Could not duplicate string for absolute path to %s: %s",
		        default_dir, log_str());
		return NULL;
	}

	env = envvars_get(varname, default_dir_abs);
	if (env == default_dir_abs)
		return default_dir_abs;
	else
		cstr_free(default_dir_abs);
	return cstr_dup(env);
}

static struct list *
_xdgdirs(char const *varname, char const *default_paths)
{
	return cstr_split(envvars_get(varname, default_paths), ":");
}
