#include <karui/envvars.h>
#include <karui/cstr.h>
#include <karui/list.h>
#include <karui/log.h>
#include <karui/memory.h>
#include <stdlib.h>

struct _envvar {
	char *name;
	char *value;
};

static struct _envvar *_envvar_create(char const *name);
static void _envvar_destroy(struct _envvar *envvar);
static char const *_envvar_get_value(struct _envvar *ev,
                                     char const *default_value);

static struct list *_envvars;

int
envvars_init(void)
{
	_envvars = list_alloc();
	if (!_envvars) {
		log_set("Could not create new environment variables list: %s",
		        log_str());
		return -1;
	}

	return 0;
}

void
envvars_deinit(void)
{
	list_clear(_envvars, (list_delfunc) _envvar_destroy);
	list_free(_envvars);
	_envvars = NULL;
}

char const *
envvars_get(char const *name, char const *default_value)
{
	struct _envvar *ev;
	int unsigned i;

	/* search in local cache */
	LIST_FOR (_envvars, i, ev)
		if (cstr_equals(ev->name, name))
			return _envvar_get_value(ev, default_value);

	/* environmen variable not found, query system and cache locally */
	ev = _envvar_create(name);
	list_append(_envvars, ev);
	return _envvar_get_value(ev, default_value);
}

static struct _envvar *
_envvar_create(char const *name)
{
	struct _envvar *ev;
	char const *osenv;

	ev = memory_alloc(sizeof(struct _envvar),
	                   "environment variable structure");
	if (!ev)
		goto error_struct;

	ev->name = cstr_dup(name);
	if (!ev->name) {
		log_set("Could not copy name string: %s", log_str());
		goto error_name;
	}

	osenv = getenv(name);
	if (osenv) {
		ev->value = cstr_dup(osenv);
		if (!ev->value) {
			log_set("Could not copy environment variable value string: %s",
			        log_str());
			goto error_value;
		}
	} else {
		ev->value = NULL;
	}

	return ev;

 error_value:
	cstr_free(ev->name);
 error_name:
	memory_free(ev);
 error_struct:
	return NULL;
}

static void
_envvar_destroy(struct _envvar *ev)
{
	cstr_free(ev->name);
	if (ev->value)
		cstr_free(ev->value);
	memory_free(ev);
}

static char const *
_envvar_get_value(struct _envvar *ev, char const *default_value)
{
	return ev->value ? ev->value : default_value;
}
