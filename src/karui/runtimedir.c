#include <karui/cstr.h>
#include <karui/envvars.h>
#include <karui/log.h>
#include <karui/runtimedir.h>
#include <karui/userdirs.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

static void _deinit_rundir(void);
static void _deinit_rundir_app(void);
static int _init_rundir(void);
static int _init_rundir_app(char const *appname);

static char *_rundir_app, *_rundir;

int
runtimedir_init(char const *appname)
{
	if (_init_rundir_app(appname) < 0) {
		log_set("Could not initialise application-wide runtime directory: %s",
		        log_str());
		goto error_rundir_app;
	}
	if (_init_rundir() < 0) {
		log_set("Could not initialise runtime directory: %s",
		        log_str());
		goto error_rundir;
	}
	return 0;

 error_rundir:
	cstr_free(_rundir_app);
 error_rundir_app:
	return -1;
}

void
runtimedir_deinit(void)
{
	_deinit_rundir();
	_deinit_rundir_app();
}

char const *
runtimedir_get(void)
{
	return (char const *) _rundir;
}

static void
_deinit_rundir(void)
{
	DEBUG("Deleting runtime directory %s", _rundir);
	if (rmdir(_rundir) < 0)
		ERROR("Could not delete runtime directory %s: %s",
		      _rundir, strerror(errno));
	cstr_free(_rundir);
	_rundir = NULL;
}

static void
_deinit_rundir_app(void)
{
	DEBUG("Deleting application-wide runtime directory %s", _rundir_app);
	if (rmdir(_rundir_app) < 0 && errno != EEXIST && errno != ENOTEMPTY)
		ERROR("Could not delete application-wide runtime directory %s: %s",
		      _rundir_app, strerror(errno));
	cstr_free(_rundir_app);
	_rundir_app = NULL;
}

static int
_init_rundir(void)
{
	char const *env_display;

	env_display = envvars_get("DISPLAY", NULL);
	if (!env_display) {
		log_set("DISPLAY variable not set");
		goto error_env;
	}

	_rundir = cstr_format("%s/%s", _rundir_app, env_display);
	if (!_rundir) {
		log_set("Could not assemble string for runtime directory: %s",
		        log_str());
		goto error_strdup;
	}

	DEBUG("Creating runtime directory %s", _rundir);
	if (mkdir(_rundir, S_IRWXU) < 0) {
		if (errno == EEXIST) {
			WARN("Could not create runtime directory %s: %s",
			     _rundir, strerror(errno));
		} else {
			log_set("Could not create runtime directory %s: %s",
			        _rundir, strerror(errno));
			goto error_mkdir;
		}
	}
	return 0;

 error_mkdir:
	log_push();
	cstr_free(_rundir);
	log_pop();
	_rundir = NULL;
 error_strdup:
 error_env:
	return -1;
}

static int
_init_rundir_app(char const *appname)
{
	_rundir_app = cstr_format("%s/%s", userdirs_get_runtimedir(), appname);
	if (!_rundir_app) {
		log_set("Could not assemble string for application-wide runtime directory: %s",
		        log_str());
		goto error_strdup;
	}

	DEBUG("Creating application-wide runtime directory %s", _rundir_app);
	if (mkdir(_rundir_app, S_IRWXU) < 0 && errno != EEXIST) {
		log_set("Could not create application-wide runtime directory %s: %s",
		        _rundir_app, strerror(errno));
		goto error_mkdir;
	}
	return 0;

 error_mkdir:
	cstr_free(_rundir_app);
	_rundir_app = NULL;
 error_strdup:
	return -1;
}
