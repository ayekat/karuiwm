#ifndef _KWM_SYSTEM_H
#define _KWM_SYSTEM_H

#include <karui/list.h>
#include <sys/types.h>

int system_init(void);
void system_deinit(void);

int system_exec(struct list const *args);
pid_t system_fork_exec(struct list const *args);

#endif /* ndef _KWM_SYSTEM_H */
