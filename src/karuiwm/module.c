#include <karuiwm/module.h>
#include <karui/cstr.h>
#include <karui/list.h>
#include <karui/log.h>
#include <karui/memory.h>
#include <dlfcn.h>

static void *_load_symbol(struct module *mod, char const *name);

struct module *
module_alloc(char const *path, char const *name)
{
	struct module *mod;

	mod = memory_alloc(sizeof(struct module), "module struct");
	if (!mod)
		goto error_alloc;

	mod->data.name = cstr_dup(name);
	if (!mod->data.name) {
		log_set("Could not copy name: %s", log_str());
		goto error_name;
	}

	mod->path = cstr_dup(path);
	if (!mod->path) {
		log_set("Could not copy path: %s", log_str());
		goto error_path;
	}

	mod->so_handler = dlopen(mod->path, RTLD_NOW);
	if (!mod->so_handler) {
		/* dlerror() is already fairly descriptive by itself */
		log_set("%s", dlerror());
		goto error_dlopen;
	}

	*(void **) &mod->announce = _load_symbol(mod, "announce");
	if (!mod->announce) {
		log_set("Could not load announce function: %s", log_str());
		goto error_dlsym;
	}

	if (mod->announce(&mod->data) < 0) {
		log_set("Module announcement failed: %s", log_str());
		goto error_announce;
	}

	return mod;

 error_announce:
 error_dlsym:
	if (dlclose(mod->so_handler) != 0)
		WARN("Could not close shared object handler: %s", dlerror());
 error_dlopen:
	log_push();
	cstr_free(mod->path);
	log_pop();
 error_path:
	log_push();
	cstr_free(mod->data.name);
	log_pop();
 error_name:
	log_push();
	memory_free(mod);
	log_pop();
 error_alloc:
	return NULL;
}

int
module_init(struct module *mod)
{
	return mod->data.init ? mod->data.init(&mod->data) : 0;
}

int
module_deinit(struct module *mod)
{
	return mod->data.deinit ? mod->data.deinit(&mod->data) : 0;
}

void
module_free(struct module *mod)
{
	if (dlclose(mod->so_handler) != 0)
		WARN("Could not close shared object handler: %s", dlerror());
	cstr_free(mod->path);
	cstr_free(mod->data.name);
	memory_free(mod);
}

static void *
_load_symbol(struct module *mod, char const *symname)
{
	void *sym;
	char const *dlerr;

	(void) dlerror();
	sym = dlsym(mod->so_handler, symname);

	dlerr = dlerror();
	if (dlerr) {
		/* dlsym() is already fairly descriptive by itself */
		log_set("%s", dlerr);
		goto error_load;
	}
	if (!sym) {
		/* XXX: According to dlsym(3), NULL is not necessarily an error,
		 * but for now we expect all symbols in the module files to
		 * resolve to a non-NULL value anyway, so... we treat this as an
		 * error.
		 */
		log_set("Obtained NULL handle for symbol '%s'", symname);
		goto error_load;
	}

	return sym;

 error_load:
	return NULL;

}
