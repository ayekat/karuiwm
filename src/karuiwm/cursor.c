#include <karuiwm/cursor.h>
#include <karuiwm/input.h>
#include <karuiwm/xserver.h>
#include <karui/log.h>
#include <X11/cursorfont.h>
#include <X11/Xlib.h>

static Cursor _cursors[CURSOR_SHAPE_LAST];

int
cursor_init(void)
{
	int unsigned i;
	int unsigned font_shapes[CURSOR_SHAPE_LAST] = {
		[CURSOR_SHAPE_NORMAL] = XC_left_ptr,
		[CURSOR_SHAPE_MOVE] = XC_fleur,
		[CURSOR_SHAPE_RESIZE_BOTTOM] = XC_bottom_side,
		[CURSOR_SHAPE_RESIZE_BOTTOM_LEFT] = XC_bottom_left_corner,
		[CURSOR_SHAPE_RESIZE_BOTTOM_RIGHT] = XC_bottom_right_corner,
		[CURSOR_SHAPE_RESIZE_LEFT] = XC_left_side,
		[CURSOR_SHAPE_RESIZE_RIGHT] = XC_right_side,
		[CURSOR_SHAPE_RESIZE_TOP] = XC_top_side,
		[CURSOR_SHAPE_RESIZE_TOP_LEFT] = XC_top_left_corner,
		[CURSOR_SHAPE_RESIZE_TOP_RIGHT] = XC_top_right_corner,
	};

	for (i = 0; i < CURSOR_SHAPE_LAST; ++i) {
		_cursors[i] = XCreateFontCursor(xserver.display,
		                                font_shapes[i]);
		if (!_cursors[i]) {
			log_set("Could not create font cursor for shape %u",
			        font_shapes[i]);
			goto error_cursor_shapes;
		}
	}

	if (cursor_set(CURSOR_SHAPE_NORMAL) < 0) {
		log_set("Could not set cursor shape to normal: %s", log_str());
		goto error_cursor_set;
	}

	return 0;

 error_cursor_set:
 error_cursor_shapes:
	log_push();
	while (i-- > 0)
		XFreeCursor(xserver.display, _cursors[i]);
	log_pop();
	return -1;
}

void
cursor_deinit(void)
{
	int unsigned i;

	for (i = 0; i < CURSOR_SHAPE_LAST; ++i)
		XFreeCursor(xserver.display, _cursors[i]);
}

int
cursor_grab(enum cursor_shape shape)
{
	int i;

	i = XGrabPointer(xserver.display, xserver.root->id, True,
	                 INPUT_MASK_MOUSE, GrabModeAsync, GrabModeAsync, None,
	                 _cursors[shape], CurrentTime);
	if (i != GrabSuccess) {
		log_set("XGrabPointer() failed");
		return -1;
	}
	return 0;
}

int
cursor_set(enum cursor_shape type)
{
	XSetWindowAttributes xswa;
	xswa.cursor = _cursors[type];
	XChangeWindowAttributes(xserver.display, xserver.root->id,
	                        CWCursor, &xswa);

	return 0;
}

void
cursor_ungrab(void)
{
	XUngrabPointer(xserver.display, CurrentTime);
}
