#ifndef _KWM_XFONT_H
#define _KWM_XFONT_H

#include <X11/Xlib.h>

struct xfont {
	Display *dpy;
	char *name;
	int unsigned ascent, descent, height;
	XFontSet fontset;
};

struct xfont *xfont_alloc(Display *dpy, char const *locale, char const *name);
void xfont_free(struct xfont *xfont);

#endif /* ndef _KWM_XFONT_H */
