#include <karuiwm/button.h>
#include <karuiwm/key.h>
#include <karui/cstr.h>
#include <karui/list.h>
#include <karui/log.h>
#include <karui/memory.h>

bool
button_equals(struct button const *b1, struct button const *b2)
{
	return b1->mod == b2->mod && b1->button == b2->button;
}

int
button_parse(struct button *b, char const *str)
{
	struct list *tokens;
	char const *modstr;
	char *buttonstr;
	struct button _b;
	int unsigned i, mod;

	tokens = cstr_split(str, "+");
	if (!tokens) {
		log_set("Could not split '%s' into modifiers and key: %s",
		        str, log_str());
		goto error_split;
	}
	if (tokens->size == 0) {
		log_set("Empty key definition: %s", str);
		goto error_empty;
	}

	/* key symbol */
	buttonstr = tokens->elements[tokens->size - 1];
	list_remove(tokens, buttonstr, NULL);
	if (button_parse_button(&_b.button, buttonstr) < 0)
		goto error_symstr;

	/* key modifiers */
	_b.mod = 0x0;
	LIST_FOR (tokens, i, modstr) {
		if (key_parse_modifier(&mod, modstr) < 0)
			goto error_modstr;
		_b.mod |= mod;
	}

	if (b)
		*b = _b;

	cstr_free(buttonstr);
	list_clear(tokens, (list_delfunc) cstr_free);
	list_free(tokens);
	return 0;

 error_modstr:
 error_symstr:
	cstr_free(buttonstr);
 error_empty:
	log_push();
	list_clear(tokens, (list_delfunc) cstr_free);
	list_free(tokens);
	log_pop();
 error_split:
	return -1;
}

int
button_parse_button(int unsigned *button, char const *str)
{
	int unsigned _button;

	if (cstr_equals(str, "Button1")) {
		_button = Button1;
	} else if (cstr_equals(str, "Button2")) {
		_button = Button2;
	} else if (cstr_equals(str, "Button3")) {
		_button = Button3;
	} else if (cstr_equals(str, "Button4")) {
		_button = Button4;
	} else if (cstr_equals(str, "Button5")) {
		_button = Button5;
	} else {
		log_set("Unknown button: %s", str);
		return -1;
	}
	if (button)
		*button = _button;
	return 0;
}
