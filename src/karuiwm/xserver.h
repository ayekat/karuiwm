#ifndef _KWM_XSERVER_H
#define _KWM_XSERVER_H

enum {
/* ICCCM Client Properties: https://tronche.com/gui/x/icccm/sec-4.html#s-4.1.2 */

	/* WM_NAME: https://tronche.com/gui/x/icccm/sec-4.html#s-4.1.2.1 */
	/* WM_ICON_NAME: https://tronche.com/gui/x/icccm/sec-4.html#s-4.1.2.2 */
	XATOM_WM_NAME,
	XATOM_WM_ICON_NAME,

	/* WM_NORMAL_HINTS: https://tronche.com/gui/x/icccm/sec-4.html#s-4.1.2.3 */
	XATOM_WM_NORMAL_HINTS,

	/* WM_HINTS: https://tronche.com/gui/x/icccm/sec-4.html#s-4.1.2.4 */
	XATOM_WM_HINTS,

	/* WM_CLASS: https://tronche.com/gui/x/icccm/sec-4.html#s-4.1.2.5 */
	XATOM_WM_CLASS,

	/* WM_TRANSIENT_FOR: https://tronche.com/gui/x/icccm/sec-4.html#s-4.1.2.6 */
	XATOM_WM_TRANSIENT_FOR,

	/* WM_PROTOCOLS: https://tronche.com/gui/x/icccm/sec-4.html#s-4.1.2.7 */
	XATOM_WM_PROTOCOLS,
	XATOM_WM_TAKE_FOCUS,
	XATOM_WM_DELETE_WINDOW,

	/* WM_COLORMAP_WINDOWS: https://tronche.com/gui/x/icccm/sec-4.html#s-4.1.2.8 */

	/* WM_CLIENT_MACHINE: https://tronche.com/gui/x/icccm/sec-4.html#s-4.1.2.9 */
	XATOM_WM_CLIENT_MACHINE,

/* ICCCM Window Manager Properties: https://tronche.com/gui/x/icccm/sec-4.html#s-4.1.3 */

	/* WM_STATE: https://tronche.com/gui/x/icccm/sec-4.html#s-4.1.3.1 */
	XATOM_WM_STATE,

	/* WM_ICON_SIZE: https://tronche.com/gui/x/icccm/sec-4.html#s-4.1.3.2 */
	XATOM_WM_ICON_SIZE,

/* ICCCM Obsolete Session Manager Conventions: https://tronche.com/gui/x/icccm/sec-C.html#s-C */
	/* (application properties) */
	XATOM_WM_COMMAND,

/* Other Properties */

	/* WM_LOCALE_NAME: Mentioned in XSetWMProperties(3) */
	XATOM_WM_LOCALE_NAME,

/* EWMH Root Window Properties: https://specifications.freedesktop.org/wm-spec/latest/ar01s03.html */

	/* _NET_ACTIVE_WINDOW: https://specifications.freedesktop.org/wm-spec/latest/ar01s03.html#idm46231959262240 */
	XATOM_NET_ACTIVE_WINDOW,

	/* _NET_CLIENT_LIST: https://specifications.freedesktop.org/wm-spec/latest/ar01s03.html#idm46231959283392 */

	/* _NET_CURRENT_DESKTOP: https://specifications.freedesktop.org/wm-spec/latest/ar01s03.html#idm46231959269136 */

	/* _NET_DESKTOP_GEOMETRY: https://specifications.freedesktop.org/wm-spec/latest/ar01s03.html#idm46231959276976 */

	/* _NET_DESKTOP_LAYOUT: https://specifications.freedesktop.org/wm-spec/latest/ar01s03.html#idm46231959248976 */

	/* _NET_DESKTOP_NAMES: https://specifications.freedesktop.org/wm-spec/latest/ar01s03.html#idm46231959265760 */

	/* _NET_DESKTOP_VIEWPORT: https://specifications.freedesktop.org/wm-spec/latest/ar01s03.html#idm46231959272848 */

	/* _NET_NUMBER_OF_DESKTOPS: https://specifications.freedesktop.org/wm-spec/latest/ar01s03.html#idm46231959281456 */

	/* _NET_SHOWING_DESKTOP: https://specifications.freedesktop.org/wm-spec/latest/ar01s03.html#idm46231959234528 */

	/* _NET_SUPPORTED: https://specifications.freedesktop.org/wm-spec/latest/ar01s03.html#idm46231959285024 */
	XATOM_NET_SUPPORTED,

	/* _NET_SUPPORTING_WM_CHECK: https://specifications.freedesktop.org/wm-spec/latest/ar01s03.html#idm46231959254112 */

	/* _NET_VIRTUAL_ROOTS: https://specifications.freedesktop.org/wm-spec/latest/ar01s03.html#idm46231959250752 */

	/* _NET_WORKAREA: https://specifications.freedesktop.org/wm-spec/latest/ar01s03.html#idm46231959257680 */

/* EWMH Other Root Window Messages: https://specifications.freedesktop.org/wm-spec/latest/ar01s04.html */

	/* _NET_CLOSE_WINDOW: https://specifications.freedesktop.org/wm-spec/latest/ar01s04.html#idm46231959230512 */

	/* _NET_MOVERESIZE_WINDOW: https://specifications.freedesktop.org/wm-spec/latest/ar01s04.html#idm46231959226288 */

	/* _NET_REQUEST_FRAME_EXTENTS: https://specifications.freedesktop.org/wm-spec/latest/ar01s04.html#idm46231959209312 */
	XATOM_NET_REQUEST_FRAME_EXTENTS,

	/* _NET_RESTACK_WINDOW: https://specifications.freedesktop.org/wm-spec/latest/ar01s04.html#idm46231959214208 */

	/* _NET_WM_MOVERESIZE: https://specifications.freedesktop.org/wm-spec/latest/ar01s04.html#idm46231959221744 */

/* EWMH Application Window properties: https://specifications.freedesktop.org/wm-spec/latest/ar01s05.html */

	/* _NET_FRAME_EXTENTS: https://specifications.freedesktop.org/wm-spec/latest/ar01s05.html#idm46231959117408 */

	/* _NET_WM_ALLOWED_ACTIONS: https://specifications.freedesktop.org/wm-spec/latest/ar01s05.html#idm46231959156752 */

	/* _NET_WM_BYPASS_COMPOSITOR: https://specifications.freedesktop.org/wm-spec/latest/ar01s05.html#idm46231959113200 */
	XATOM_NET_WM_BYPASS_COMPOSITOR,

	/* _NET_WM_DESKTOP: https://specifications.freedesktop.org/wm-spec/latest/ar01s05.html#idm46231959196560 */

	/* _NET_WM_HANDLED_ICONS: https://specifications.freedesktop.org/wm-spec/latest/ar01s05.html#idm46231959126656 */

	/* _NET_WM_ICON: https://specifications.freedesktop.org/wm-spec/latest/ar01s05.html#idm46231959132960 */

	/* _NET_WM_ICON_GEOMETRY: https://specifications.freedesktop.org/wm-spec/latest/ar01s05.html#idm46231959135504 */

	/* _NET_WM_ICON_NAME: https://specifications.freedesktop.org/wm-spec/latest/ar01s05.html#idm46231959200192 */
	/* _NET_WM_VISIBLE_ICON_NAME: https://specifications.freedesktop.org/wm-spec/latest/ar01s05.html#idm46231959198368 */
	XATOM_NET_WM_ICON_NAME,

	/* _NET_WM_NAME: https://specifications.freedesktop.org/wm-spec/latest/ar01s05.html#idm46231959205200 */
	/* _NET_WM_VISIBLE_NAME: https://specifications.freedesktop.org/wm-spec/latest/ar01s05.html#idm46231959203280 */
	XATOM_NET_WM_NAME,

	/* _NET_WM_OPAQUE_REGION: https://specifications.freedesktop.org/wm-spec/latest/ar01s05.html#idm46231959115488 */
	XATOM_NET_WM_OPAQUE_REGION,

	/* _NET_WM_PID: https://specifications.freedesktop.org/wm-spec/latest/ar01s05.html#idm46231959130528 */

	/* _NET_WM_STATE: https://specifications.freedesktop.org/wm-spec/latest/ar01s05.html#idm46231959178000 */
	XATOM_NET_WM_STATE,
	XATOM_NET_WM_STATE_ABOVE,
	XATOM_NET_WM_STATE_BELOW,
	XATOM_NET_WM_STATE_DEMANDS_ATTENTION,
	XATOM_NET_WM_STATE_FULLSCREEN,
	XATOM_NET_WM_STATE_HIDDEN,
	XATOM_NET_WM_STATE_MAXIMIZED_HORZ,
	XATOM_NET_WM_STATE_MAXIMIZED_VERT,
	XATOM_NET_WM_STATE_MODAL,
	XATOM_NET_WM_STATE_SHADED,
	XATOM_NET_WM_STATE_SKIP_PAGER,
	XATOM_NET_WM_STATE_SKIP_TASKBAR,
	XATOM_NET_WM_STATE_STICKY,

	/* _NET_WM_STRUT: https://specifications.freedesktop.org/wm-spec/latest/ar01s05.html#idm46231959143520 */
	/* _NET_WM_STRUT_PARTIAL: https://specifications.freedesktop.org/wm-spec/latest/ar01s05.html#idm46231959141440 */
	XATOM_NET_WM_STRUT,
	XATOM_NET_WM_STRUT_PARTIAL,

	/* _NET_WM_USER_TIME: https://specifications.freedesktop.org/wm-spec/latest/ar01s05.html#idm46231959124640 */
	/* _NET_WM_USER_TIME_WINDOW: https://specifications.freedesktop.org/wm-spec/latest/ar01s05.html#idm46231959120064 */
	XATOM_NET_WM_USER_TIME,

	/* _NET_WM_WINDOW_TYPE: https://specifications.freedesktop.org/wm-spec/latest/ar01s05.html#idm46231959190688 */
	XATOM_NET_WM_WINDOW_TYPE,
	XATOM_NET_WM_WINDOW_TYPE_DESKTOP,
	XATOM_NET_WM_WINDOW_TYPE_DOCK,
	XATOM_NET_WM_WINDOW_TYPE_TOOLBAR,
	XATOM_NET_WM_WINDOW_TYPE_MENU,
	XATOM_NET_WM_WINDOW_TYPE_UTILITY,
	XATOM_NET_WM_WINDOW_TYPE_SPLASH,
	XATOM_NET_WM_WINDOW_TYPE_DIALOG,
	XATOM_NET_WM_WINDOW_TYPE_DROPDOWN_MENU,
	XATOM_NET_WM_WINDOW_TYPE_POPUP_MENU,
	XATOM_NET_WM_WINDOW_TYPE_TOOLTIP,
	XATOM_NET_WM_WINDOW_TYPE_NOTIFICATION,
	XATOM_NET_WM_WINDOW_TYPE_COMBO,
	XATOM_NET_WM_WINDOW_TYPE_DND,
	XATOM_NET_WM_WINDOW_TYPE_NORMAL,

/* EWMH Window Manager Protocols: https://specifications.freedesktop.org/wm-spec/latest/ar01s06.html */

	/* _NET_WM_FULLSCREEN_MONITORS: https://specifications.freedesktop.org/wm-spec/latest/ar01s06.html#idm46231959098176 */

	/* _NET_WM_PING: https://specifications.freedesktop.org/wm-spec/latest/ar01s06.html#idm46231959110112 */

	/* _NET_WM_SYNC_REQUEST: https://specifications.freedesktop.org/wm-spec/latest/ar01s06.html#idm46231959104816 */

/* EWMH Other Properties: https://specifications.freedesktop.org/wm-spec/latest/ar01s07.html */

	/* _NET_WM_FULL_PLACEMENT: https://specifications.freedesktop.org/wm-spec/latest/ar01s07.html#idm46231959088704 */

/* EWMH Compositing Managers: https://specifications.freedesktop.org/wm-spec/latest/ar01s08.html#idm46231959077968 */

	/* _NET_WM_CM_Sn Manager Selection: https://specifications.freedesktop.org/wm-spec/latest/ar01s08.html#idm46231959082016 */

	/* WM_TRANSIENT_FOR for override-redirect windows: https://specifications.freedesktop.org/wm-spec/latest/ar01s08.html#idm46231959077968 */

	XATOM_LAST
};

enum xserver_request_type {
	XSERVER_REQUEST_CONFIGURE,
	XSERVER_REQUEST_MAP,
};

enum xserver_request_handler_result {
	XSERVER_REQUEST_UNHANDLED,
	XSERVER_REQUEST_PASSTHROUGH,
	XSERVER_REQUEST_HANDLED,
	XSERVER_REQUEST_REJECTED,
	XSERVER_REQUEST_FAILED,
};

#include <X11/Xlib.h>

struct xserver {
	/* connection to X server */
	Display *display;
	int fd;

	/* root and other windows */
	struct xwindow *root;
	GC graphics_context; /* TODO: move into root window */
	struct list *windows;

	/* known atoms */
	Atom atoms[XATOM_LAST];
};

#include <karuiwm/rectangle.h>

union xserver_request {
	enum xserver_request_type type;
	struct {
		enum xserver_request_type type;
		struct xwindow *xwindow;
		struct rectangle dimension;
	} configure;
	struct {
		enum xserver_request_type type;
		struct xwindow *xwindow;
	} map;
};

#include <karuiwm/xwindow.h>

int xserver_init(void);
void xserver_deinit(void);

int xserver_add_request_handler(enum xserver_request_type reqtype,
                                int (*cb)(union xserver_request const *,
                                          enum xserver_request_handler_result *,
                                          void *),
                                void *ctx);
struct xwindow *xserver_create_xwindow(struct rectangle dim,
                                       int unsigned border_width,
                                       int long unsigned event_mask,
                                       bool override_redirect);
void xserver_destroy_xwindow(struct xwindow *xwin);
struct xwindow *xserver_find_xwindow(Window id);
void xserver_remove_request_handler(enum xserver_request_type reqtype,
                                int (*cb)(union xserver_request const *,
                                          enum xserver_request_handler_result *,
                                          void *));

extern struct xserver xserver;

#endif /* ndef _KWM_XSERVER_H */
