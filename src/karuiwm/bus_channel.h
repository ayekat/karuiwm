#ifndef _KWM_BUS_CHANNEL_H
#define _KWM_BUS_CHANNEL_H

struct bus_channel {
	char *name;
	struct list *subscriptions;
};

#include <karuiwm/bus_session.h>
#include <stdbool.h>

struct bus_channel *bus_channel_alloc(char const *name);
void bus_channel_free(struct bus_channel *ch);

bool bus_channel_has_name(struct bus_channel const *ch, char const *name);
void bus_channel_send(struct bus_channel const *ch, void *data);

#endif /* ndef _KWM_BUS_CHANNEL_H */
