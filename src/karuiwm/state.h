#ifndef _KWM_STATE_H
#define _KWM_STATE_H

#include <stdint.h>

struct state {
	struct list *workspaces;
	struct workspace *scratchpad;
	struct {
		char *savefile;
		/* FIXME move away by splitting client meta/view information */
		int unsigned client_border_width;
		uint32_t client_border_colour_focused;
		uint32_t client_border_colour_unfocused;
	} config;
};

#include <karuiwm/client.h>
#include <karuiwm/workspace.h>
#include <stdbool.h>

int state_init(void);
void state_deinit(bool restart);

int state_attach_workspace(struct workspace *ws);
int state_clean_up(void);
void state_detach_workspace(struct workspace *ws);
struct client *state_find_client(struct xwindow const *xwin);
bool state_locate_workspace(struct workspace **ws, char const *name);
struct workspace *state_next_free_workspace(void);
struct workspace *state_recommend_workspace(struct client *c);
int state_rename_workspace(struct workspace *ws, char const *name);
int state_set_client_border_colour_focused(uint32_t col);
int state_set_client_border_colour_unfocused(uint32_t col);
int state_set_client_border_width(int unsigned bw);
int state_set_clientmask(bool set);

extern struct state state;

#endif /* ndef _KWM_STATE_H */
