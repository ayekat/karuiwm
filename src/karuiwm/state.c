#include <karuiwm/state.h>
#include <karuiwm/bus.h>
#include <karuiwm/commandline.h>
#include <karuiwm/karuiwm.h>
#include <karuiwm/monitor.h>
#include <karuiwm/xserver.h>
#include <karuiwm/xwindow.h>
#include <karui/cstr.h>
#include <karui/list.h>
#include <karui/log.h>
#include <karui/memory.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

static int _init(void);
static FILE *_savefile_create(void);
static int _savefile_scan(void);
static int _savefile_scan_client(FILE *fs, struct workspace *ws);
static int _savefile_scan_state(FILE *fs);
static int _savefile_scan_workspace(FILE *fs, bool is_scratchpad);
static int _savefile_write(void);
static int _server_scan(void);
static int _server_scan_window(struct xwindow *xwin);
static int _update_commandline(void);

static char const *_CLA_SAVEFILE = "state.savefile";

struct state state;

int
state_init(void)
{
	state.workspaces = list_alloc();
	if (!state.workspaces) {
		log_set("Could not create workspace list: %s", log_str());
		goto error_workspaces;
	}

	state.scratchpad = workspace_alloc();
	if (!state.scratchpad) {
		log_set("Could not create scratchpad workspace: %s", log_str());
		goto error_scratchpad;
	}
	state.scratchpad->nofloat = true;

	state.config.savefile = NULL;
	state.config.client_border_width = 1;
	state.config.client_border_colour_focused = 0xAFD700;
	state.config.client_border_colour_unfocused = 0x888888;

	if (_init() < 0)
		goto error_init;

	return 0;

 error_init:
	log_push();
	workspace_free(state.scratchpad);
	log_pop();
 error_scratchpad:
	log_push();
	list_free(state.workspaces);
	log_pop();
 error_workspaces:
	return -1;
}

void
state_deinit(bool restart)
{
	struct workspace *ws;
	struct client *c;

	if (restart) {
		if (_savefile_write() < 0)
			CRITICAL("Could not save state to savefile: %s",
			         log_str());
		else if (_update_commandline() < 0)
			CRITICAL("Could not set state savefile in commandline: %s",
			         log_str());
	}

	while (state.workspaces->size > 0) {
		ws = state.workspaces->elements[0];
		while (ws->tiled->size > 0) {
			c = ws->tiled->elements[0];
			workspace_detach_client(ws, c);
			client_free(c);
		}
		while (ws->floating->size > 0) {
			c = ws->floating->elements[0];
			workspace_detach_client(ws, c);
			client_free(c);
		}
		state_detach_workspace(ws);
		workspace_free(ws);
	}
	list_free(state.workspaces);

	while (state.scratchpad->tiled->size > 0) {
		c = state.scratchpad->tiled->elements[0];
		workspace_detach_client(state.scratchpad, c);
		client_free(c);
	}
	workspace_free(state.scratchpad);
}

int
state_attach_workspace(struct workspace *ws)
{
	list_insert_sorted(state.workspaces, ws,
	                   (list_cmpfunc) workspace_compare_name);

	bus_channel_send(bus.workspace_attach, ws);

	return 0;
}

int
state_clean_up(void)
{
	int unsigned i;
	struct workspace *ws;

	/* remove non-visible workspaces that are empty */
	LIST_FOR (state.workspaces, i, ws) {
		if (ws->mon || !workspace_isempty(ws))
			continue;

		state_detach_workspace(ws);
		--i; /* XXX we're inside a loop */
		workspace_free(ws);
	}

	/* hide scratchpad if empty */
	if (state.scratchpad->mon && workspace_isempty(state.scratchpad)) {
		DEBUG("Scratchpad is empty, hiding");
		if (monitor_show_scratchpad(state.scratchpad->mon, NULL) < 0)
			WARN("Could not hide empty scratchpad: %s", log_str());
	}

	return 0;
}

void
state_detach_workspace(struct workspace *ws)
{
	bus_channel_send(bus.workspace_detach, ws);

	if (!list_contains(state.workspaces, ws, NULL)) {
		BUG("Attempt to detach non-existent workspace");
		return;
	}

	list_remove(state.workspaces, ws, NULL);
}

struct client *
state_find_client(struct xwindow const *xwin)
{
	unsigned int i, j;
	struct workspace *ws;
	struct client *c;

	LIST_FOR (state.scratchpad->tiled, j, c)
		if (c->xwindow == xwin)
			return c;

	LIST_FOR (state.workspaces, i, ws) {
		LIST_FOR (ws->tiled, j, c)
			if (c->xwindow == xwin)
				return c;
		LIST_FOR (ws->floating, j, c)
			if (c->xwindow == xwin)
				return c;
	}

	return NULL;
}

bool
state_locate_workspace(struct workspace **ws, char const *name)
{
	unsigned int i;
	struct workspace *_ws;

	LIST_FOR (state.workspaces, i, _ws) {
		if (cstr_equals(_ws->name, name)) {
			if (ws)
				*ws = _ws;
			return true;
		}
	}
	return false;
}

struct workspace *
state_next_free_workspace(void)
{
	int unsigned i;
	struct workspace *ws;

	LIST_FOR (state.workspaces, i, ws)
		if (!ws->mon)
			return ws;
	return NULL;
}

struct workspace *
state_recommend_workspace(struct client *c)
{
	struct client *tr4c;

	/* transient window: recommend same workspace as transient-for */
	if (c->xwindow->transient_for) {
		tr4c = c->xwindow->transient_for->client;
		if (tr4c && tr4c->ws) {
			DEBUG("Client %lu is transient for client %lu; recommending workspace '%s'",
			      c->xwindow->id, tr4c->xwindow->id,
			      tr4c->ws->name);
			return tr4c->ws;
		}
	}

	/* nothing special: no recommendation */
	return NULL;
}

int
state_rename_workspace(struct workspace *ws, char const *name)
{
	struct workspace *otherws;

	if (!list_find(state.workspaces, ws, NULL)) {
		/* can happen for scratchpad */
		log_set("Workspace is not part of state");
		return -1;
	}

	if (state_locate_workspace(&otherws, name)) {
		if (otherws == ws) {
			DEBUG("Workspace is already named '%s', not changing",
			      name);
			return 0;
		} else {
			log_set("Workspace name '%s' already in use", name);
			return -1;
		}
	}

	workspace_rename(ws, name);

	/* reinsert to keep workspaces sorted by name
	 * (TODO: Use list_reinsert_sorted() once that becomes a thing)
	 */
	list_remove(state.workspaces, ws, NULL);
	list_insert_sorted(state.workspaces, ws,
	                   (list_cmpfunc) workspace_compare_name);

	bus_channel_send(bus.workspace_rename, ws);

	return 0;
}

int
state_set_client_border_colour_focused(uint32_t col)
{
	struct workspace *ws;
	struct client *c;
	int unsigned wi, ci;

	state.config.client_border_colour_focused = col;

	LIST_FOR (state.scratchpad->tiled, ci, c)
		if (client_set_border_colour_focused(c, col) < 0)
			ERROR("Could not set focused border colour on client %lu to #%06X: %s",
			      c->xwindow->id, col, log_str());
	LIST_FOR (state.workspaces, wi, ws) {
		LIST_FOR (ws->tiled, ci, c)
			if (client_set_border_colour_focused(c, col) < 0)
				ERROR("Could not set focused border colour on client %lu to #%06X: %s",
				      c->xwindow->id, col, log_str());
		LIST_FOR (ws->floating, ci, c)
			if (client_set_border_colour_focused(c, col) < 0)
				ERROR("Could not set focused border colour on client %lu to #%06X: %s",
				      c->xwindow->id, col, log_str());
	}
	return 0;
}

int
state_set_client_border_colour_unfocused(uint32_t col)
{
	struct workspace *ws;
	struct client *c;
	int unsigned wi, ci;

	state.config.client_border_colour_unfocused = col;

	LIST_FOR (state.scratchpad->tiled, ci, c)
		if (client_set_border_colour_unfocused(c, col) < 0)
			ERROR("Could not set unfocused border colour on client %lu to #%06X: %s",
			      c->xwindow->id, col, log_str());
	LIST_FOR (state.workspaces, wi, ws) {
		LIST_FOR (ws->tiled, ci, c)
			if (client_set_border_colour_unfocused(c, col) < 0)
				ERROR("Could not set unfocused border colour on client %lu to #%06X: %s",
				      c->xwindow->id, col, log_str());
		LIST_FOR (ws->floating, ci, c)
			if (client_set_border_colour_unfocused(c, col) < 0)
				ERROR("Could not set unfocused border colour on client %lu to #%06X: %s",
				      c->xwindow->id, col, log_str());
	}
	return 0;
}

int
state_set_client_border_width(int unsigned bw)
{
	struct workspace *ws;
	struct client *c;
	int unsigned wi, ci;

	state.config.client_border_width = bw;

	LIST_FOR (state.scratchpad->tiled, ci, c)
		if (client_set_border_width(c, bw) < 0)
			ERROR("Could not set border width on client %lu to %u: %s",
			      c->xwindow->id, bw, log_str());
	LIST_FOR (state.workspaces, wi, ws) {
		LIST_FOR (ws->tiled, ci, c)
			if (client_set_border_width(c, bw) < 0)
				ERROR("Could not set border width on client %lu %u: %s",
				      c->xwindow->id, bw, log_str());
		LIST_FOR (ws->floating, ci, c)
			if (client_set_border_width(c, bw) < 0)
				ERROR("Could not set border width on client %lu to %u %s",
				      c->xwindow->id, bw, log_str());
	}
	return 0;
}

int
state_set_clientmask(bool set)
{
	struct workspace *ws;
	struct client *c;
	int unsigned i, j;

	LIST_FOR (state.scratchpad->tiled, i, c)
		XSelectInput(xserver.display, c->xwindow->id, set ? CLIENTMASK : 0);
	LIST_FOR (state.workspaces, j, ws) {
		LIST_FOR (ws->tiled, i, c)
			XSelectInput(xserver.display, c->xwindow->id,
			             set ? CLIENTMASK : 0);
		LIST_FOR (ws->floating, i, c)
			XSelectInput(xserver.display, c->xwindow->id,
			             set ? CLIENTMASK : 0);
	}

	return 0;
}

static int
_init(void)
{
	struct workspace *initws = NULL;

	if (_savefile_scan() < 0) {
		log_set("Could not restore state from savefile: %s", log_str());
		goto error_savefile;
	}

	/* No savefile passed (or loading failed, or otherwise ended up with 0
	 * workspaces); create an initial workspace to work with.
	 */
	if (state.workspaces->size == 0) {
		DEBUG("No workspaces existing, creating initial one");
		initws = workspace_alloc();
		if (!initws) {
			log_set("Could not create initial workspace: %s",
			        log_str());
			goto error_initws;
		}
		if (state_attach_workspace(initws) < 0) {
			log_set("Could not attach initial workspace to state: %s",
			        log_str());
		}
	}

	if (_server_scan() < 0) {
		log_set("Could not scan X server for pre-existing X windows: %s",
		        log_str());
		goto error_xserver;
	}

	return 0;

 error_xserver:
	/* TODO: clean-up? */
 error_initws:
	/* TODO: clean-up? */
 error_savefile:
	return -1;
}

static FILE *
_savefile_create(void)
{
	char path_template[] = "/tmp/karuiwm.state-XXXXXX";
	int fd;
	FILE *fs;

	fd = mkstemp(path_template);
	if (fd < 0) {
		log_set("Could not create temporary file: %s", strerror(errno));
		goto error_mkstemp;
	}
	DEBUG("Created new state savefile %s (fd=%d)", path_template, fd);

	state.config.savefile = cstr_dup(path_template);
	if (!state.config.savefile) {
		log_set("Could not duplicate savefile path: %s", log_str());
		goto error_pathdup;
	}

	fs = fdopen(fd, "w");
	if (!fs) {
		log_set("Could not open file stream from fd=%d: %s",
		        fd, strerror(errno));
		goto error_fdopen;
	}
	return fs;

 error_fdopen:
	log_push();
	cstr_free(state.config.savefile);
	state.config.savefile = NULL;
	log_pop();
 error_pathdup:
	close(fd);
 error_mkstemp:
	return NULL;
}

static int
_savefile_scan(void)
{
	FILE *fs;
	char const *savefile;

	savefile = commandline_get(_CLA_SAVEFILE);
	if (!savefile) {
		log_set("No state savefile given, nothing to restore from file");
		return 0;
	}


	fs = fopen(savefile, "r");
	if (!fs) {
		log_set("Could not open savefile %s for reading: %s",
		        savefile, strerror(errno));
		goto error_savefile_open;
	}

	if (_savefile_scan_state(fs) < 0) {
		log_set("Could not scan state from savefile: %s", log_str());
		goto error_savefile_scan;
	}

	if (fclose(fs) < 0)
		WARN("Could not close savefile %s after reading: %s",
		     savefile, strerror(errno));
	return 0;

 error_savefile_scan:
	if (fclose(fs) < 0) {
		log_push();
		WARN("Could not close savefile %s after reading: %s",
		     savefile, strerror(errno));
		log_pop();
	}
 error_savefile_open:
	return -1;
}

static int
_savefile_scan_client(FILE *fs, struct workspace *ws)
{
	int sret;
	Window id;
	struct client *c;
	struct rectangle dim;
	int floating, fullscreen;
	struct xwindow *xwin;

	/* read client */
	errno = 0;
	sret = fscanf(fs, "  %lu:%d:%d:%u:%u:%d:%d\n",
	                  &id, &dim.x, &dim.y, &dim.width, &dim.height,
	                  (int *) &floating, (int *) &fullscreen);
	if (sret < 7) {
		if (sret == EOF) {
			if (errno == 0)
				log_set("Reached end of savefile while matching");
			else
				log_set("Could not read from savefile: %s",
				        strerror(errno));
		} else {
			log_set("Could not match values (%d matches instead of 7)",
			        sret);
		}
		goto error_match;
	} else if (sret > 7) {
		BUG("Matched more values than expected (%d matches instead of 7)",
		    sret);
	}

	/* identify X window */
	xwin = xserver_find_xwindow(id);
	if (!xwin) {
		WARN("X window %lu found in savefile, but not in X server; ignoring",
		     id);
		return 0;
	}

	if (xwin->client) {
		WARN("Duplicate X window %lu (workspace '%s') found in X server, not re-managing",
		     xwin->id, xwin->client->ws->name);
		return 0;
	}

	/* create and attach client */
	c = client_alloc(xwin);
	if (!c) {
		log_set("Could not create client for X window %lu: %s",
		        xwin->id, log_str());
		goto error_create;
	}
	c->floating = (bool) floating;
	c->dim_floating = dim;

	DEBUG("  › Restoring %s client to %ux%u%+d%+d (base=%ux%u, inc=%u,%u)",
	      c->floating ? "floating" : "non-floating",
	      dim.width, dim.height, dim.x, dim.y,
	      c->xwindow->sizehints.base_width, c->xwindow->sizehints.base_height,
	      c->xwindow->sizehints.inc_width, c->xwindow->sizehints.inc_height);
	if (client_moveresize_floating(c, dim) < 0)
		WARN("Could not restore X window dimensions for client %lu: %s",
		     c->xwindow->id, log_str());

	if (workspace_attach_client(ws, c) < 0) {
		log_set("Could not attach client for X window %lu to workspace: %s",
		        c->xwindow->id, log_str());
		goto error_attach;
	}

	DEBUG("Restored X window %lu with name '%s'",
	      c->xwindow->id, c->xwindow->name);
	return 0;

 error_attach:
	log_push();
	client_free(c);
	log_pop();
 error_create:
 error_match:
	return -1;
}

static int
_savefile_scan_state(FILE *fs)
{
	int sret;
	size_t nws;
	unsigned int i;

	/* read number of workspaces */
	errno = 0;
	sret = fscanf(fs, "%zu\n", &nws);
	if (sret < 1) {
		if (sret == EOF) {
			if (errno == 0)
				log_set("Reached end of savefile while reading number of workspaces");
			else
				log_set("Could not read from savefile: %s",
				        strerror(errno));
		} else {
			log_set("Could not match number of workspaces (%d matches instead of 1)",
			        sret);
		}
		goto error_read;
	} else if (sret > 1) {
		BUG("Matched more than the number of workspaces (%d matches instead of 1)",
		    sret);
	}

	DEBUG("Scanning %zu workspaces...", nws);
	for (i = 0; i < nws; i++) {
		if (_savefile_scan_workspace(fs, false) < 0) {
			log_set("Could not scan workspace %d: %s",
			        i, log_str());
			goto error_workspace;
		}
	}

	DEBUG("Scanning scratchpad");
	if (_savefile_scan_workspace(fs, true) < 0) {
		log_set("Could not read scratchpad workspace: %s", log_str());
		goto error_workspace;
	}

	return 0;

 error_workspace:
	/* TODO: cleanup? */
 error_read:
	return -1;
}

static int
_savefile_scan_workspace(FILE *fs, bool is_scratchpad)
{
	int sret, sret_expected;
	size_t nc;
	char name[256];
	struct workspace *ws;
	bool newws;
	size_t i;

	/* read workspace */
	errno = 0;
	if (is_scratchpad) {
		sret_expected = 1;
		sret = fscanf(fs, "%zu\n", &nc);
	} else {
		sret_expected = 2;
		sret = fscanf(fs, "%zu:%s\n", &nc, name);
	}
	if (sret < sret_expected) {
		if (sret == EOF) {
			if (errno == 0)
				log_set("Reached end of savefile while matching");
			else
				log_set("Could not read from savefile: %s",
				        strerror(errno));
		} else {
			log_set("Could not match values (%d matches instead of %d)",
			        sret, sret_expected);
		}
		goto error_match;
	} else if (sret > sret_expected) {
		BUG("Matched more values than expected (%d matches instead of %d)",
		    sret, sret_expected);
	}

	/* create and attach workspace */
	newws = true;
	if (is_scratchpad) {
		ws = state.scratchpad;
	} else {
		if (state_locate_workspace(&ws, name)) {
			WARN("Duplicate workspace '%s', not recreating", name);
			newws = false;
			/* we just continue using this one now */
		} else {
			ws = workspace_alloc();
			if (!ws) {
				log_set("Could not create workspace '%s': %s",
				        name, log_str());
				goto error_create;
			}
			workspace_rename(ws, name);
			if (state_attach_workspace(ws) < 0) {
				log_set("Could not attach workspace '%s' to state: %s",
				        ws->name, log_str());
				goto error_attach;
			}
		}
	}

	/* scan clients */
	DEBUG("Scanning %zu clients...", nc);
	for (i = 0; i < nc; ++i) {
		if (_savefile_scan_client(fs, ws) < 0) {
			log_set("Could not read client %zu: %s", i, log_str());
			goto error_client;
		}
	}

	return 0;

 error_client:
	if (newws) {
		log_push();
		while (ws->tiled->size > 0)
			workspace_detach_client(ws, ws->tiled->elements[0]);
		while (ws->floating->size > 0)
			workspace_detach_client(ws, ws->floating->elements[0]);
		log_pop();
	}
 error_attach:
	if (newws) {
		log_push();
		workspace_free(ws);
		log_pop();
	}
 error_create:
 error_match:
	return -1;
}

static int
_savefile_write(void)
{
	FILE *fs;
	unsigned int i, j;
	struct client *c;
	struct workspace *ws;

	/* create new savefile */
	fs = _savefile_create();
	if (!fs) {
		log_set("Could not create savefile: %s", log_str());
		goto error_savefile_create;
	}

	/* save number of workspaces */
	if (fprintf(fs, "%zu\n", state.workspaces->size) < 0) {
		log_set("Could not write number of workspaces to savefile: %s",
		        strerror(errno));
		goto error_write;
	}

	/* save workspaces */
	DEBUG("Storing %zu workspaces:", state.workspaces->size);
	LIST_FOR (state.workspaces, i, ws) {
		/* workspace itself (name + number of clients) */
		DEBUG("- storing workspace[%u]: '%s' (%zu clients)",
		     i, ws->name,
		     ws->tiled->size + ws->floating->size);
		if (fprintf(fs, "%zu:%s\n",
		                ws->tiled->size + ws->floating->size,
		                ws->name) < 0)
		{
			log_set("Could not write workspace information for '%s': %s",
			        ws->name, strerror(errno));
			goto error_write;
		}

		/* clients (tiled) */
		LIST_FOR (ws->tiled, j, c) {
			DEBUG("  storing tiled client '%s' (%lu, dim=%ux%u%+d%+d)",
			      c->xwindow->name, c->xwindow->id,
			      c->dim_floating.width, c->dim_floating.height,
			      c->dim_floating.x, c->dim_floating.y);
			if (fprintf(fs, "%lu:%d:%d:%d:%d:%d:%d\n",
			                c->xwindow->id,
			                c->dim_floating.x, c->dim_floating.y,
			                c->dim_floating.width,
			                c->dim_floating.height,
			                c->floating, c->fullscreen) < 0)
			{
				log_set("Could not write information for tiled client %lu: %s",
				        c->xwindow->id, strerror(errno));
				goto error_write;
			}
		}

		/* clients (floating) */
		LIST_FOR (ws->floating, j, c) {
			DEBUG("  storing floating client '%s' (%lu, dim=%ux%u%+d%+d)",
			      c->xwindow->name, c->xwindow->id,
			      c->dim_floating.width, c->dim_floating.height,
			      c->dim_floating.x, c->dim_floating.y);
			if (fprintf(fs, "%lu:%d:%d:%d:%d:%d:%d\n",
			                c->xwindow->id,
			                c->dim_floating.x, c->dim_floating.y,
			                c->dim_floating.width,
			                c->dim_floating.height,
			                c->floating, c->fullscreen) < 0)
			{
				log_set("Could not write information for floating client %lu: %s",
				        c->xwindow->id, strerror(errno));
				goto error_write;
			}
		}
	}

	/* save scratchpad */
	if (fprintf(fs, "%zu\n", state.scratchpad->tiled->size) < 0) {
		log_set("Could not write workspace information for scratchpad: %s",
		        strerror(errno));
		goto error_write;
	}

	/* save scratchpad clients */
	LIST_FOR(state.scratchpad->tiled, j, c) {
		DEBUG("  storing tiled client '%s' (%lu)",
		      c->xwindow->name, c->xwindow->id);
		if (fprintf(fs, "  %lu:%d:%d:%d:%d:%d:%d\n",
		                c->xwindow->id,
		                c->dim_floating.x, c->dim_floating.y,
		                c->dim_floating.width, c->dim_floating.height,
		                c->floating, c->fullscreen) < 0)
		{
			log_set("Could not write information for scratchpad client %lu: %s",
			        c->xwindow->id, strerror(errno));
			goto error_write;
		}
	}

	fclose(fs);
	return 0;

 error_write:
	fclose(fs);
 error_savefile_create:
	return -1;
}

static int
_server_scan(void)
{
	unsigned int i;
	struct xwindow *xwin;

	LIST_FOR (xserver.windows, i, xwin) {
		if (_server_scan_window(xwin) < 0) {
			ERROR("Could not scan X window %lu: %s",
			      xwin->id, log_str());
			continue;
		}
	}

	return 0;
}

static int
_server_scan_window(struct xwindow *xwin)
{
	struct client *c;
	struct workspace *initws;

	if (!xwindow_is_manageable(xwin) || !xwindow_is_viewable(xwin))
		return 0;

	c = state_find_client(xwin);
	if (c)
		return 0;

	/* determine workspace to attach found windows */
	initws = state.workspaces->elements[0];

	c = client_alloc(xwin);
	if (!c) {
		log_set("Could not create client for X window %lu: %s",
		        xwin->id, log_str());
		goto error_client;
	}

	WARN("› X Window %lu: Unhandled %s window found, managing now",
	     c->xwindow->id, c->visible ? "visible" : "invisible");
	if (workspace_attach_client(initws, c) < 0) {
		log_set("Could not attach unhandled X window %lu to workspace '%s': %s",
		        c->xwindow->id, initws->name, log_str());
		goto error_attach;
	}

	return 0;

 error_attach:
	log_push();
	client_free(c);
	log_pop();
 error_client:
	return -1;
}

static int
_update_commandline(void)
{
	if (!state.config.savefile) {
		DEBUG("No savefile set");
		/* TODO: Remove state.savefile from commandline */
		return 0;
	}

	if (commandline_set(_CLA_SAVEFILE, state.config.savefile) < 0) {
		log_set("Could not set savefile argument in commandline: %s",
		        log_str());
		goto error_add;
	}

	return 0;

 error_add:
	return -1;
}
