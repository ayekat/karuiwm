#ifndef _KWM_FDIO_H
#define _KWM_FDIO_H

#include <karuiwm/fdio_client.h>

int fdio_init(void);
void fdio_deinit(void);

int fdio_add_fd(struct fdio_client *c, int *fd);
struct fdio_client *fdio_register(char const *name,
                                  int (*callback)(int, void *), void *data);
void fdio_remove_fd(struct fdio_client *c, int *fd);
void fdio_unregister(struct fdio_client *c);
int fdio_wait(void);

#endif /* ndef _KWM_FDIO_H */
