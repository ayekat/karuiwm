#ifndef _KWM_CLIENT_H
#define _KWM_CLIENT_H

#include <karuiwm/rectangle.h>
#include <stdbool.h>
#include <stdint.h>
#include <X11/Xlib.h>

struct client {
	struct xwindow *xwindow;
	struct rectangle dim_floating;
	bool floating, fullscreen, dialog, splash;
	struct {
		int unsigned width;
		uint32_t colour_focused;
		uint32_t colour_unfocused;
	} border;
	struct workspace *ws;
	bool focused, visible;
	struct bus_session *bus_session;
};

#include <karuiwm/state.h>

struct client *client_alloc(struct xwindow *xwin);
void client_free(struct client *c);

void client_get_sizehints(struct client const *c, struct size *size);
void client_kill(struct client *c);
int client_move(struct client *c, struct position pos);
int client_move_floating(struct client *c, struct position pos);
int client_moveresize(struct client *c, struct rectangle dim);
int client_moveresize_floating(struct client *c, struct rectangle dim);
int client_moveresize_fullscreen(struct client *c);
int client_resize(struct client *c, struct size size);
int client_resize_floating(struct client *c, struct size size);
bool client_send_event(struct client *c, Atom atom);
int client_set_border_colour_focused(struct client *c, uint32_t col);
int client_set_border_colour_unfocused(struct client *c, uint32_t col);
int client_set_border_width(struct client *c, int unsigned border_width);
void client_set_focus(struct client *c, bool focus);
int client_set_fullscreen(struct client *c, bool fullscreen);
void client_show(struct client *c, bool show);

#endif /* ndef _KWM_CLIENT_H */
