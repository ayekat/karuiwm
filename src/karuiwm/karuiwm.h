#ifndef _KWM_KARUIWM_H
#define _KWM_KARUIWM_H

/* macros */
#define APPNAME "karuiwm"
#ifndef PREFIX
# define PREFIX "/usr/local"
#endif

#include <X11/Xlib.h>
#define CLIENTMASK (EnterWindowMask|PropertyChangeMask)

enum runstate { RUNNING, RESTARTING, STOPPING };

struct karuiwm {
	struct commandline *commandline;
	enum runstate runstate;
	char const *locale;
};

extern struct karuiwm karuiwm;

#endif /* ndef _KWM_KARUIWM_H */
