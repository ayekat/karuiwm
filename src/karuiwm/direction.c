#include <karuiwm/direction.h>
#include <karui/cstr.h>
#include <karui/log.h>
#include <stddef.h>

enum
direction direction_opposite(enum direction dir)
{
	switch (dir) {
		case DIR_NONE:  return DIR_NONE;
		case DIR_LEFT:  return DIR_RIGHT;
		case DIR_RIGHT: return DIR_LEFT;
		case DIR_UP:    return DIR_DOWN;
		case DIR_DOWN:  return DIR_UP;
	}
	FATAL_BUG("direction_opposite(%d): Reached unreachable section", dir);
}

void
direction_relative(struct position start, struct position *end,
                   enum direction dir)
{
	*end = start;
	switch (dir) {
		case DIR_NONE:  break;
		case DIR_LEFT:  --end->x; break;
		case DIR_RIGHT: ++end->x; break;
		case DIR_UP:    --end->y; break;
		case DIR_DOWN:  ++end->y; break;
	}
}

int
direction_parse(enum direction *dir, char const *str)
{
	char const *leftstrs[] = { "Left", "left", "L", "l" };
	char const *rightstrs[] = { "Right", "right", "R", "r" };
	char const *upstrs[] = { "Up", "up", "U", "u" };
	char const *downstrs[] = { "Down", "down", "D", "d" };
	size_t const NSTRS = 4;
	int unsigned i;

	for (i = 0; i < NSTRS; ++i) {
		if (cstr_equals(str, leftstrs[i])) {
			if (dir)
				*dir = DIR_LEFT;
			return 0;
		}
		if (cstr_equals(str, rightstrs[i])) {
			if (dir)
				*dir = DIR_RIGHT;
			return 0;
		}
		if (cstr_equals(str, upstrs[i])) {
			if (dir)
				*dir = DIR_UP;
			return 0;
		}
		if (cstr_equals(str, downstrs[i])) {
			if (dir)
				*dir = DIR_DOWN;
			return 0;
		}
	}
	log_set("Not a direction: '%s'", str);
	return -1;
}

char const *
direction_str(enum direction dir)
{
	switch (dir) {
	case DIR_NONE:  return "none";
	case DIR_LEFT:  return "left";
	case DIR_RIGHT: return "left";
	case DIR_UP:    return "up";
	case DIR_DOWN:  return "down";
	}
	FATAL_BUG("direction_str(%d): Reached unreachable section", dir);
}
