#ifndef _KWM_ACTION_H
#define _KWM_ACTION_H

#include <karuiwm/value.h>
#include <karui/list.h>
#include <stdbool.h>
#include <stdint.h>

struct action {
	char *name;
	size_t nargs;
	enum value_type *arg_types;
	int (*function)(struct list **, struct list const *);
	struct list *action_calls; /* managed by action_call.c */
};

struct action *action_alloc(char const *name,
                            int (*function)(struct list **, struct list const *),
                            size_t nargs, enum value_type const *arg_types);
void action_free(struct action *a);

bool action_has_name(struct action const *a, char const *name);

#endif /* ndef _KWM_ACTION_H */
