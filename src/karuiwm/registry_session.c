#include <karuiwm/registry_session.h>
#include <karuiwm/buttonmap.h>
#include <karuiwm/keymap.h>
#include <karuiwm/registry.h>
#include <karui/cstr.h>
#include <karui/list.h>
#include <karui/log.h>
#include <karui/memory.h>

struct registry_session *
registry_session_alloc(char const *name)
{
	struct registry_session *rs;

	rs = memory_alloc(sizeof(struct registry_session),
	                  "registry session structure");
	if (!rs)
		return NULL;
	if (registry_session_init(rs, name) < 0) {
		log_set("Could not initialise registry session: %s", log_str());
		memory_free(rs);
		return NULL;
	}
	return rs;
}

int
registry_session_init(struct registry_session *rs, char const *name)
{
	DEBUG("Opening registry session '%s'", name);

	rs->name = cstr_dup(name);
	if (!rs->name) {
		log_set("Could not duplicate registry session name '%s': %s",
		        name, log_str());
		goto error_name;
	}
	rs->actions = list_alloc();
	if (!rs->actions) {
		log_set("Could not create actions list: %s", log_str());
		goto error_actions;
	}
	return 0;

 error_actions:
	log_push();
	cstr_free(rs->name);
	log_pop();
 error_name:
	return -1;
}

void
registry_session_deinit(struct registry_session *rs)
{
	struct action *a;

	DEBUG("Closing registry session '%s' with %zu actions",
	      rs->name, rs->actions->size);

	/* delete any keys bound to actions */
	while (rs->actions->size > 0) {
		a = rs->actions->elements[0];
		registry_session_remove_action(rs, a);
		action_free(a);
	}
	list_free(rs->actions);
	rs->actions = NULL;

	cstr_free(rs->name);
	rs->name = NULL;
}

void
registry_session_free(struct registry_session *rs)
{
	registry_session_deinit(rs);
	memory_free(rs);
}

int
registry_session_add_action(struct registry_session *rs, struct action *a)
{
	list_append(rs->actions, a);

	return 0;
}

struct action *
registry_session_find_action_by_name(struct registry_session const *rs,
                                     char const *name)
{
	return list_find(rs->actions, name, (list_eqfunc) action_has_name);
}

void
registry_session_remove_action(struct registry_session *rs, struct action *a)
{
	list_remove(rs->actions, a, NULL);
}
