#include <karuiwm/workspace.h>
#include <karuiwm/bus.h>
#include <karuiwm/layout.h>
#include <karuiwm/monitor.h>
#include <karuiwm/utils.h>
#include <karuiwm/xserver.h>
#include <karuiwm/xwindow.h>
#include <karui/list.h>
#include <karui/log.h>
#include <karui/memory.h>
#include <assert.h>
#include <string.h>

static void _bus_event(struct bus_subscription const *sub, void *data,
                       void *ctx);
static int _subscribe(struct workspace *ws, struct client *c);

static struct bus_channel **const _CLIENT_CHANNELS[] = {
	&bus.client_destroy,
	&bus.client_state_dialog,
	&bus.client_state_fullscreen,
	&bus.client_transience,
	&bus.client_type,
};

struct workspace *
workspace_alloc(void)
{
	struct workspace *ws;

	ws = memory_alloc(sizeof(struct workspace), "workspace structure");
	if (!ws)
		goto error_alloc;

	ws->tiled = list_alloc();
	if (!ws->tiled) {
		log_set("Could not create list for tiled clients: %s",
		        log_str());
		goto error_tiled;
	}

	ws->floating = list_alloc();
	if (!ws->floating) {
		log_set("Could not create list for floating clients: %s",
		        log_str());
		goto error_floating;
	}

	ws->selcli = NULL;

	ws->layout = layout_alloc(ws->tiled, LAYOUT_RSTACK);
	if (!ws->layout) {
		log_set("Could not create layout: %s", log_str());
		goto error_layout;
	}

	ws->bus_subscriptions = list_alloc();
	if (!ws->bus_subscriptions) {
		log_set("Could not create list for event bus subscriptions: %s",
		        log_str());
		goto error_bus_subscriptions;
	}

	ws->bus_session = bus_connect("workspace", _bus_event, ws);
	if (!ws->bus_session) {
		log_set("Could not connect to event bus: %s", log_str());
		goto error_bus_connect;
	}

	ws->mon = NULL;
	workspace_rename(ws, NULL);
	ws->focused = false;
	ws->nofloat = false;

	return ws;

 error_bus_connect:
	log_push();
	list_free(ws->bus_subscriptions);
	log_pop();
 error_bus_subscriptions:
	log_push();
	layout_free(ws->layout);
	log_pop();
 error_layout:
	log_push();
	list_free(ws->floating);
	log_pop();
 error_floating:
	log_push();
	list_free(ws->tiled);
	log_pop();
 error_tiled:
	log_push();
	memory_free(ws);
	log_pop();
 error_alloc:
	return NULL;
}

void
workspace_free(struct workspace *ws)
{
	if (!workspace_isempty(ws))
		FATAL_BUG("Attempt to free non-empty workspace");

	bus_disconnect(ws->bus_session);
	list_free(ws->bus_subscriptions);
	layout_free(ws->layout);
	list_free(ws->tiled);
	list_free(ws->floating);
	memory_free(ws);
}

int
workspace_attach_client(struct workspace *ws, struct client *c)
{
	int unsigned i, pos, cbwidth;
	struct list *list;
	uint32_t cbcolour_focused, cbcolour_unfocused;

	if (ws->nofloat && c->floating) {
		DEBUG("Attaching floating client %lu to non-floating workspace '%s'; tiling",
		      c->xwindow->id, ws->name);
		c->floating = false;
	} else if (c->dialog || c->splash) {
		DEBUG("Attaching %s client %lu to workspace '%s'; floating",
		      c->splash ? "splash" : "dialog",
		      c->xwindow->id, ws->name);
		c->floating = true;
	}

	if (_subscribe(ws, c) < 0) {
		log_set("Could not subscribe to client bus event channels: %s",
		        log_str());
		goto error_subscribe;
	}

	list = c->floating ? ws->floating : ws->tiled;

	if (list == ws->floating) {
		pos = 0;
	} else if (list->size == 0) {
		pos = 0;
	} else if (list_contains(list, ws->selcli, NULL)) {
		i = list_find_index(list, ws->selcli, NULL);
		pos = MIN(i + 1, (int unsigned) list->size);
	} else {
		pos = (int unsigned) list->size;
	}
	list_insert(list, c, pos);
	c->ws = ws;

	/* Update looks according to configuration in state.
	 * FIXME: Move looks out of state/workspace/client.
	 */
	cbcolour_focused = state.config.client_border_colour_focused;
	cbcolour_unfocused = state.config.client_border_colour_unfocused;
	cbwidth = state.config.client_border_width;
	if (client_set_border_colour_focused(c, cbcolour_focused) < 0)
		WARN("Could not set focused border colour on client %lu to 0x%06X: %s",
		     c->xwindow->id, cbcolour_focused, log_str());
	if (client_set_border_colour_unfocused(c, cbcolour_unfocused) < 0)
		WARN("Could not set unfocused border colour on client %lu to 0x%06X: %s",
		     c->xwindow->id, cbcolour_unfocused, log_str());
	if (client_set_border_width(c, cbwidth) < 0)
		WARN("Could not set border width on client %lu to %u: %s",
		     c->xwindow->id, cbwidth, log_str());

	client_show(c, ws->mon);
	workspace_focus_client(ws, c);

	/* TODO: use events, once they are a thing */
	if (ws->mon) {
		monitor_restack(ws->mon);
		if (monitor_arrange(ws->mon) < 0)
			ERROR("Could not rearrange monitor: %s", log_str());
	}

	return 0;

 error_subscribe:
	return -1;
}

int
workspace_compare_name(struct workspace *ws1, struct workspace *ws2)
{
	return strcmp(ws1->name, ws2->name);
}

void
workspace_detach_client(struct workspace *ws, struct client *c)
{
	unsigned int i;
	struct list *clients, *otherclients;
	struct client *nextc;
	struct bus_subscription *sub;

	if (c->ws != ws) {
		BUG("Attempt to detach client from wrong workspace");
		return;
	}

	while ((sub = list_find(ws->bus_subscriptions, c,
	                        (list_eqfunc) bus_subscription_matches_event)))
	{
		list_remove(ws->bus_subscriptions, sub, NULL);
		bus_subscription_free(sub);
	}

	if (ws->nofloat)
		assert(!c->floating);

	clients = c->floating ? ws->floating : ws->tiled;
	otherclients = c->floating ? ws->tiled : ws->floating;

	assert(list_contains(clients, c, NULL));

	/* focus next client */
	i = list_find_index(clients, c, NULL);
	nextc = i < clients->size - 1 ? clients->elements[i + 1]
	        : i > 0 ? clients->elements[i - 1]
	        : otherclients->size > 0 ? otherclients->elements[0]
	        : NULL;
	workspace_focus_client(ws, nextc);

	/* remove current client */
	list_remove(clients, c, NULL);
	c->ws = NULL;

	/* TODO: use events, once they are a thing */
	if (ws->mon) {
		monitor_restack(ws->mon);
		if (monitor_arrange(ws->mon) < 0)
			ERROR("Could not rearrange monitor: %s", log_str());
	}
}

void
workspace_focus(struct workspace *ws, bool focus)
{
	assert(!focus || ws->mon);

	if (ws->focused == focus)
		return;

	ws->focused = focus;

	if (ws->selcli)
		client_set_focus(ws->selcli, focus);
	else if (focus)
		XSetInputFocus(xserver.display, xserver.root->id,
		               RevertToPointerRoot, CurrentTime);
}

void
workspace_focus_client(struct workspace *ws, struct client *c)
{
	if (ws->selcli == c)
		return;

	if (ws->focused && ws->selcli)
		client_set_focus(ws->selcli, false);
	ws->selcli = c;
	if (ws->focused) {
		if (ws->selcli)
			client_set_focus(ws->selcli, true);
		else
			XSetInputFocus(xserver.display, xserver.root->id,
			               RevertToPointerRoot, CurrentTime);
	}
}

bool
workspace_isempty(struct workspace *ws)
{
	return ws->tiled->size == 0 && ws->floating->size == 0;
}

void
workspace_raise_floating(struct workspace *ws, struct client *c)
{
	assert(c->floating);
	assert(!ws->nofloat);

	list_push(ws->floating, c);

	/* TODO: use events, once they are a thing */
	if (ws->mon)
		monitor_restack(ws->mon);
}

void
workspace_rename(struct workspace *ws, char const *name)
{
	if (name && strlen(name) > 0) {
		if (name[0] == '*') {
			ERROR("Name must not start with an asterisk");
			return;
		}
		strncpy(ws->name, name, 255);
		ws->name[255] = '\0';
	} else {
		sprintf(ws->name, "*%p", (void *) ws);
	}
}

int
workspace_set_floating(struct workspace *ws, struct client *c, bool floating)
{
	struct list *src, *dst;

	if (c->floating == floating)
		return 0;

	if (ws->nofloat) {
		log_set("Cannot float client on non-floating workspace");
		return -1;
	}

	src = c->floating ? ws->floating : ws->tiled;
	dst = c->floating ? ws->tiled : ws->floating;

	list_remove(src, c, NULL);
	if (dst == ws->floating)
		list_prepend(dst, c);
	else
		list_append(dst, c);
	c->floating = floating;

	/* TODO: use events, once they are a thing */
	if (ws->mon) {
		monitor_restack(ws->mon);
		if (monitor_arrange(ws->mon) < 0)
			ERROR("Could not rearrange monitor: %s", log_str());
	}

	return 0;
}

int
workspace_set_fullscreen(struct workspace *ws, struct client *c,
                         bool fullscreen)
{
	if (ws->nofloat) {
		log_set("Cannot set client in non-floating workspace to fullscreen");
		return -1;
	}

	if (ws != c->ws) {
		log_set("Attempt to set client from workspace '%s' fullscreen on workspace '%s'",
		        c->ws->name, ws->name);
		BUG("%s", log_str());
		return -1;
	}

	if (client_set_fullscreen(c, fullscreen) < 0) {
		log_set("Could not set client to fullscreen: %s", log_str());
		goto error_client;
	}

	/* TODO: notify monitor through events */
	if (ws->mon) {
		monitor_restack(ws->mon);
		if (monitor_arrange(ws->mon) < 0)
			ERROR("Could not rearrange clients on monitor: %s",
			      log_str());
	}
	return 0;

 error_client:
	return -1;
}

int
workspace_set_layout(struct workspace *ws, enum layout_type type)
{
	struct layout *l;
	
	if (ws->layout->type == type)
		return 0;

	l = layout_alloc(ws->tiled, type);
	if (!l) {
		log_set("Could not create new layout: %s", log_str());
		goto error_alloc;
	}

	layout_free(ws->layout);
	ws->layout = l;

	bus_channel_send(bus.workspace_layout, ws);

	return 0;

 error_alloc:
	return -1;
}

void
workspace_show(struct workspace *ws, bool show)
{
	struct client *c;
	int unsigned i;

	LIST_FOR (ws->tiled, i, c)
		client_show(c, show);
	LIST_FOR (ws->floating, i, c)
		client_show(c, show);
}

int
workspace_transfer_client(struct workspace *ws, struct client *c)
{
	struct workspace *oldws = c->ws;

	if (ws == oldws)
		return 0;

	if (c->ws)
		workspace_detach_client(c->ws, c);

	if (workspace_attach_client(ws, c) < 0) {
		log_set("Could not attach client to workspace: %s", log_str());
		goto error_attach;
	}

	return 0;

 error_attach:
	log_push();
	if (workspace_attach_client(oldws, c) < 0)
		/* This one is lost. RIP.
		 * TODO: Allow client to be attached to multiple workspaces
		 * simultaneously, and detaching from old WS *after* attaching
		 * to new WS.
		 */
		CRITICAL("Could not reattach client to previous workspace: %s",
		         log_str());
	log_pop();
	return -1;
}

static void
_bus_event(struct bus_subscription const *sub, void *data, void *ctx)
{
	struct bus_channel *ch = sub->channel;
	struct workspace *ws = ctx;
	struct workspace *ws_rec;
	struct client *c;

	if (ch == bus.client_destroy) {
		c = data;
		DEBUG("Workspace '%s' received bus event '%s'; detaching client",
		      ws->name, ch->name);
		workspace_detach_client(ws, c);
	} else if (ch == bus.client_state_dialog) {
		c = data;
		DEBUG("Workspace '%s' received bus event '%s'; updating float state",
		      ws->name, ch->name);
		if (c->dialog && !c->floating)
			if (workspace_set_floating(ws, c, true) < 0)
				ERROR("Could not float client %lu on workspace '%s': %s",
				      c->xwindow->id, ws->name, log_str());
	} else if (ch == bus.client_state_fullscreen) {
		DEBUG("Workspace '%s' received bus event '%s'; signaling monitor",
		      ws->name, ch->name);
	} else if (ch == bus.client_transience) {
		c = data;
		log_set("Workspace '%s' received bus event '%s' for client %lu",
		        ws->name, ch->name, c->xwindow->id);
		log_push();
		ws_rec = state_recommend_workspace(c);
		log_pop();
		if (ws_rec) {
			if (ws_rec != ws) {
				DEBUG("%s; transferring to recommended workspace '%s'",
				      log_str(), ws_rec->name);
				if (workspace_transfer_client(ws_rec, c) < 0)
					ERROR("Could not transfer transient client %lu to recommended workspace '%s': %s",
					      c->xwindow->id, ws_rec->name, log_str());
			} else {
				log_set("%s; not transferring: already on recommended workspace",
				        log_str());
				if (c->dialog && !c->floating) {
					DEBUG("%s; floating", log_str());
					if (workspace_set_floating(ws, c,
					                           true) < 0)
						ERROR("Could not float dialog client %lu: %s",
						      c->xwindow->id,
						      log_str());
				} else {
					log_flush(LOG_DEBUG);
				}
			}
		} else {
			DEBUG("%s; not transferring: no recommended workspace",
			      log_str());
		}
		return;
	} else if (ch == bus.client_type) {
		c = data;
		DEBUG("Workspace '%s' received bus event '%s'; updating float state",
		      ws->name, ch->name);
		if ((c->splash || c->dialog) && !c->floating)
			if (workspace_set_floating(ws, c, true) < 0)
				ERROR("Could not float client %lu on workspace '%s': %s",
				      c->xwindow->id, ws->name, log_str());
	} else {
		BUG("Workspace '%s' received unexpected bus event '%s'",
		    ws->name, ch->name);
		return;
	}

	if (ws->mon) {
		monitor_restack(ws->mon);
		if (monitor_arrange(ws->mon) < 0)
			ERROR("Could not rearrange clients on monitor: %s",
			      log_str());
	}
}

static int
_subscribe(struct workspace *ws, struct client *c)
{
	int unsigned i;
	struct bus_channel *ch;
	struct bus_subscription *sub;

	for (i = 0; i < LENGTH(_CLIENT_CHANNELS); ++i) {
		ch = *_CLIENT_CHANNELS[i];
		sub = bus_subscription_alloc(ws->bus_session, ch, c);
		if (!sub) {
			log_set("Could not join event bus channel '%s': %s",
			        ch->name, log_str());
			goto error_subscribe;
		}
		list_append(ws->bus_subscriptions, sub);
	}
	return 0;

 error_subscribe:
	log_push();
	while ((sub = list_find(ws->bus_subscriptions, c,
	                        (list_eqfunc) bus_subscription_matches_event)))
	{
		list_remove(ws->bus_subscriptions, sub, NULL);
		bus_subscription_free(sub);
	}
	log_pop();
	return -1;
}
