#ifndef _KWM_SIZE_H
#define _KWM_SIZE_H

struct size {
	int unsigned width;
	int unsigned height;
};

#include <stdbool.h>

bool size_equals(struct size const *s1, struct size const *s2);

#endif /* ndef _KWM_SIZE_H */
