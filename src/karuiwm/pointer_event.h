#ifndef _KWM_POINTER_EVENT_H
#define _KWM_POINTER_EVENT_H

enum pointer_event_type {
	POINTER_MOVE,
	POINTER_ENTER,
	POINTER_LEAVE,
};

#include <karuiwm/pointer.h>

struct pointer_event {
	enum pointer_event_type type;
	struct pointer pointer;
	struct client *clienct;
};

char const *pointer_event_type_str(enum pointer_event_type type);

#endif /* ndef _KWM_POINTER_EVENT_H */
