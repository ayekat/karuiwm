#!/bin/sh -

if [ $# -ne 2 ]; then
	cat <<- EOF
	Usage: $0 name varname
	EOF
	exit 1
fi

name=$1
cppname=$(printf '%s' "$name" | tr '[:lower:]' '[:upper:]')
varname=$2
desc=$(printf '%s' "$name" | tr '_' ' ')
T=$(printf '\t')

cat >"$name".h <<- EOF
	#ifndef _KWM_${cppname}_H
	#define _KWM_${cppname}_H
	
	struct $name {
	${T}char *name;
	};
	
	struct $name *${name}_alloc(char const *name);
	void ${name}_free(struct $name *$varname);
	
	#endif /* ndef _KWM_${cppname}_H */
EOF

cat >"$name".c <<- EOF
	#include <karuiwm/$name.h>
	#include <karui/cstr.h>
	#include <karui/log.h>
	#include <karui/memory.h>
	
	struct $name *
	${name}_alloc(char const *name)
	{
	${T}struct $name *$varname;
	
	${T}$varname = memory_alloc(sizeof(struct $name), "$desc struct");
	${T}if (!$varname)
	${T}${T}goto error_alloc;
	
	${T}$varname->name = cstr_dup(name);
	${T}if (!$varname->name) {
	${T}${T}log_set("Could not copy name: %s", log_str());
	${T}${T}goto error_name;
	${T}}
	
	${T}return $varname;
	
	 error_name:
	${T}log_push();
	${T}memory_free($varname);
	${T}log_pop();
	 error_alloc:
	${T}return NULL;
	}
	
	void
	${name}_free(struct $name *$varname)
	{
	${T}cstr_free($varname->name);
	${T}memory_free($varname);
	}
EOF
