#ifndef _KWM_FDIO_CLIENT_H
#define _KWM_FDIO_CLIENT_H

struct fdio_client {
	char *name;
	int (*callback)(int, void *);
	struct list *fds;
	void *data;
};

#include <stdbool.h>

struct fdio_client *fdio_client_alloc(char const *name,
                                      int (*callback)(int, void *), void *data);
void fdio_client_free(struct fdio_client *c);

int fdio_client_add_fd(struct fdio_client *c, int *fd);
bool fdio_client_has_fd(struct fdio_client const *c, int const *fd);
int fdio_client_notify(struct fdio_client const *c, int const *fd);
void fdio_client_remove_fd(struct fdio_client *c, int *fd);

#endif /* ndef _KWM_FDIO_CLIENT_H */
