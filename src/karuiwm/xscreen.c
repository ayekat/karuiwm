#include <karuiwm/xscreen.h>
#include <karuiwm/bus.h>
#include <karuiwm/rectangle.h>
#include <karui/cstr.h>
#include <karui/log.h>
#include <karui/memory.h>

struct xscreen *
xscreen_alloc(struct rectangle dim)
{
	struct xscreen *xscr;

	xscr = memory_alloc(sizeof(struct xscreen), "xscreen struct");
	if (!xscr)
		goto error_alloc;

	xscr->dimension = dim;

	return xscr;

 error_alloc:
	return NULL;
}

void
xscreen_free(struct xscreen *xscr)
{
	memory_free(xscr);
}

int
xscreen_moveresize(struct xscreen *xscr, struct rectangle dim)
{
	if (rectangle_equals(&dim, &xscr->dimension))
		return 0;

	xscr->dimension = dim;
	bus_channel_send(bus.x_screen_resize, xscr);

	return 0;
}
