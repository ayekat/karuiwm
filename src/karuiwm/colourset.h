#ifndef _KWM_COLOURSET_H
#define _KWM_COLOURSET_H

#include <stdint.h>

struct colourset {
	uint32_t fg;
	uint32_t bg;
	uint32_t border;
};

#endif /* ndef _KWM_COLOURSET_H */
