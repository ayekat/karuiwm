#ifndef _KWM_WSMBOX_H
#define _KWM_WSMBOX_H

#include <X11/Xlib.h>
#include <karuiwm/position.h>
#include <karuiwm/size.h>
#include <stdbool.h>

struct wsmbox {
	bool mapped;
	struct xwindow *xwindow;
	Pixmap pixmap;
	struct workspace *workspace;
	struct size size;
	struct position position;
	struct {
		struct bus_session *session;
		struct bus_subscription *sub_ws_rename;
	} bus;
};

#include <karuiwm/rectangle.h>
#include <karuiwm/workspace.h>
#include <karuiwm/wsmap.h>

struct wsmbox *wsmbox_alloc(struct workspace *ws);
void wsmbox_free(struct wsmbox *wb);

bool wsmbox_has_position(struct wsmbox const *wb, struct position const *pos);
bool wsmbox_has_workspace(struct wsmbox const *wb, struct workspace const *ws);
int wsmbox_moveresize(struct wsmbox *wb, struct rectangle coord);
int wsmbox_set_workspace(struct wsmbox *wb, struct workspace *ws);
int wsmbox_show(struct wsmbox *wb, bool visible);
int wsmbox_update_border(struct wsmbox *wb);
int wsmbox_update_content(struct wsmbox *wb);

#endif /* ndef _KWM_WSMBOX_H */
