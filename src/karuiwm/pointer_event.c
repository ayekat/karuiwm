#include <karuiwm/pointer_event.h>
#include <karui/log.h>

char const *
pointer_event_type_str(enum pointer_event_type type)
{
	switch (type) {
	case POINTER_MOVE: return "POINTER_MOVE";
	case POINTER_ENTER: return "POINTER_ENTER";
	case POINTER_LEAVE: return "POINTER_LEAVE";
	}
	FATAL_BUG("Unknown pointer event type %d", type);
}
