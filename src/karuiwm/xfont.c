#include <karuiwm/xfont.h>
#include <karuiwm/utils.h>
#include <karui/cstr.h>
#include <karui/log.h>
#include <karui/memory.h>

struct xfont *
xfont_alloc(Display *dpy, char const *locale, char const *name)
{
	struct xfont *xfont;
	XFontStruct **fontstructs;
	char **fontnames;
	char *def, **missing;
	int n;

	xfont = memory_alloc(sizeof(struct xfont), "X font structure");
	if (!xfont)
		goto error_xfont_alloc;

	xfont->dpy = dpy;
	xfont->name = cstr_dup(name);
	if (!xfont->name) {
		log_set("Could not duplicate font name '%s': %s",
		        name, log_str());
		goto error_dup_name;
	}

	xfont->fontset = XCreateFontSet(dpy, xfont->name, &missing, &n, &def);
	if (!xfont->fontset) {
		log_set("Could not gather font set for fonts string '%s'",
		        xfont->name);
		goto error_font_set;
	}

	if (missing) {
		WARN("%s: Found %i missing font charsets for %s",
		     name, n, locale);
		//while (n--) {
		//	DEBUG("%s: Missing font charset for %s: %s",
		//	     name, locale, missing[n]);
		//}
		XFreeStringList(missing);
	}

	xfont->ascent = xfont->descent = 0;
	n = XFontsOfFontSet(xfont->fontset, &fontstructs, &fontnames);
	while (n--) {
		xfont->ascent = MAX(xfont->ascent,
		                    (int unsigned) fontstructs[n]->ascent);
		xfont->descent = MAX(xfont->descent,
		                     (int unsigned) fontstructs[n]->descent);
	}
	xfont->height = xfont->ascent + xfont->descent;

	return xfont;

 error_font_set:
	log_push();
	cstr_free(xfont->name);
	log_pop();
 error_dup_name:
	log_push();
	memory_free(xfont);
	log_pop();
 error_xfont_alloc:
	return NULL;
}

void
xfont_free(struct xfont *xfont)
{
	XFreeFontSet(xfont->dpy, xfont->fontset);
	cstr_free(xfont->name);
	memory_free(xfont);
}
