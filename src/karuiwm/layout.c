#include <karuiwm/layout.h>
#include <karuiwm/client.h>
#include <karuiwm/icon.h>
#include <karuiwm/utils.h>
#include <karuiwm/xwindow.h>
#include <karui/list.h>
#include <karui/log.h>
#include <karui/memory.h>

struct _stack_state {
	size_t nmaster;
	float mfact;
};

static void _bstack_apply(struct layout const *l, struct rectangle const *area,
                          int unsigned client_border_width);
static void _init_icon(struct layout *l);
static void _lfixed_apply(struct layout const *l, struct rectangle const *area,
                          int unsigned client_border_width);
static void _rstack_apply(struct layout const *l, struct rectangle const *area,
                          int unsigned client_border_width);

struct layout *
layout_alloc(struct list *clients, enum layout_type type)
{
	struct layout *l;
	struct _stack_state *lstate;

	l = memory_alloc(sizeof(struct layout), "layout structure");
	if (!l)
		goto error_alloc;

	l->clients = clients;
	l->type = type;

	lstate = memory_alloc(sizeof(struct _stack_state),
	                     "stack layout state structure");
	if (!lstate)
		goto error_state;

	lstate->nmaster = 1;
	lstate->mfact = 0.5f;
	l->state = lstate;

	_init_icon(l);

	return l;

 error_state:
	log_push();
	memory_free(l);
	log_pop();
 error_alloc:
	return NULL;
}

void
layout_free(struct layout *l)
{
	icon_free(l->icon);
	memory_free(l->state);
	memory_free(l);
}

void
layout_apply(struct layout const *l, struct rectangle const *area,
             int unsigned client_border_width)
{
	switch (l->type) {
	case LAYOUT_RSTACK:
		_rstack_apply(l, area, client_border_width);
		break;
	case LAYOUT_BSTACK:
		_bstack_apply(l, area, client_border_width);
		break;
	case LAYOUT_LFIXED:
		_lfixed_apply(l, area, client_border_width);
		break;
	}
}

void
layout_setmfact(struct layout const *l, float dmfact)
{
	struct _stack_state *s = l->state;

	s->mfact = MAX(0.1f, MIN(0.9f, s->mfact + dmfact));
}

void
layout_setnmaster(struct layout const *l, ssize_t dnmaster)
{
	struct _stack_state *s = l->state;

	s->nmaster = (size_t) MAX(0, (ssize_t) s->nmaster + dnmaster);
}

static void
_bstack_apply(struct layout const *l, struct rectangle const *area,
              int unsigned client_border_width)
{
	struct _stack_state *lstate = l->state;
	struct rectangle cdim, real_cdim;
	int unsigned i;
	struct client *c;
	size_t nmaster;
	float mfact;

	if (l->clients->size == 0)
		return;

	/* master area */
	nmaster = MIN(lstate->nmaster, l->clients->size);
	mfact = nmaster == 0 ? 0.0f
	      : nmaster >= l->clients->size ? 1.0f
	      : lstate->mfact;
	if (nmaster > 0) {
		cdim.y = area->y;
		cdim.width = area->width / (int unsigned) nmaster;
		cdim.height = (int unsigned) (mfact * (float) area->height);
		for (i = 0; i < nmaster; ++i) {
			c = l->clients->elements[i];
			real_cdim = cdim;
			real_cdim.x = area->x + (int signed) (i * cdim.width);
			real_cdim.width -= 2 * client_border_width;
			real_cdim.height -= 2 * client_border_width;
			if (client_moveresize(c, real_cdim) < 0)
				ERROR("Could not redimension client %lu to %ux%u%+d%+d: %s",
				      c->xwindow->id,
				      real_cdim.width, real_cdim.height,
				      real_cdim.x, real_cdim.y, log_str());
		}
	}
	if (nmaster == l->clients->size)
		return;

	/* stack area */
	cdim.y = area->y + (int signed) (mfact * (float) area->height);
	cdim.width = area->width / (int unsigned) (l->clients->size - nmaster);
	cdim.height = area->height - (int unsigned) (cdim.y - area->y);
	for (i = (int unsigned) nmaster; i < l->clients->size; ++i) {
		c = l->clients->elements[i];
		real_cdim = cdim;
		real_cdim.x = area->x
		              + (int signed) ((i - (int unsigned) nmaster)
		                              * cdim.width);
		real_cdim.width -= 2 * client_border_width;
		real_cdim.height -= 2 * client_border_width;
		if (client_moveresize(c, real_cdim) < 0)
			ERROR("Could not redimension client %lu to %ux%u%+d%+d: %s",
			      c->xwindow->id, real_cdim.width, real_cdim.height,
			      real_cdim.x, real_cdim.y, log_str());
	}
}

static void
_init_icon(struct layout *l)
{
	uint64_t *icon_lines = NULL;
	uint64_t icon_lines_bstack[] = {
		0x00000,
		0x00000,
		0x1FFFF,
		0x1FFFF,
		0x1FFFF,
		0x1FFFF,
		0x1FFFF,
		0x00000,
		0x1F7DF,
		0x1F7DF,
		0x1F7DF,
		0x1F7DF,
		0x1F7DF,
		0x00000,
		0x00000,
	};
	uint64_t icon_lines_rstack[] = {
		0x00000,
		0x00000,
		0x1FEFF,
		0x1FEFF,
		0x1FEFF,
		0x1FE00,
		0x1FEFF,
		0x1FEFF,
		0x1FEFF,
		0x1FE00,
		0x1FEFF,
		0x1FEFF,
		0x1FEFF,
		0x00000,
		0x00000,
	};
	uint64_t icon_lines_lfixed[] = {
		0x00000,
		0x00000,
		0x1FDFF,
		0x1FDFF,
		0x1FDFF,
		0x1FDFF,
		0x1FDFF,
		0x1FC00,
		0x1FDEF,
		0x1FDEF,
		0x1FDEF,
		0x1FDEF,
		0x1FDEF,
		0x00000,
		0x00000,
	};

	/* icons */
	switch (l->type) {
	case LAYOUT_BSTACK: icon_lines = icon_lines_bstack; break;
	case LAYOUT_RSTACK: icon_lines = icon_lines_rstack; break;
	case LAYOUT_LFIXED: icon_lines = icon_lines_lfixed; break;
	}
	l->icon = icon_alloc(17, 15, icon_lines);
	if (!l->icon)
		FATAL("Could not create icon: %s", log_str());
}

static void
_lfixed_apply(struct layout const *l, struct rectangle const *area,
              int unsigned client_border_width)
{
	struct rectangle cdim, real_cdim;
	int unsigned i, wfixed, border_width, font_width;
	size_t nfixed;
	struct client *c;

	if (l->clients->size == 0)
		return;

	border_width = client_border_width;
	font_width = 6; /* FIXME */
	nfixed = 1; /* FIXME */

	/* draw fix client to the left */
	wfixed = font_width * (2 + (1 + 3 + 1) + 81) + border_width * (1 + 1);
	/*                     errs  linenums  cols                    L   R */
	wfixed = MIN(wfixed + 2 * border_width,
	             area->width - 2 * border_width - 1);
	nfixed = MIN(nfixed, l->clients->size);
	if (nfixed > 0) {
		cdim.x = area->x;
		cdim.y = area->y;
		cdim.width = nfixed == l->clients->size ? area->width : wfixed;
		cdim.height = area->height;
		for (i = 0; i < nfixed; ++i) {
			c = l->clients->elements[i];
			real_cdim = cdim;
			real_cdim.width -= 2 * border_width;
			real_cdim.height -= 2 * border_width;
			if (client_moveresize(c, real_cdim) < 0)
				ERROR("Could not redimension client %lu to %ux%u%+d%+d: %s",
				      c->xwindow->id,
				      real_cdim.width, real_cdim.height,
				      real_cdim.x, real_cdim.y, log_str());
		}
	}
	if (nfixed == l->clients->size)
		return;

	/* apply bstack to the remaining area */
	struct layout stack_layout;
	struct list stack_clients;
	struct rectangle stack_area;

	stack_clients.elements = l->clients->elements + nfixed;
	stack_clients.size = l->clients->size - nfixed;
	stack_layout.clients = &stack_clients;
	stack_layout.state = l->state;
	stack_area = *area;
	if (nfixed > 0) {
		stack_area.x += (int signed) wfixed;
		stack_area.width -= wfixed + 2 * border_width;
	}
	_bstack_apply(&stack_layout, &stack_area, client_border_width);
}

static void
_rstack_apply(struct layout const *l, struct rectangle const *area,
              int unsigned client_border_width)
{
	struct _stack_state *lstate = l->state;
	struct rectangle cdim, real_cdim;
	int unsigned i;
	size_t nmaster;
	float mfact;
	struct client *c;

	/* draw master area */
	nmaster = MIN(lstate->nmaster, l->clients->size);
	mfact = nmaster == 0 ? 0.0f
	      : nmaster >= l->clients->size ? 1.0f
	      : lstate->mfact;
	if (nmaster > 0) {
		cdim.x = area->x;
		cdim.width = (int unsigned) (mfact * (float) area->width);
		cdim.height = area->height / (int unsigned) nmaster;
		for (i = 0; i < nmaster; ++i) {
			c = l->clients->elements[i];
			real_cdim = cdim;
			real_cdim.y = area->y + (int signed) (i * cdim.height);
			real_cdim.width -= 2 * client_border_width;
			real_cdim.height -= 2 * client_border_width;
			if (client_moveresize(c, real_cdim) < 0)
				ERROR("Could not redimension client %lu to %ux%u%+d%+d: %s",
				      c->xwindow->id,
				      real_cdim.width, real_cdim.height,
				      real_cdim.x, real_cdim.y, log_str());
		}
	}
	if (nmaster == l->clients->size)
		return;

	/* draw stack area */
	cdim.x = area->x + (int signed) (mfact * (float) area->width);
	cdim.width = area->width - (int unsigned) (cdim.x - area->x);
	cdim.height = area->height
	            / (int unsigned) (l->clients->size - nmaster);
	for (i = (int unsigned) nmaster; i < l->clients->size; ++i) {
		c = l->clients->elements[i];
		real_cdim = cdim;
		real_cdim.y = area->y
		              + (int signed) ((i - nmaster) * cdim.height);
		real_cdim.width -= 2 * client_border_width;
		real_cdim.height -= 2 * client_border_width;
		if (client_moveresize(c, real_cdim) < 0)
			ERROR("Could not redimension client %lu to %ux%u%+d%+d: %s",
			      c->xwindow->id, real_cdim.width, real_cdim.height,
			      real_cdim.x, real_cdim.y, log_str());
	}
}
