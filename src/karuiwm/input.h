#ifndef _KWM_INPUT_H
#define _KWM_INPUT_H

#include <karuiwm/buttonmap.h>
#include <karuiwm/keymap.h>
#include <stdbool.h>
#include <X11/Xlib.h>

#define INPUT_MASK_BUTTON (ButtonPressMask|ButtonReleaseMask)
#define INPUT_MASK_MOUSE (INPUT_MASK_BUTTON|PointerMotionMask)
#define INPUT_KEYMAP_DEFAULT "normal"
#define INPUT_BUTTONMAP_DEFAULT "normal"

int input_init(void);
void input_deinit(void);

int input_create_buttonmap(char const *name);
int input_create_keymap(char const *name, bool grab_keyboard);
int input_destroy_buttonmap(char const *name);
int input_destroy_keymap(char const *name);
struct buttonmap *input_find_buttonmap_by_name(char const *name);
struct keymap *input_find_keymap_by_name(char const *name);
int input_resolve_button(struct button_event const *bev);
int input_resolve_key(struct key const *k);
int input_set_active_buttonmap(char const *name);
int input_set_active_keymap(char const *name);
int input_unbind_button(char const *buttonmap_name, struct button const *b);
int input_unbind_key(char const *keymap_name, struct key const *k);

#endif /* ndef _KWM_INPUT_H */
