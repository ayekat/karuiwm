#include <karuiwm/seat.h>
#include <karuiwm/bus.h>
#include <karuiwm/input.h>
#include <karuiwm/registry.h>
#include <karuiwm/utils.h>
#include <karuiwm/workspace.h>
#include <karuiwm/xoutput.h>
#include <karuiwm/xserver.h>
#include <karui/list.h>
#include <karui/log.h>
#include <karui/memory.h>
#include <X11/extensions/Xinerama.h>
#include <X11/Xlib.h>

static void _attach_monitor(struct monitor *mon);
static void _bus_event(struct bus_subscription const *sub, void *data,
                       void *ctx);
static void _detach_monitor(struct monitor *mon);
static struct workspace *_get_or_create_free_workspace(void);
static int _handle_configurerequest(union xserver_request const *req,
                                    enum xserver_request_handler_result *res,
                                    void *ctx);
static int _handle_maprequest(union xserver_request const *req,
                              enum xserver_request_handler_result *res,
                              void *ctx);
static int _init_bus(void);
static int _init_monitors(void);

struct seat seat;

static struct bus_session *_bus_session;

int
seat_init(void)
{
	seat.selmon = NULL;
	seat.config.scratchpad_margin = 50;
	seat.config.client_border_width = 1;

	/* output: monitors */
	if (_init_monitors() < 0) {
		log_set("Could not scan X monitors: %s", log_str());
		goto error_scan_monitors;
	}

	/* input: keyboard and mouse */
	if (input_init() < 0) {
		log_set("Could not initialise input: %s", log_str());
		goto error_input;
	}

	/* bus events */
	if (_init_bus() < 0) {
		log_set("Could not initialise bus: %s", log_str());
		goto error_bus;
	}

	/* request handlers */
	if (xserver_add_request_handler(XSERVER_REQUEST_CONFIGURE,
	                                _handle_configurerequest, NULL) < 0)
	{
		log_set("Could not install X window map request handler: %s",
		        log_str());
		goto error_reqhandler_configure;
	}
	if (xserver_add_request_handler(XSERVER_REQUEST_MAP, _handle_maprequest,
	                                NULL) < 0)
	{
		log_set("Could not install X window map request handler: %s",
		        log_str());
		goto error_reqhandler_map;
	}

	return 0;

 error_reqhandler_map:
	log_push();
	xserver_remove_request_handler(XSERVER_REQUEST_MAP, _handle_maprequest);
	log_pop();
 error_reqhandler_configure:
	log_push();
	list_clear(seat.monitors, (list_delfunc) monitor_free);
	list_free(seat.monitors);
	log_pop();
 error_scan_monitors:
	log_push();
	input_deinit();
	log_pop();
 error_input:
	log_push();
	bus_disconnect(_bus_session);
	log_pop();
 error_bus:
	return -1;
}

void
seat_deinit(void)
{
	struct monitor *m;

	/* X and bus events */
	xserver_remove_request_handler(XSERVER_REQUEST_CONFIGURE,
	                               _handle_configurerequest);
	xserver_remove_request_handler(XSERVER_REQUEST_MAP, _handle_maprequest);
	bus_disconnect(_bus_session);

	/* input (keybaord and mouse) */
	input_deinit();

	/* output (monitors) */
	while (seat.monitors->size > 0) {
		m = seat.monitors->elements[0];
		_detach_monitor(m);
		monitor_free(m);
	}
	list_free(seat.monitors);
}

void
seat_focus_monitor(struct monitor *mon)
{
	if (seat.selmon == mon)
		/* nothing to do */
		return;

	if (seat.selmon)
		monitor_focus(seat.selmon, false);

	seat.selmon = mon;

	if (seat.selmon)
		monitor_focus(seat.selmon, true);
	else
		XSetInputFocus(xserver.display, xserver.root->id,
		               RevertToPointerRoot, CurrentTime);

	bus_channel_send(bus.monitor_focus, mon);
}

struct monitor *
seat_locate_point(struct position pos)
{
	struct rectangle posrec = {
		.pos = pos,
		.width = 1,
		.height = 1
	};
	return seat_locate_rectangle(posrec);
}

struct monitor *
seat_locate_rectangle(struct rectangle r)
{
	struct monitor *mon, *selmon;
	int unsigned a, sela;
	int unsigned i;

	selmon = NULL;
	sela = 0u;
	LIST_FOR (seat.monitors, i, mon) {
		a = rectangle_intersection(mon->dim, r);
		if (a > sela) {
			sela = a;
			selmon = mon;
		}
	}
	return selmon;
}

int
seat_set_client_border_width(int unsigned client_border_width)
{
	struct monitor *mon;
	int unsigned i;

	seat.config.client_border_width = client_border_width;
	LIST_FOR (seat.monitors, i, mon)
		if (monitor_arrange(mon) < 0)
			ERROR("Could not rearrange monitor %u after updating client border width: %s",
			      i, log_str());
	return 0;
}

int
seat_set_scratchpad_margin(int unsigned margin)
{
	int unsigned i;
	struct monitor *mon;

	seat.config.scratchpad_margin = margin;
	LIST_FOR (seat.monitors, i, mon)
		if (monitor_arrange(mon) < 0)
			ERROR("Could not rearrange monitor %u after updating scratchpad margin: %s",
			      i, log_str());
	return 0;
}

static void
_attach_monitor(struct monitor *mon)
{
	struct workspace *ws;

	list_append(seat.monitors, mon);

	bus_channel_send(bus.monitor_attach, mon);

	ws = _get_or_create_free_workspace();
	if (!ws) {
		/* TODO propagate error upwards */
		log_set("Could not get free workspace for new monitor: %s",
		        log_str());
		goto error_freews;
	}

	monitor_show_workspace(mon, ws);

	if (!seat.selmon)
		seat_focus_monitor(mon);

	return;

 error_freews:
	/* TODO: propagate upwards */
	log_flush(LOG_ERROR);
}

static void
_bus_event(struct bus_subscription const *sub, void *data, void *ctx)
{
	struct bus_channel *ch = sub->channel;
	struct xwindow *xwin;
	struct client *c;
	struct xscreen *xscr;
	struct monitor *mon;

	(void) ctx;

	if (ch == bus.client_dimension) {
		c = data;
		if (!c->ws) {
			CRITICAL("Orphan client %lu redimensioned; ignoring",
			         c->xwindow->id);
			return;
		}
		if (!c->ws->mon)
			/* unmapped window; ignore */
			return;

		mon = seat_locate_rectangle(c->xwindow->dimension);
		if (!mon) {
			mon = seat.selmon;
			WARN("Client %lu has moved off-screen (%ux%u%+d%+d); moving back to focused monitor %ux%u%+d%+d (workspace '%s')",
			     c->xwindow->id,
			     c->xwindow->dimension.width,
			     c->xwindow->dimension.height,
			     c->xwindow->dimension.x, c->xwindow->dimension.y,
			     mon->dim.width, mon->dim.height, mon->dim.x, mon->dim.y,
			     mon->workspace->name);
			if (c->ws != mon->workspace) {
				if (workspace_transfer_client(mon->workspace, c) < 0) {
					log_set("Could not transfer client %lu to workspace '%s': %s",
					        c->xwindow->id, mon->workspace->name,
					        log_str());
					return;
				}
			} else {
				/* "snap" client back into the monitor area */
				if (monitor_arrange(mon) < 0)
					ERROR("Could not rearrange clients on monitor %ux%u%+d%+d: %s",
					      mon->dim.width, mon->dim.height,
					      mon->dim.x, mon->dim.y, log_str());
			}
		} else if (mon != c->ws->mon) {
			DEBUG("Client %lu has changed monitor; transferring from workspace '%s' to '%s'",
			      c->xwindow->id, c->ws->name, mon->workspace->name);
			if (workspace_transfer_client(mon->workspace, c) < 0)
				ERROR("Could not transfer client %lu to workspace '%s': %s",
				      c->xwindow->id, mon->workspace->name,
				      log_str());
			seat_focus_monitor(mon);
		}
	} else if (ch == bus.x_screen_attach) {
		xscr = data;
		mon = monitor_alloc(xscr);
		if (!mon) {
			CRITICAL("Could not create monitor to track X screen %ux%u%+d%+d: %s",
			         xscr->dimension.width, xscr->dimension.height,
			         xscr->dimension.x, xscr->dimension.y, log_str());
			return;
		}
		_attach_monitor(mon);
	} else if (ch == bus.x_screen_detach) {
		xscr = data;
		mon = list_find(seat.monitors,
		                xscr, (list_eqfunc) monitor_has_xscreen);
		if (!mon) {
			BUG("Seat received detach event for unmanaged X screen %ux%u%+d%+d",
			    xscr->dimension.width, xscr->dimension.height,
			    xscr->dimension.x, xscr->dimension.y);
			return;
		}
		_detach_monitor(mon);
	} else if (ch == bus.x_window_notify_focus) {
		xwin = data;
		if (!seat.selmon
		|| !seat.selmon->workspace
		|| !seat.selmon->workspace->selcli)
		{
			WARN("Seat received focus-in event on X window %lu without having a focused client; resetting to none",
			     xwin->id);
			XSetInputFocus(xserver.display, xserver.root->id,
			               RevertToPointerRoot, CurrentTime);
			return;
		}

		c = xwin->client;
		if (seat.selmon->workspace->selcli != c) {
			WARN("Seat received focus-in event on unfocused X window %lu; resetting to focused %lu",
			     xwin->id,
			     seat.selmon->workspace->selcli->xwindow->id);
			client_set_focus(seat.selmon->workspace->selcli, true);
			return;
		}
	} else {
		BUG("Seat received unexpected bus event '%s'", ch->name);
	}
}

static void
_detach_monitor(struct monitor *mon)
{
	if (mon->workspace)
		monitor_show_workspace(mon, NULL);

	if (mon == seat.selmon) {
		if (seat.monitors->size > 0)
			seat_focus_monitor(seat.monitors->elements[0]);
		else
			seat_focus_monitor(NULL);
	}

	bus_channel_send(bus.monitor_detach, mon);

	list_remove(seat.monitors, mon, NULL);
}

static struct workspace *
_get_or_create_free_workspace(void)
{
	struct workspace *ws;

	ws = state_next_free_workspace();
	if (ws)
		return ws;

	ws = workspace_alloc();
	if (!ws) {
		log_set("Could not create new free workspace: %s", log_str());
		goto error_alloc;
	}
	if (state_attach_workspace(ws) < 0) {
		log_set("Could not attach new free workspace to state: %s",
		        log_str());
		goto error_attach;
	}

	return ws;

 error_attach:
	log_push();
	workspace_free(ws);
	log_pop();
 error_alloc:
	return NULL;
}

static int
_handle_configurerequest(union xserver_request const *req,
                         enum xserver_request_handler_result *res,
                         void *ctx)
{
	struct xwindow *xwin;
	struct client *c;
	struct rectangle newdim;

	(void) ctx;

	if (req->type != XSERVER_REQUEST_CONFIGURE) {
		BUG("Unexpected X server request");
		return -1;
	}
	xwin = req->configure.xwindow;

	if (!xwin->client) {
		*res = XSERVER_REQUEST_PASSTHROUGH;
		return 0;
	}
	c = xwin->client;

	newdim = req->configure.dimension;
	if (c->floating) {
		/* floating window: redimension according to request */
		if (client_moveresize(c, newdim) < 0) {
			log_set("Could not move-resize floating client %lu to %ux%u%+d%+d: %s",
			        xwin->id, newdim.width, newdim.height,
			        newdim.x, newdim.y, log_str());
			goto error_moveresize;
		}
		*res = XSERVER_REQUEST_HANDLED;
	} else {
		/* tiled window: adapt only floating dimension and reject */
		if (client_moveresize_floating(c, newdim) < 0) {
			log_set("Could not move-resize client %lu to floating dimension %ux%u%+d%+d: %s",
			        xwin->id, newdim.width, newdim.height,
			        newdim.x, newdim.y, log_str());
			goto error_moveresize;
		}
		*res = XSERVER_REQUEST_REJECTED;
	}
	return 0;

 error_moveresize:
	return -1;
}

static int
_handle_maprequest(union xserver_request const *req,
                   enum xserver_request_handler_result *res,
                   void *ctx)
{
	struct xwindow *xwin;
	struct client *c;
	struct workspace *ws;

	(void) ctx;

	if (req->type != XSERVER_REQUEST_MAP) {
		BUG("Unexpected X server request");
		return -1;
	}
	xwin = req->map.xwindow;

	if (xwin->client) {
		c = xwin->client;
		WARN("Remapping pre-existing X window %lu; freeing associated client",
		     xwin->id);
		if (c->ws)
			workspace_detach_client(c->ws, c);
		client_free(c);
	}

	c = client_alloc(xwin);
	if (!c) {
		log_set("Could not create client for X window %lu: %s",
		        xwin->id, log_str());
		goto error_alloc;
	}

	ws = state_recommend_workspace(c);
	if (!ws)
		/* XXX: we assume there's always a selected workspace */
		ws = seat.selmon->selws;

	if (workspace_attach_client(ws, c) < 0) {
		log_set("Could not attach client to workspace '%s': %s",
		        ws->name, log_str());
		goto error_attach;
	}

	*res = XSERVER_REQUEST_HANDLED;
	return 0;

 error_attach:
	log_push();
	client_free(c);
	log_pop();
 error_alloc:
	return -1;
}

static int
_init_bus(void)
{
	int unsigned i;
	struct {
		struct bus_channel *const *channel;
		void const *mask;
	} subs[] = {
		{ &bus.client_dimension, BUS_SUBSCRIPTION_DATA_MASK_ALL },
		{ &bus.x_screen_attach, BUS_SUBSCRIPTION_DATA_MASK_ALL },
		{ &bus.x_screen_detach, BUS_SUBSCRIPTION_DATA_MASK_ALL },
		{ &bus.x_window_notify_focus, BUS_SUBSCRIPTION_DATA_MASK_ALL },
	};

	_bus_session = bus_connect("seat", _bus_event, NULL);
	if (!_bus_session) {
		log_set("Could not register on event bus: %s", log_str());
		goto error_register;
	}

	for (i = 0; i < LENGTH(subs); ++i) {
		if (!bus_subscription_alloc(_bus_session,
		                            *subs[i].channel, subs[i].mask))
		{
			log_set("Could not join event bus channel '%s': %s",
			        (*subs[i].channel)->name, log_str());
			goto error_subscribe;
		}
	}

	return 0;

 error_subscribe:
	log_push();
	bus_disconnect(_bus_session);
	log_pop();
 error_register:
	return -1;
}

static int
_init_monitors(void)
{
	int unsigned i;
	struct xscreen *xscr;
	struct monitor *mon;

	seat.monitors = list_alloc();
	if (!seat.monitors) {
		log_set("Could not create list for monitors: %s", log_str());
		goto error_list;
	}

	DEBUG("Tracking %zu X screens as monitors", xoutput.screens->size);
	LIST_FOR (xoutput.screens, i, xscr) {
		mon = monitor_alloc(xscr);
		if (!mon) {
			log_set("Could not create monitor for X screen %ux%u%+d%+d: %s",
			        xscr->dimension.width, xscr->dimension.height,
			        xscr->dimension.x, xscr->dimension.y,
			        log_str());
			goto error_monitor;
		}
		_attach_monitor(mon);
	}

	return 0;

 error_monitor:
	log_push();
	while (seat.monitors->size > 0) {
		mon = seat.monitors->elements[0];
		_detach_monitor(mon);
		monitor_free(mon);
	}
	list_free(seat.monitors);
	log_pop();
 error_list:
	return -1;
}
