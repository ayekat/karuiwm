#include <karuiwm/input.h>
#include <karuiwm/bus.h>
#include <karuiwm/buttonmap.h>
#include <karuiwm/cursor.h>
#include <karuiwm/keybind.h>
#include <karuiwm/keymap.h>
#include <karuiwm/seat.h>
#include <karuiwm/utils.h>
#include <karuiwm/xevent.h>
#include <karuiwm/xserver.h>
#include <karuiwm/xwindow.h>
#include <karui/list.h>
#include <karui/log.h>

static void _bus_event(struct bus_subscription const *sub, void *data,
                       void *ctx);
static int _init_bus(void);

static struct {
	struct bus_session *bus_session;
	struct list *keymaps;
	struct keymap *active_keymap;
	struct list *buttonmaps;
	struct buttonmap *active_buttonmap;
} _input;

int
input_init(void)
{
	/* bus */
	if (_init_bus() < 0) {
		log_set("Could not initialise bus: %s", log_str());
		goto error_bus;
	}

	/* keymaps */
	_input.keymaps = list_alloc();
	if (!_input.keymaps) {
		log_set("Could not create list for keymaps: %s", log_str());
		goto error_keymap_list;
	}
	_input.active_keymap = NULL;
	if (input_create_keymap(INPUT_KEYMAP_DEFAULT, false) < 0) {
		log_set("Could not create default keymap '%s': %s",
		        INPUT_KEYMAP_DEFAULT, log_str());
		goto error_keymap_create;
	}
	if (input_set_active_keymap(INPUT_KEYMAP_DEFAULT) < 0) {
		log_set("Could not set active keymap to '%s': %s",
		        INPUT_KEYMAP_DEFAULT, log_str());
		goto error_keymap_activate;
	}

	/* buttonmaps */
	_input.buttonmaps = list_alloc();
	if (!_input.buttonmaps) {
		log_set("Could not create list for buttonmaps: %s", log_str());
		goto error_buttonmap_list;
	}
	_input.active_buttonmap = NULL;
	if (input_create_buttonmap(INPUT_BUTTONMAP_DEFAULT) < 0) {
		log_set("Could not create default buttonmap '%s': %s",
		        INPUT_BUTTONMAP_DEFAULT, log_str());
		goto error_buttonmap_create;
	}
	if (input_set_active_buttonmap(INPUT_BUTTONMAP_DEFAULT) < 0) {
		log_set("Could not set buttonmap to '%s': %s",
		        INPUT_BUTTONMAP_DEFAULT, log_str());
		goto error_buttonmap_activate;
	}

	/* cursor */
	if (cursor_init() < 0) {
		log_set("Could not initialise cursor: %s", log_str());
		goto error_cursor;
	}

	return 0;

 error_cursor:
	log_push();
	input_set_active_buttonmap(NULL);
	log_pop();
 error_buttonmap_activate:
	log_push();
	input_destroy_buttonmap(INPUT_BUTTONMAP_DEFAULT);
	log_pop();
 error_buttonmap_create:
	log_push();
	list_free(_input.buttonmaps);
	log_pop();
 error_buttonmap_list:
	log_push();
	input_set_active_keymap(NULL);
	log_pop();
 error_keymap_activate:
	log_push();
	input_destroy_keymap(INPUT_KEYMAP_DEFAULT);
	log_pop();
 error_keymap_create:
	log_push();
	list_free(_input.keymaps);
	log_pop();
 error_keymap_list:
	log_push();
	bus_disconnect(_input.bus_session);
	log_pop();
 error_bus:
	return -1;
}

void
input_deinit(void)
{
	/* cursor */
	cursor_deinit();

	/* buttonmaps */
	if (input_set_active_buttonmap(NULL) < 0)
		CRITICAL("Could not unset active buttonmap: %s", log_str());
	if (input_destroy_buttonmap(INPUT_BUTTONMAP_DEFAULT) < 0)
		CRITICAL("Could not destroy default buttonmap '%s': %s",
		         INPUT_BUTTONMAP_DEFAULT, log_str());
	if (_input.active_buttonmap) {
		WARN("Buttonmap '%s' is still active",
		     _input.active_buttonmap->name);
		if (input_set_active_buttonmap(NULL) < 0)
			CRITICAL("Could not unset active buttonmap: %s",
			         log_str());
	}
	if (_input.buttonmaps->size > 0) {
		WARN("List of buttonmaps not empty, clearing");
		list_clear(_input.buttonmaps, (list_delfunc) buttonmap_free);
	}
	list_free(_input.buttonmaps);

	/* keymaps */
	if (input_set_active_keymap(NULL) < 0)
		CRITICAL("Could not unset active keymap: %s", log_str());
	if (input_destroy_keymap(INPUT_KEYMAP_DEFAULT) < 0)
		CRITICAL("Could not destroy default keymap '%s': %s",
		         INPUT_KEYMAP_DEFAULT, log_str());
	if (_input.active_keymap) {
		WARN("Keymap '%s' is still active", _input.active_keymap->name);
		if (input_set_active_keymap(NULL) < 0)
			CRITICAL("Could not unset active keymap: %s",
			         log_str());
	}
	if (_input.keymaps->size > 0) {
		WARN("List of keymaps not empty, clearing");
		list_clear(_input.keymaps, (list_delfunc) keymap_free);
	}
	list_free(_input.keymaps);

	/* bus */
	bus_disconnect(_input.bus_session);
}

int
input_create_buttonmap(char const *name)
{
	struct buttonmap *bm;

	bm = buttonmap_alloc(name);
	if (!bm) {
		log_set("Could not create buttonmap '%s': %s", name, log_str());
		goto error_buttonmap;
	}

	list_append(_input.buttonmaps, bm);

	return 0;

 error_buttonmap:
	return -1;
}

int
input_create_keymap(char const *name, bool grab_keyboard)
{
	struct keymap *km;

	km = keymap_alloc(name, grab_keyboard);
	if (!km) {
		log_set("Could not create keymap '%s': %s", name, log_str());
		goto error_keymap;
	}

	list_append(_input.keymaps, km);

	return 0;

 error_keymap:
	return -1;
}

int
input_destroy_buttonmap(char const *name)
{
	struct buttonmap *bm;

	DEBUG("Destroying buttonmap %s", name);

	bm = list_find(_input.buttonmaps, name, (list_eqfunc) buttonmap_has_name);
	if (!bm) {
		log_set("Buttonmap %s is not registered", name);
		return -1;
	}

	if (bm == _input.active_buttonmap) {
		/* TODO: Handle this more carefully */
		WARN("Buttonmap %s is the active buttonmap; deactivating",
		     name);
		if (input_set_active_buttonmap(NULL) < 0)
			CRITICAL("Could not unset active buttonmap: %s",
			         log_str());
	}

	list_remove(_input.buttonmaps, bm, NULL);
	buttonmap_free(bm);
	return 0;
}

int
input_destroy_keymap(char const *name)
{
	struct keymap *km;

	DEBUG("Destroying keymap %s", name);

	km = list_find(_input.keymaps, name, (list_eqfunc) keymap_has_name);
	if (!km) {
		log_set("Keymap %s is not registered", name);
		return -1;
	}

	if (km == _input.active_keymap) {
		/* TODO: Handle this more carefully */
		WARN("Keymap %s is the active keymap; deactivating", name);
		if (input_set_active_keymap(NULL) < 0)
			CRITICAL("Could not unset active keymap: %s",
			         log_str());
	}

	list_remove(_input.keymaps, km, NULL);
	keymap_free(km);
	return 0;
}

struct buttonmap *
input_find_buttonmap_by_name(char const *name)
{
	return list_find(_input.buttonmaps,
	                 name, (list_eqfunc) buttonmap_has_name);
}

struct keymap *
input_find_keymap_by_name(char const *name)
{
	return list_find(_input.keymaps, name, (list_eqfunc) keymap_has_name);
}

int
input_resolve_button(struct button_event const *bev)
{
	if (!_input.active_buttonmap) {
		log_set("No active buttonmap");
		return -1;
	}

	return buttonmap_resolve_button(_input.active_buttonmap, bev);
}

int
input_resolve_key(struct key const *k)
{
	if (!_input.active_keymap) {
		log_set("No active keymap");
		return -1;
	}

	return keymap_resolve_key(_input.active_keymap, k);
}

int
input_set_active_buttonmap(char const *name)
{
	struct buttonmap *bm;

	DEBUG("Setting active buttonmap to %s", name);

	if (!name) {
		/* set no active buttonmap */
		if (_input.active_buttonmap) {
			if (buttonmap_deactivate(_input.active_buttonmap) < 0) {
				log_set("Could not deactivate buttonmap '%s': %s",
				        _input.active_buttonmap->name,
				        log_str());
				return -1;
			}
			_input.active_buttonmap = NULL;
		}
		return 0;
	}

	bm = list_find(_input.buttonmaps,
	               name, (list_eqfunc) buttonmap_has_name);
	if (!bm) {
		log_set("Buttonmap '%s' is not registered", name);
		return -1;
	}

	if (_input.active_buttonmap) {
		if (buttonmap_deactivate(_input.active_buttonmap) < 0) {
			log_set("Could not deactivate buttonmap '%s': %s",
			        _input.active_buttonmap->name, log_str());
			goto error_deactivate;
		}
	}
	if (buttonmap_activate(bm) < 0) {
		log_set("Could not activate buttonmap '%s': %s",
		        bm->name, log_str());
		goto error_activate;
	}
	_input.active_buttonmap = bm;

	return 0;

 error_activate:
	if (_input.active_buttonmap) {
		log_push();
		DEBUG("Reactivating old buttonmap '%s'",
		      _input.active_buttonmap->name);
		if (buttonmap_activate(_input.active_buttonmap) < 0) {
			CRITICAL("Could not reactivate old buttonmap '%s' while handling error: %s",
			         _input.active_buttonmap->name, log_str());
			_input.active_buttonmap = NULL;
		}
		log_pop();
	}
 error_deactivate:
	return -1;
}

int
input_set_active_keymap(char const *name)
{
	struct keymap *km;

	DEBUG("Setting active keymap to '%s'", name);

	if (!name) {
		/* set no active keymap */
		if (_input.active_keymap) {
			if (keymap_deactivate(_input.active_keymap) < 0) {
				log_set("Could not deactivate keymap '%s': %s",
				        _input.active_keymap->name, log_str());
				return -1;
			}
			_input.active_keymap = NULL;
		}
		return 0;
	}

	km = list_find(_input.keymaps, name, (list_eqfunc) keymap_has_name);
	if (!km) {
		log_set("Keymap '%s' is not registered", name);
		return -1;
	}

	if (_input.active_keymap) {
		if (keymap_deactivate(_input.active_keymap) < 0) {
			log_set("Could not deactivate keymap '%s': %s",
			        _input.active_keymap->name, log_str());
			goto error_deactivate;
		}
	}
	if (keymap_activate(km) < 0) {
		log_set("Could not activate keymap '%s': %s",
		        km->name, log_str());
		goto error_activate;
	}
	_input.active_keymap = km;

	return 0;

 error_activate:
	if (_input.active_keymap) {
		log_push();
		DEBUG("Reactivating old keymap '%s'",
		      _input.active_keymap->name);
		if (keymap_activate(_input.active_keymap) < 0) {
			CRITICAL("Could not reactivate old keymap '%s': %s",
			         _input.active_keymap->name, log_str());
			_input.active_keymap = NULL;
		}
		log_pop();
	}
 error_deactivate:
	return -1;
}

int
input_unbind_button(char const *buttonmap_name, struct button const *b)
{
	struct buttonmap *bm;

	DEBUG("Unbinding button %u+%u in buttonmap %s",
	      b->mod, b->button, buttonmap_name);

	bm = list_find(_input.buttonmaps, buttonmap_name,
	               (list_eqfunc) buttonmap_has_name);
	if (!bm) {
		log_set("Buttonmap '%s' is not registered", buttonmap_name);
		return -1;
	}

	return buttonmap_unbind_button(bm, b);
}

int
input_unbind_key(char const *keymap_name, struct key const *k)
{
	struct keymap *km;

	DEBUG("Unbinding key %u+%s in keymap %s",
	      k->mod, XKeysymToString(k->sym), keymap_name);

	km = list_find(_input.keymaps,
	               keymap_name, (list_eqfunc) keymap_has_name);
	if (!km) {
		log_set("Keymap '%s' is not registered", keymap_name);
		return -1;
	}

	return keymap_unbind_key(km, k);
}

static void
_bus_event(struct bus_subscription const *sub, void *data, void *ctx)
{
	struct bus_channel *ch = sub->channel;
	struct xwindow *xwin;
	struct client *c;
	struct workspace *ws;
	struct monitor *mon;
	struct xevent_key const *xkev;
	struct xevent_button const *xbev;
	struct xevent_pointer const *xpev;
	struct button_event bev;

	(void) ctx;

	if (ch == bus.x_key_notify) {
		xkev = data;
		if (input_resolve_key(&xkev->key) < 0)
			ERROR("Input could not resolve key event %s: %s",
			      key_event_type_str(xkev->type), log_str());
	} else if (ch == bus.x_mouse_notify) {
		xbev = data;
		xwin = xbev->xwindow;
		c = xwin->client;
		if (!c)
			return;
		bev.type = xbev->type;
		bev.button = xbev->button;
		bev.position = xbev->position;
		bev.client = c;
		if (input_resolve_button(&bev) < 0)
			ERROR("Input could not resolve mouse event %s: %s",
			      button_event_type_str(bev.type), log_str());
	} else if (ch == bus.x_pointer_notify) {
		/* extract information from event */
		xpev = data;
		if (xpev->xwindow == xserver.root
		&& xpev->type != POINTER_LEAVE)
		{
			mon = seat_locate_point(xpev->pointer.position);
			if (!mon) {
				WARN("Pointer event on root window at %+d%+d not covered by monitor; ignoring",
				     xpev->pointer.position.x,
				     xpev->pointer.position.y);
			} else {
				seat_focus_monitor(mon);
			}
			return;
		}
		if (xpev->type != POINTER_ENTER)
			/* Pointer move events we're interested in are handled
			 * via mouse-move events.
			 */
			return;
		c = xpev->xwindow->client;
		if (!c) {
			DEBUG("Pointer enter event on unmanaged X window %lu; ignoring",
			      xpev->xwindow->id);
			return;
		}

		/* determine location within state and seat */
		ws = c->ws;
		if (!ws) {
			WARN("Pointer enter event on client %lu that is not attached to any workspace; ignoring",
			     c->xwindow->id);
			return;
		}
		mon = ws->mon;
		if (!mon) {
			WARN("Pointer enter event on client %lu on invisible workspace '%s'; ignoring",
			     c->xwindow->id, ws->name);
			return;
		}

		/* update focus */
		if (mon != seat.selmon)
			seat_focus_monitor(mon);
		if (c != ws->selcli) {
			/* only update client focus if the scratchpad is not shown (or
			 * the client is in the scratchpad itself)
			 */
			if (!ws->mon->scratchpad || ws == ws->mon->scratchpad)
				workspace_focus_client(ws, c);
		}
	}
}

static int
_init_bus(void)
{
	int unsigned i;
	struct {
		struct bus_channel *const *channel;
		void const *mask;
	} subs[] = {
		{ &bus.x_key_notify, BUS_SUBSCRIPTION_DATA_MASK_ALL },
		{ &bus.x_mouse_notify, BUS_SUBSCRIPTION_DATA_MASK_ALL },
		{ &bus.x_pointer_notify, BUS_SUBSCRIPTION_DATA_MASK_ALL },
	};

	_input.bus_session = bus_connect("input", _bus_event, NULL);
	if (!_input.bus_session) {
		log_set("Could not connect to event bus: %s", log_str());
		goto error_register;
	}

	for (i = 0; i < LENGTH(subs); ++i) {
		if (!bus_subscription_alloc(_input.bus_session,
		                            *subs[i].channel, subs[i].mask))
		{
			log_set("Could not join event bus channel '%s': %s",
			        (*subs[i].channel)->name, log_str());
			goto error_subscribe;
		}
	}

	return 0;

 error_subscribe:
	log_push();
	bus_disconnect(_input.bus_session);
	log_pop();
 error_register:
	return -1;
}
