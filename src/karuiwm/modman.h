#ifndef _KWM_MODMAN_H
#define _KWM_MODMAN_H

int modman_init(void);
void modman_deinit(void);

int modman_load(char const *name);

#endif /* ndef _KWM_MODMAN_H */
