#include <karuiwm/rpc.h>
#include <karuiwm/fdio.h>
#include <karui/cstr.h>
#include <karui/envvars.h>
#include <karui/list.h>
#include <karui/log.h>
#include <karui/memory.h>
#include <karui/runtimedir.h>
#include <errno.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <unistd.h>

static int _fd_callback(int fd, void *data);

static struct {
	int fd;
	char *path;
	struct list *clients;
	struct fdio_client *fdio;
} _rpc;

int
rpc_init(void)
{
	int ret;
	struct sockaddr_un saddr;

	/* prepare clients */
	_rpc.clients = list_alloc();
	if (!_rpc.clients) {
		log_set("Could not create RPC socket client list");
		goto error_clients;
	}

	/* determine socket path */
	_rpc.path = cstr_format("%s/rpc.sock", runtimedir_get());
	if (!_rpc.path) {
		log_set("Could not create string for socket path: %s",
		        log_str());
		goto error_path;
	}
	DEBUG("Creating socket %s", _rpc.path);

	/* create new socket */
	remove(_rpc.path); /* remove left-over socket from a previous run */
	_rpc.fd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (_rpc.fd < 0) {
		log_set("Could not create socket: %s", strerror(errno));
		goto error_socket;
	}

	/* bind to socket */
	(void) memset(&saddr, '\0', sizeof(struct sockaddr_un));
	saddr.sun_family = AF_UNIX;
	(void) strncpy(saddr.sun_path, _rpc.path, sizeof(saddr.sun_path) - 1);
	ret = bind(_rpc.fd, (struct sockaddr *) &saddr,
	           sizeof(struct sockaddr_un));
	if (ret < 0) {
		log_set("Could not bind socket: %s", strerror(errno));
		goto error_bind;
	}

	/* listen to socket */
	if (listen(_rpc.fd, 10) < 0) {
		log_set("Could not listen to socket: %s", strerror(errno));
		goto error_listen;
	}

	/* register at file descriptor I/O */
	_rpc.fdio = fdio_register("rpc", _fd_callback, NULL);
	if (!_rpc.fdio) {
		log_set("Could not register to I/O: %s", log_str());
		goto error_fdio_register;
	}
	if (fdio_add_fd(_rpc.fdio, &_rpc.fd) < 0) {
		log_set("Could not add socket file descriptor to I/O: %s",
		        log_str());
		goto error_fdio_add;
	}

	return 0;

 error_fdio_add:
	log_push();
	fdio_unregister(_rpc.fdio);
	log_pop();
 error_fdio_register:
 error_listen:
 error_bind:
	close(_rpc.fd);
	log_push();
	if (unlink(_rpc.path) < 0)
		CRITICAL("Could not delete %s while handling error: %s",
		         _rpc.path, strerror(errno));
	log_pop();
 error_socket:
	log_push();
	cstr_free(_rpc.path);
	log_pop();
 error_path:
	log_push();
	list_free(_rpc.clients);
	log_pop();
 error_clients:
	return -1;
}

void
rpc_deinit(void)
{
	/* stop listening */
	close(_rpc.fd);
	if (unlink(_rpc.path) < 0)
		ERROR("Could not delete %s: %s", _rpc.path, strerror(errno));
	cstr_free(_rpc.path);

	/* close client connections */
	list_clear(_rpc.clients, (list_delfunc) rpc_client_free);
	list_free(_rpc.clients);

}

int
rpc_clean_up(void)
{
	int unsigned i;
	struct rpc_client *rpcc;

	LIST_FOR (_rpc.clients, i, rpcc) {
		if (rpcc->fd < 0) {
			list_remove(_rpc.clients, rpcc, NULL);
			--i;
			rpc_client_free(rpcc);
		}
	}

	return 0;
}

static int
_fd_callback(int fd, void *data)
{
	struct rpc_client *rpcc;
	struct sockaddr_un *caddr;
	socklen_t caddr_len;

	(void) data;

	caddr_len = sizeof(struct sockaddr_un);
	fd = accept(_rpc.fd, (struct sockaddr *) &caddr, &caddr_len);
	if (fd < 0) {
		log_set("Could not accept connection on RPC socket: %s",
		        strerror(errno));
		goto error_accept;
	}
	DEBUG("New RPC connection on socket %d", fd);

	rpcc = rpc_client_alloc(fd);
	if (!rpcc) {
		log_set("Could not allocate %zu bytes for RPC client structure: %s",
		        sizeof(struct rpc_client), strerror(errno));
		goto error_client;
	}

	list_append(_rpc.clients, rpcc);

	return 0;

 error_client:
	close(fd);
 error_accept:
	return -1;
}
