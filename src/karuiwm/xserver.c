#include <karuiwm/xserver.h>
#include <karuiwm/bus.h>
#include <karuiwm/fdio.h>
#include <karuiwm/karuiwm.h>
#include <karuiwm/utils.h>
#include <karuiwm/xevent.h>
#include <karuiwm/xoutput.h>
#include <karuiwm/xscreen.h>
#include <karui/list.h>
#include <karui/log.h>
#include <karui/memory.h>
#include <X11/Xproto.h>

struct _xrequest_handler {
	int (*handle)(union xserver_request const *,
	              enum xserver_request_handler_result *,
	              void *);
	void *context;
};

static int _fd_callback(int fd, void *data);
static struct xwindow *_find_event_window(Window wid, char const *eventstr);
static int _handle_xerror(Display *dpy, XErrorEvent *ee);
static void _handle_xevent_buttonpress(XEvent *xev);
static void _handle_xevent_buttonrelease(XEvent *xev);
static void _handle_xevent_clientmessage(XEvent *xev);
static void _handle_xevent_configurerequest(XEvent *xev);
static void _handle_xevent_configurenotify(XEvent *xev);
static void _handle_xevent_createnotify(XEvent *xev);
static void _handle_xevent_destroynotify(XEvent *xev);
static void _handle_xevent_enternotify(XEvent *xev);
static void _handle_xevent_expose(XEvent *xev);
static void _handle_xevent_focusin(XEvent *xev);
static void _handle_xevent_keypress(XEvent *xev);
static void _handle_xevent_maprequest(XEvent *xev);
static void _handle_xevent_motionnotify(XEvent *xev);
static void _handle_xevent_propertynotify(XEvent *xev);
static void _handle_xevent_unmapnotify(XEvent *xev);
static int _init_atoms(void);
static int _init_rootwin(void);
static char const *_request_type_str(enum xserver_request_type reqtype);
static int _scan_server(void);
static int _xwindow_has_override_redirect(Window id, bool *override_redirect);

struct xserver xserver;

static void (*karuiwm_xhandle[LASTEvent])(XEvent *) = {
	[ButtonPress]      = _handle_xevent_buttonpress,       /* 4*/
	[ButtonRelease]    = _handle_xevent_buttonrelease,     /* 5*/
	[ClientMessage]    = _handle_xevent_clientmessage,     /*33*/
	[ConfigureNotify]  = _handle_xevent_configurenotify,   /*22*/
	[ConfigureRequest] = _handle_xevent_configurerequest,  /*23*/
	[CreateNotify]     = _handle_xevent_createnotify,      /*16*/
	[DestroyNotify]    = _handle_xevent_destroynotify,     /*17*/
	[EnterNotify]      = _handle_xevent_enternotify,       /* 7*/
	[Expose]           = _handle_xevent_expose,            /*12*/
	[FocusIn]          = _handle_xevent_focusin,           /* 9*/
	[KeyPress]         = _handle_xevent_keypress,          /* 2*/
	[MapRequest]       = _handle_xevent_maprequest,        /*20*/
	[MotionNotify]     = _handle_xevent_motionnotify,      /* 6*/
	[PropertyNotify]   = _handle_xevent_propertynotify,    /*28*/
	[UnmapNotify]      = _handle_xevent_unmapnotify        /*18*/
};

static struct {
	int (*xerror)(Display *, XErrorEvent *);
	struct fdio_client *fdio;
	struct bus_session *bus_session;
	struct {
		struct xwindow *xwindow;
		struct button button;
	} grabbed;
	struct {
		struct _xrequest_handler *configure;
		struct _xrequest_handler *map;
	} request_handlers;
} _xserver;

int
xserver_init(void)
{
	/* locale */
	if (!XSupportsLocale()) {
		log_set("Xlib does not support the currently set locale '%s'",
		        karuiwm.locale);
		goto error_locale;
	}

	/* server connection */
	xserver.display = XOpenDisplay(NULL);
	if (!xserver.display) {
		log_set("Could not open connection to X server display");
		goto error_display;
	}
	xserver.fd = ConnectionNumber(xserver.display);

	/* error handler */
	_xserver.xerror = XSetErrorHandler(_handle_xerror);

	/* output */
	if (xoutput_init() < 0) {
		log_set("Could not initialise output component: %s", log_str());
		goto error_output;
	}

	/* root window */
	if (_init_rootwin() < 0) {
		log_set("Could not initialise root window: %s", log_str());
		goto error_rootwin;
	}
	xserver.graphics_context = XCreateGC(xserver.display, xserver.root->id,
	                                     0x0l, NULL);

	/* other windows */
	xserver.windows = list_alloc();
	if (!xserver.windows) {
		log_set("Could not create list for windows: %s", log_str());
		goto error_windows;
	}
	_xserver.grabbed.xwindow = NULL;

	/* atoms */
	if (_init_atoms() < 0) {
		log_set("Could not initialise atoms: %s", log_str());
		goto error_atoms;
	}

	/* X events */
	_xserver.fdio = fdio_register("xserver", _fd_callback, NULL);
	if (!_xserver.fdio) {
		log_set("Could not register at I/O: %s", log_str());
		goto error_fdio_register;
	}
	if (fdio_add_fd(_xserver.fdio, &xserver.fd) < 0) {
		log_set("Could not add X server connection file descriptor %d to I/O: %s",
		        xserver.fd, log_str());
		goto error_fdio_add;
	}
	_xserver.request_handlers.configure = NULL;
	_xserver.request_handlers.map = NULL;

	if (_scan_server() < 0) {
		log_set("Could not scan X server for windows: %s", log_str());
		goto error_scan;
	}

	XSync(xserver.display, False);
	return 0;

 error_scan:
 error_fdio_add:
	log_push();
	fdio_unregister(_xserver.fdio);
	log_pop();
 error_fdio_register:
 error_atoms:
	log_push();
	list_free(xserver.windows);
	log_pop();
 error_windows:
	XFreeGC(xserver.display, xserver.graphics_context);
	log_push();
	xwindow_free(xserver.root);
	log_pop();
 error_rootwin:
	log_push();
	xoutput_deinit();
	log_pop();
 error_output:
	XCloseDisplay(xserver.display);
 error_display:
 error_locale:
	return -1;
}

void
xserver_deinit(void)
{
	if (_xserver.request_handlers.configure) {
		WARN("Handler for %s still installed; removing",
		     _request_type_str(XSERVER_REQUEST_CONFIGURE));
		memory_free(_xserver.request_handlers.configure);
		_xserver.request_handlers.configure = NULL;
	}
	if (_xserver.request_handlers.map) {
		WARN("Handler for %s still installed; removing",
		     _request_type_str(XSERVER_REQUEST_MAP));
		memory_free(_xserver.request_handlers.map);
		_xserver.request_handlers.map = NULL;
	}
	fdio_unregister(_xserver.fdio);

	list_clear(xserver.windows, (list_delfunc) xwindow_free);
	list_free(xserver.windows);
	XFreeGC(xserver.display, xserver.graphics_context);
	xwindow_free(xserver.root);

	xoutput_deinit();

	XCloseDisplay(xserver.display);
}

int
xserver_add_request_handler(enum xserver_request_type reqtype,
                            int (*cb)(union xserver_request const *,
                                      enum xserver_request_handler_result *,
                                      void *),
                            void *ctx)
{
	struct _xrequest_handler **xrh = NULL;

	switch (reqtype) {
	case XSERVER_REQUEST_CONFIGURE:
		xrh = &_xserver.request_handlers.configure;
		break;
	case XSERVER_REQUEST_MAP:
		xrh = &_xserver.request_handlers.map;
		break;
	}
	DEBUG("Adding X request handler for %s", _request_type_str(reqtype));

	if (*xrh) {
		log_set("%s already has handler assigned",
		        _request_type_str(reqtype));
		return -1;
	}

	*xrh = memory_alloc(sizeof(struct _xrequest_handler),
	                    "X request handler struct");
	if (!*xrh)
		goto error_alloc;

	(*xrh)->handle = cb;
	(*xrh)->context = ctx;

	return 0;

 error_alloc:
	return -1;
}

struct xwindow *
xserver_create_xwindow(struct rectangle dim, int unsigned border_width,
                       int long unsigned event_mask, bool override_redirect)
{
	XSetWindowAttributes wa;
	Window wid;
	struct xwindow *xwin;

	wa.override_redirect = (Bool) override_redirect;
	wa.background_pixmap = ParentRelative;
	wa.event_mask = (int long signed) event_mask;

	wid = XCreateWindow(xserver.display, xserver.root->id,
	                    dim.x, dim.y, dim.width, dim.height,
	                    border_width,
	                    (int signed) xoutput.screen_depth,
	                    CopyFromParent,
	                    DefaultVisual(xserver.display, xoutput.screen),
	                    CWOverrideRedirect|CWBackPixmap|CWEventMask,
	                    &wa);
	if (!wid) {
		log_set("XCreateWindow() failed");
		goto error_create;
	}

	xwin = xwindow_alloc(wid, &dim, event_mask, override_redirect,
	                     XWINDOW_CLASS_OWNED);
	if (!xwin) {
		log_set("Could not create X window structure for %lu: %s",
		        wid, log_str());
		goto error_alloc;
	}

	list_append(xserver.windows, xwin);

	return xwin;

 error_alloc:
	if (!XDestroyWindow(xserver.display, wid)) {
		log_push();
		ERROR("XDestroyWindow() failed to destroy X window %lu for failed xwindow struct",
		      wid);
		log_pop();
	}
 error_create:
	return NULL;
}

void
xserver_destroy_xwindow(struct xwindow *xwin)
{
	Window wid = xwin->id;

	if (xwin->class != XWINDOW_CLASS_OWNED) {
		BUG("Attempt to destroy X window %lu not owned by us", wid);
		return;
	}

	DEBUG("Destroying owned X window %lu", xwin->id);
	/* TODO: Send atom? */
	if (!XDestroyWindow(xserver.display, wid))
		ERROR("XDestroyWindow() failed to destroy owned X window %lu",
		      xwin->id);

	XSync(xserver.display, False);
}

struct xwindow *
xserver_find_xwindow(Window id)
{
	return list_find(xserver.windows, &id, (list_eqfunc) xwindow_has_id);
}

void
xserver_remove_request_handler(enum xserver_request_type reqtype,
                               int (*cb)(union xserver_request const *,
                                         enum xserver_request_handler_result *,
                                         void *))
{
	struct _xrequest_handler **xrh = NULL;

	switch (reqtype) {
	case XSERVER_REQUEST_CONFIGURE:
		xrh = &_xserver.request_handlers.configure;
		break;
	case XSERVER_REQUEST_MAP:
		xrh = &_xserver.request_handlers.map;
		break;
	}

	/* silently ignore request to remove request handler for non-matching
	 * callback
	 */
	if (!*xrh || (*xrh)->handle != cb)
		return;

	memory_free(*xrh);
	*xrh = NULL;
}

static int
_fd_callback(int fd, void *data)
{
	XEvent xev;

	(void) fd;
	(void) data;

	while (XPending(xserver.display) > 0) {
		if (XNextEvent(xserver.display, &xev)) {
			ERROR("Could not get next X event");
			continue;
		}
		if (karuiwm_xhandle[xev.type])
			karuiwm_xhandle[xev.type](&xev);

		XSync(xserver.display, False);
	}

	return 0;
}

static struct xwindow *
_find_event_window(Window wid, char const *eventstr)
{
	struct xwindow *xwin;
	bool or;

	if (wid == xserver.root->id)
		return xserver.root;

	xwin = list_find(xserver.windows, &wid, (list_eqfunc) xwindow_has_id);
	if (!xwin) {
		BUG("%s for unknown X window %lu; adding", eventstr, wid);
		if (_xwindow_has_override_redirect(wid, &or) < 0) {
			WARN("X server: Could not determine override_redirect for X window %lu; assuming yes (%s)",
			     wid, log_str());
			or = true;
		}
		xwin = xwindow_alloc(wid, NULL, 0x0LU, or,
		                     XWINDOW_CLASS_CLIENT);
		if (!xwin) {
			log_set("Could not create struct for X window %lu: %s",
			        wid, log_str());
			return NULL;
		}
		list_append(xserver.windows, xwin);
	}

	return xwin;
}

static int
_handle_xerror(Display *dpy, XErrorEvent *ee)
{
	char errstr[1024];

	/* Given the asynchronic nature of X, we may occasionally send a request
	 * for a window that is no longer valid (but we haven't noticed yet
	 * because haven't received the event yet). Consequently, certain types
	 * of errors can simply not be avoided.
	 *
	 * To prevent such errors from terminating our program, we catch some of
	 * them here.
	 *
	 * The error codes are defined in X11/X.h, the request codes in
	 * X11/Xproto.h.
	 */

	/* Errors that we want to silently ignore, because they are fully
	 * expected to happen.
	 */
	bool handle_silent = false
		/* We use XGetWindowAttributes on each new window, so this will
		 * naturally cause a BadWindow error on override_redirect
		 * windows.
		 */
		|| (ee->request_code == X_GetWindowAttributes && ee->error_code == BadWindow)

		/* BadWindow and BadDrawable can happen if we're handling an
		 * earlier event for a window that no longer exists (as we have
		 * simply not seen the XDestroyEvent for it yet).
		 */
		|| ee->error_code == BadWindow
		|| ee->error_code == BadDrawable

		/* BadMatch can happen if we try to focus a window that is not
		 * mapped (in case we haven't seen an unmap event initiated by
		 * the client).
		 */
		|| (ee->request_code == X_SetInputFocus && ee->error_code == BadMatch)
		;

	/* Errors that we occasionally encounter (and we should clarify whether
	 * they can be avoided), but that we don't want to crash karuiwm.
	 */
	bool handle = handle_silent;

	/* For handled events, we simply return.
	 */
	if (handle) {
		if (!handle_silent) {
			XGetErrorText(dpy, ee->error_code, errstr, 1024);
			ERROR("X Error %d (%s) after request %d:%d",
			      ee->error_code, errstr, ee->request_code,
			      ee->minor_code);
		}
		return 0;
	}

	/* Pass everything else to default error handler (might call exit). */
	return _xserver.xerror(dpy, ee);
}

static void
_handle_xevent_keypress(XEvent *xev)
{
	XKeyEvent *xkev = &xev->xkey;
	struct xevent_key kev;

	kev.type = KEY_PRESS;
	kev.key.mod = xkev->state;
	kev.key.sym = XLookupKeysym(xkev, 0);

	bus_channel_send(bus.x_key_notify, &kev);
}

static void
_handle_xevent_buttonpress(XEvent *xev)
{
	XButtonEvent *xbev = &xev->xbutton;
	struct xwindow *xwin;
	struct xevent_button cbev;

	DEBUG("XButtonPress(%lu, %u+%u) @ %+d%+d",
	      xbev->subwindow, xbev->state, xbev->button, xbev->x, xbev->y);

	if (_xserver.grabbed.xwindow) {
		DEBUG("Already an active grab with %u+%u on X window %lu; ignoring",
		      _xserver.grabbed.button.mod,
		      _xserver.grabbed.button.button,
		      _xserver.grabbed.xwindow->id);
		return;
	}

	if (xbev->subwindow == 0)
		/* no subwindow == event happened on root window itself */
		return;

	xwin = _find_event_window(xbev->subwindow, "XButtonPressEvent");
	if (!xwin) {
		log_flush(LOG_CRITICAL);
		return;
	}

	/* mark window as grabbed until release */
	_xserver.grabbed.xwindow = xwin;
	_xserver.grabbed.button.mod = xbev->state;
	_xserver.grabbed.button.button = xbev->button;

	cbev.type = BUTTON_PRESS;
	cbev.button = _xserver.grabbed.button;
	cbev.position.x = xbev->x_root;
	cbev.position.y = xbev->y_root;
	cbev.xwindow = _xserver.grabbed.xwindow;
	bus_channel_send(bus.x_mouse_notify, &cbev);
}

static void
_handle_xevent_buttonrelease(XEvent *xev)
{
	XButtonEvent *xbev = &xev->xbutton;
	struct xevent_button cbev;

	DEBUG("XButtonRelease(%lu, %u+%u) @ %+d%+d",
	      xbev->subwindow, xbev->state, xbev->button, xbev->x, xbev->y);

	if (!_xserver.grabbed.xwindow)
		return;

	if (xbev->button != _xserver.grabbed.button.button) {
		DEBUG("Button %u+%u released while %u+%u grabbed; ignoring",
		      xbev->state, xbev->button,
		      _xserver.grabbed.button.mod,
		      _xserver.grabbed.button.button);
		return;
	}

	cbev.type = BUTTON_RELEASE;
	cbev.button = _xserver.grabbed.button;
	cbev.position.x = xbev->x_root;
	cbev.position.y = xbev->y_root;
	cbev.xwindow = _xserver.grabbed.xwindow;
	bus_channel_send(bus.x_mouse_notify, &cbev);

	/* button released, ungrab window */
	_xserver.grabbed.xwindow = NULL;
}

static void
_handle_xevent_clientmessage(XEvent *xev)
{
	XClientMessageEvent *xcmev = &xev->xclient;
	struct xwindow *xwin;

	// DEBUG("clientmessage(%lu)", xcmev->window);

	xwin = _find_event_window(xcmev->window, "XClientMessageEvent");
	if (!xwin) {
		log_flush(LOG_CRITICAL);
		return;
	}

	if (xwindow_handle_clientmessage(xwin, xcmev) < 0)
		ERROR("X window %lu could not handle client message: %s",
		      xwin->id, log_str());
}

static void
_handle_xevent_configurenotify(XEvent *xev)
{
	XConfigureEvent *xcev = &xev->xconfigure;
	struct xwindow *xwin;

	xwin = _find_event_window(xcev->window, "XConfigureEvent");
	if (!xwin) {
		log_flush(LOG_CRITICAL);
		return;
	}

	if (xwin == xserver.root) {
		/* Root window configured, the monitors arrangement changed. */
		if (xoutput_scan() < 0) {
			log_set("Could not scan X screens: %s", log_str());
			return;
		}
	} else if (!xwindow_is_manageable(xwin)) {
		/* Non-manageable window configured, update local data
		 * structure.
		 */
		xwin->dimension.x = xcev->x;
		xwin->dimension.y = xcev->y;
		xwin->dimension.width = (int unsigned) MAX(1, xcev->width);
		xwin->dimension.height = (int unsigned) MAX(1, xcev->height);
		xwin->border.width = (int unsigned) MAX(1, xcev->border_width);
	}
	/* No need to do anything for managed windows, as they have already been
	 * dealt with during the configure request.
	 */
}

static void
_handle_xevent_configurerequest(XEvent *xev)
{
	XConfigureRequestEvent *xcrev = &xev->xconfigurerequest;
	struct xwindow *xwin;
	struct _xrequest_handler *xrh = _xserver.request_handlers.configure;
	struct rectangle newdim;
	int unsigned bw;
	union xserver_request req;
	enum xserver_request_handler_result res = XSERVER_REQUEST_UNHANDLED;
	XConfigureEvent cev;
	bool resize = false, reborder = false;

	xwin = _find_event_window(xcrev->window, "XConfigureRequestEvent");
	if (!xwin) {
		log_flush(LOG_CRITICAL);
		return;
	}

	/* check request validity */
	if ((xcrev->value_mask & CWWidth) && xcrev->width <= 0) {
		ERROR("XConfigureRequestEvent(%lu) for width change with invalid width %d; rejecting",
		      xwin->id, xcrev->width);
		res = XSERVER_REQUEST_REJECTED;
	}
	if ((xcrev->value_mask & CWHeight) && xcrev->height <= 0) {
		ERROR("XConfigureRequestEvent(%lu) for width change with invalid height %d; rejecting",
		      xwin->id, xcrev->height);
		res = XSERVER_REQUEST_REJECTED;
	}

	/* extract configuration and run handler */
	if (res != XSERVER_REQUEST_REJECTED) {
		newdim.x = (xcrev->value_mask & CWX) ? xcrev->x : xwin->dimension.x;
		newdim.y = (xcrev->value_mask & CWY) ? xcrev->y : xwin->dimension.y;
		newdim.width = (xcrev->value_mask & CWWidth)
		             ? (int unsigned) xcrev->width : xwin->dimension.width;
		newdim.height = (xcrev->value_mask & CWHeight)
		              ? (int unsigned) xcrev->height : xwin->dimension.height;
		resize = (xcrev->value_mask & (CWX | CWY | CWWidth | CWHeight));
		reborder = xcrev->value_mask & CWBorderWidth;
		bw = reborder ? (int unsigned) xcrev->border_width : xwin->border.width;

		if (xrh) {
			req.configure.type = XSERVER_REQUEST_CONFIGURE;
			req.configure.xwindow = xwin;
			req.configure.dimension = newdim;
			if (xrh->handle(&req, &res, xrh->context) < 0)
				res = XSERVER_REQUEST_FAILED;
		}
	}

	/* post-handler actions */
	switch (res) {
	case XSERVER_REQUEST_UNHANDLED:
		WARN("X window %lu: No configure request handler installed; configuring directly",
		     xwin->id);
		break;
	case XSERVER_REQUEST_HANDLED:
		DEBUG("X window %lu: Configure request handled successfully",
		      xwin->id);
		resize = reborder = false;
		break;
	case XSERVER_REQUEST_REJECTED:
		DEBUG("X window %lu: Configure request rejected; notifying client about current dimensions",
		      xwin->id);
		break;
	case XSERVER_REQUEST_PASSTHROUGH:
		DEBUG("X window %lu: Configure request handler passed through; configuring directly",
		      xwin->id);
		break;
	case XSERVER_REQUEST_FAILED:
		ERROR("X window %lu: Configure request handler failed; configuring directly (%s)",
		      xwin->id, log_str());
		break;
	}

	/* clean up */
	if (res == XSERVER_REQUEST_REJECTED) {
		cev = (XConfigureEvent) {
			.type = ConfigureNotify,
			.display = xserver.display,
			.event = xwin->id,
			.window = xwin->id,
			.x = xwin->dimension.x,
			.y = xwin->dimension.y,
			.width = (int signed) xwin->dimension.width,
			.height = (int signed) xwin->dimension.height,
			.border_width = (int signed) xwin->border.width,
			.above = None,
			.override_redirect = False,
		};
		if (!XSendEvent(xserver.display, xwin->id, False,
		                StructureNotifyMask, (XEvent *) &cev))
			ERROR("X window %lu: Could not notify client about current dimensions (%ux%u%+d%+d): XSendEvent() failed",
			      xwin->id,
			      xwin->dimension.width, xwin->dimension.height,
			      xwin->dimension.x, xwin->dimension.y);
	} else {
		if (resize && xwindow_moveresize(xwin, newdim, true) < 0)
			ERROR("Could not move-resize X window %lu to dimension %ux%u%+d%+d: %s",
			      xwin->id, newdim.width, newdim.height,
			      newdim.x, newdim.y, log_str());
		if (reborder && xwindow_set_border_width(xwin, bw) < 0)
			ERROR("Could not change window border size for X window %lu to %u: %s",
			      xwin->id, bw, log_str());
	}
}

static void
_handle_xevent_createnotify(XEvent *xev)
{
	XCreateWindowEvent *xcwev = &xev->xcreatewindow;
	struct rectangle dim;
	struct xwindow *xwin;

	DEBUG("createnotify(%lu)", xcwev->window);

	xwin = list_find(xserver.windows,
	                 &xcwev->window, (list_eqfunc) xwindow_has_id);
	if (xwin) {
		if (xwin->class != XWINDOW_CLASS_OWNED)
			WARN("XCreateWindowEvent for already known X window %lu",
			     xwin->id);
		return;
	}

	dim = (struct rectangle) {
		.x = xcwev->x,
		.y = xcwev->y,
		.width = (int unsigned) xcwev->width,
		.height = (int unsigned) xcwev->height,
	};
	xwin = xwindow_alloc(xcwev->window, &dim, 0x0LU,
	                     (bool) xcwev->override_redirect,
	                     XWINDOW_CLASS_CLIENT);
	if (!xwin) {
		CRITICAL("Could not create X window for tracking %lu: %s",
		         xcwev->window, log_str());
		return;
	}

	list_append(xserver.windows, xwin);

	return;
}

static void
_handle_xevent_destroynotify(XEvent *xev)
{
	XDestroyWindowEvent *xdwev = &xev->xdestroywindow;
	struct xwindow *xwin;

	DEBUG("destroynotify(%lu)", xdwev->window);

	xwin = list_find(xserver.windows,
	                 &xdwev->window, (list_eqfunc) xwindow_has_id);
	if (!xwin) {
		WARN("X destroy notify on unknown X window %lu", xdwev->window);
		return;
	}
	xwin->destroyed = true;

	bus_channel_send(bus.x_window_notify_destroy, xwin);

	list_remove(xserver.windows, xwin, NULL);
	xwindow_free(xwin);
}

static void
_handle_xevent_enternotify(XEvent *xev)
{
	XCrossingEvent *xcev = &xev->xcrossing;
	struct xwindow *xwin;
	struct xevent_pointer xpev;

	DEBUG("enternotify(%lu)", xcev->window);

	xwin = _find_event_window(xcev->window, "XEnterNotifyEvent");
	if (!xwin) {
		log_flush(LOG_CRITICAL);
		return;
	}

	xpev.type = POINTER_ENTER;
	xpev.pointer.position.x = xcev->x_root;
	xpev.pointer.position.y = xcev->y_root;
	xpev.xwindow = xwin;

	bus_channel_send(bus.x_pointer_notify, &xpev);
}

static void
_handle_xevent_expose(XEvent *xev)
{
	XExposeEvent *xxpev = &xev->xexpose;
	struct xwindow *xwin;

	xwin = _find_event_window(xxpev->window, "XExposeEvent");
	if (!xwin) {
		log_flush(LOG_CRITICAL);
		return;
	}

	bus_channel_send(bus.x_window_notify_expose, xwin);
}

static void
_handle_xevent_focusin(XEvent *xev)
{
	XFocusChangeEvent *xfev = &xev->xfocus;
	struct xwindow *xwin;

	xwin = _find_event_window(xfev->window, "XFocusChangeEvent");
	if (!xwin) {
		log_flush(LOG_CRITICAL);
		return;
	}

	if (xwin == xserver.root)
		return;

	bus_channel_send(bus.x_window_notify_focus, xwin);
}

static void
_handle_xevent_maprequest(XEvent *xev)
{
	XMapRequestEvent *xmrev = &xev->xmaprequest;
	struct _xrequest_handler *xrh = _xserver.request_handlers.map;
	struct xwindow *xwin;
	union xserver_request req;
	enum xserver_request_handler_result res = XSERVER_REQUEST_UNHANDLED;
	bool map_directly = false;

	DEBUG("maprequest(%lu)", xmrev->window);

	xwin = _find_event_window(xmrev->window, "XMapRequestEvent");
	if (!xwin) {
		log_flush(LOG_CRITICAL);
		return;
	}

	if (xwindow_query_type(xwin) < 0)
		CRITICAL("X window %lu: Could not query type; window may be handled incorrectly (%s)",
		         xwin->id, log_str());
	if (xwindow_query_state(xwin) < 0)
		CRITICAL("X window %lu: Could not query state; window may be handled incorrectly (%s)",
		         xwin->id, log_str());

	/* run handler */
	if (xrh) {
		req.map.type = XSERVER_REQUEST_MAP;
		req.map.xwindow = xwin;
		if (xrh->handle(&req, &res, xrh->context) < 0)
			res = XSERVER_REQUEST_FAILED;
	}

	/* handle handler result */
	switch (res) {
	case XSERVER_REQUEST_UNHANDLED:
		WARN("X window %lu: No map request handler installed; mapping directly",
		     xwin->id);
		map_directly = true;
		break;
	case XSERVER_REQUEST_HANDLED:
		DEBUG("X window %lu: Map request handled successfully",
		      xwin->id);
		break;
	case XSERVER_REQUEST_REJECTED:
		DEBUG("X window %lu: Map request rejected; not mapping",
		      xwin->id);
		break;
	case XSERVER_REQUEST_PASSTHROUGH:
		DEBUG("X window %lu: Map request passed through; mapping directly",
		      xwin->id);
		map_directly = true;
		break;
	case XSERVER_REQUEST_FAILED:
		ERROR("X window %lu: Map request handler failed; mapping directly (%s)",
		      xwin->id, log_str());
		map_directly = true;
		break;
	}

	/* clean up */
	if (map_directly)
		if (xwindow_map(xwin) < 0)
			ERROR("Could not map X window %lu: %s",
			      xwin->id, log_str());
}

static void
_handle_xevent_motionnotify(XEvent *xev)
{
	XMotionEvent *xmev = &xev->xmotion;
	struct xevent_button cbev;

	//DEBUG("XMotion(%lu) %+d%+d", xmev->subwindow, xmev->x, xmev->y);

	if (!_xserver.grabbed.xwindow)
		return;

	cbev.type = BUTTON_MOVE;
	cbev.button = _xserver.grabbed.button;
	cbev.position.x = xmev->x_root;
	cbev.position.y = xmev->y_root;
	cbev.xwindow = _xserver.grabbed.xwindow;
	bus_channel_send(bus.x_mouse_notify, &cbev);
}

static void
_handle_xevent_propertynotify(XEvent *xev)
{
	XPropertyEvent *xpev = &xev->xproperty;
	struct xwindow *xwin;

	//DEBUG("XPropertyEvent(%lu)", xpev->window);

	if (xpev->window == xserver.root->id) {
		/* TODO */
		DEBUG("X property notify for root window; ignoring");
		return;
	}

	xwin = _find_event_window(xpev->window, "XPropertyEvent");
	if (!xwin) {
		log_flush(LOG_CRITICAL);
		return;
	}

	if (xwindow_handle_propertynotify(xwin, xpev) < 0)
		ERROR("Could not handle XPropertyEvent for X window %lu: %s",
		      xwin->id, log_str());
}

static void
_handle_xevent_unmapnotify(XEvent *xev)
{
	XUnmapEvent *xuev = &xev->xunmap;
	XWindowAttributes xwa;
	struct xwindow *xwin;

	/* Check if the window is indeed unmapped now.
	 * We do so because some client-level actions (e.g. send+follow) result
	 * in unmap+remap events, and by the time we receive the unmap event,
	 * the client thinks it is supposed to be visible, and thus erroneously
	 * unmanages the window.
	 * Once we start reparenting, this issue should hopefully be resolved
	 * (see also the comment in client.c).
	 * Note that XGetWindowAttributes(3) will fail if the window is gone, so
	 * there is no need to print a warning here.
	 */
	if (XGetWindowAttributes(xserver.display, xuev->window, &xwa)
	&& xwa.map_state != IsUnmapped)
		return;

	xwin = _find_event_window(xuev->window, "XUnmapEvent");
	if (!xwin) {
		log_flush(LOG_CRITICAL);
		return;
	}

	bus_channel_send(bus.x_window_notify_unmap, xwin);
}

static int
_init_atoms(void)
{
	int unsigned i;
	struct {
		Atom *atom;
		char const *name;
	} map[] = {
		/* ICCCM Client Properties (section 4.1.2) */
		{ &xserver.atoms[XATOM_WM_NAME], "WM_NAME" }, /* defined in X11/Xlib.h */
		{ &xserver.atoms[XATOM_WM_ICON_NAME], "WM_ICON_NAME" }, /* defined in X11/Xlib.h */
		{ &xserver.atoms[XATOM_WM_NORMAL_HINTS], "WM_NORMAL_HINTS" }, /* defined in X11/Xlib.h */
		{ &xserver.atoms[XATOM_WM_HINTS], "WM_HINTS" }, /* defined in X11/Xlib.h */
		{ &xserver.atoms[XATOM_WM_CLASS], "WM_CLASS" }, /* defined in X11/Xlib.h */
		{ &xserver.atoms[XATOM_WM_TRANSIENT_FOR], "WM_TRANSIENT_FOR" }, /* defined in X11/Xlib.h */
		{ &xserver.atoms[XATOM_WM_PROTOCOLS], "WM_PROTOCOLS" },
		{ &xserver.atoms[XATOM_WM_TAKE_FOCUS], "WM_TAKE_FOCUS" },
		{ &xserver.atoms[XATOM_WM_DELETE_WINDOW], "WM_DELETE_WINDOW" },
		{ &xserver.atoms[XATOM_WM_CLIENT_MACHINE], "WM_CLIENT_MACHINE" }, /* defined in X11/Xlib.h */

		/* ICCCM Window Manager Properties (section 4.1.3) */
		{ &xserver.atoms[XATOM_WM_STATE], "WM_STATE" },
		{ &xserver.atoms[XATOM_WM_ICON_SIZE], "WM_ICON_SIZE" }, /* defined in X11/Xlib.h */

		/* ICCCM Obsolete Properties (Appendix C) */
		{ &xserver.atoms[XATOM_WM_COMMAND], "WM_COMMAND" }, /* defined in X11/Xlib.h */

		/* Other Properties */
		{ &xserver.atoms[XATOM_WM_LOCALE_NAME], "WM_LOCALE_NAME" },

		/* EWMH Root Window Properties (1.3) */
		{ &xserver.atoms[XATOM_NET_ACTIVE_WINDOW], "_NET_ACTIVE_WINDOW" },

		/* EWMH Other Root Window Messages (1.4) */
		{ &xserver.atoms[XATOM_NET_REQUEST_FRAME_EXTENTS], "_NET_REQUEST_FRAME_EXTENTS" },

		/* EWMH Application Window Properties (1.5) */
		{ &xserver.atoms[XATOM_NET_WM_BYPASS_COMPOSITOR], "_NET_WM_BYPASS_COMPOSITOR" },
		{ &xserver.atoms[XATOM_NET_WM_ICON_NAME], "_NET_WM_ICON_NAME" },
		{ &xserver.atoms[XATOM_NET_WM_NAME], "_NET_WM_NAME" },
		{ &xserver.atoms[XATOM_NET_WM_OPAQUE_REGION], "_NET_WM_OPAQUE_REGION" },
		{ &xserver.atoms[XATOM_NET_WM_STATE], "_NET_WM_STATE" },
		{ &xserver.atoms[XATOM_NET_WM_STATE_ABOVE], "_NET_WM_STATE_ABOVE" },
		{ &xserver.atoms[XATOM_NET_WM_STATE_BELOW], "_NET_WM_STATE_BELOW" },
		{ &xserver.atoms[XATOM_NET_WM_STATE_DEMANDS_ATTENTION], "_NET_WM_STATE_DEMANDS_ATTENTION" },
		{ &xserver.atoms[XATOM_NET_WM_STATE_FULLSCREEN], "_NET_WM_STATE_FULLSCREEN" },
		{ &xserver.atoms[XATOM_NET_WM_STATE_HIDDEN], "_NET_WM_STATE_HIDDEN" },
		{ &xserver.atoms[XATOM_NET_WM_STATE_MAXIMIZED_HORZ], "_NET_WM_STATE_MAXIMIZED_HORZ" },
		{ &xserver.atoms[XATOM_NET_WM_STATE_MAXIMIZED_VERT], "_NET_WM_STATE_MAXIMIZED_VERT" },
		{ &xserver.atoms[XATOM_NET_WM_STATE_MODAL], "_NET_WM_STATE_MODAL" },
		{ &xserver.atoms[XATOM_NET_WM_STATE_SHADED], "_NET_WM_STATE_SHADED" },
		{ &xserver.atoms[XATOM_NET_WM_STATE_SKIP_PAGER], "_NET_WM_STATE_SKIP_PAGER" },
		{ &xserver.atoms[XATOM_NET_WM_STATE_SKIP_TASKBAR], "_NET_WM_STATE_SKIP_TASKBAR" },
		{ &xserver.atoms[XATOM_NET_WM_STATE_STICKY], "_NET_WM_STATE_STICKY" },
		{ &xserver.atoms[XATOM_NET_WM_STRUT_PARTIAL], "_NET_WM_STRUT_PARTIAL" },
		{ &xserver.atoms[XATOM_NET_WM_USER_TIME], "_NET_WM_USER_TIME" },
		{ &xserver.atoms[XATOM_NET_WM_WINDOW_TYPE], "_NET_WM_WINDOW_TYPE" },
		{ &xserver.atoms[XATOM_NET_WM_WINDOW_TYPE_DESKTOP], "_NET_WM_WINDOW_TYPE_DESKTOP" },
		{ &xserver.atoms[XATOM_NET_WM_WINDOW_TYPE_DIALOG], "_NET_WM_WINDOW_TYPE_DIALOG" },
		{ &xserver.atoms[XATOM_NET_WM_WINDOW_TYPE_DOCK], "_NET_WM_WINDOW_TYPE_DOCK" },
		{ &xserver.atoms[XATOM_NET_WM_WINDOW_TYPE_MENU], "_NET_WM_WINDOW_TYPE_MENU" },
		{ &xserver.atoms[XATOM_NET_WM_WINDOW_TYPE_NORMAL], "_NET_WM_WINDOW_TYPE_NORMAL" },
		{ &xserver.atoms[XATOM_NET_WM_WINDOW_TYPE_SPLASH], "_NET_WM_WINDOW_TYPE_SPLASH" },
		{ &xserver.atoms[XATOM_NET_WM_WINDOW_TYPE_TOOLBAR], "_NET_WM_WINDOW_TYPE_TOOLBAR" },
		{ &xserver.atoms[XATOM_NET_WM_WINDOW_TYPE_UTILITY], "_NET_WM_WINDOW_TYPE_UTILITY" },
	};

	for (i = 0; i < LENGTH(map); ++i) {
		*map[i].atom = XInternAtom(xserver.display, map[i].name, False);
		if (*map[i].atom == None) {
			log_set("Could not get atom '%s'", map[i].name);
			goto error_atom;
		}
		DEBUG("%s = %lu", map[i].name, *map[i].atom);
	}

	return 0;

 error_atom:
	return -1;
}

static int
_init_rootwin(void)
{
	Window rootwin;
	struct rectangle dim;
	int signed xdw, xdh;
	int long unsigned event_mask = XWINDOW_EVENT_MASK_BUTTON_PRESS
	                             | XWINDOW_EVENT_MASK_BUTTON_MOTION
	                             | XWINDOW_EVENT_MASK_FOCUS_CHANGE
	                             | XWINDOW_EVENT_MASK_KEY_PRESS
	                             | XWINDOW_EVENT_MASK_PROPERTY_CHANGE
	                             | XWINDOW_EVENT_MASK_STRUCTURE_NOTIFY
	                             | XWINDOW_EVENT_MASK_SUBSTRUCTURE_NOTIFY
	                             | XWINDOW_EVENT_MASK_SUBSTRUCTURE_REDIRECT
	                             | XWINDOW_EVENT_MASK_WINDOW_ENTER;

	/* get root window info */
	rootwin = RootWindow(xserver.display, xoutput.screen);
	if (!rootwin) {
		log_set("RootWindow() failed");
		goto error_rootwin;
	}
	dim.x = 0;
	dim.y = 0;
	xdw = DisplayWidth(xserver.display, xoutput.screen);
	if (xdw < 0) {
		log_set("DisplayWidth() returned negative value %d", xdw);
		goto error_negative_xdisplay;
	}
	xdh = DisplayHeight(xserver.display, xoutput.screen);
	if (xdh < 0) {
		log_set("DisplayHeight() returned negative value %d", xdh);
		goto error_negative_xdisplay;
	}

	/* manage root window */
	dim = (struct rectangle) {
		.x = 0, .y = 0,
		.width = (int unsigned) xdw,
		.height = (int unsigned) xdh,
	};
	xserver.root = xwindow_alloc(rootwin, &dim, event_mask, false,
	                             XWINDOW_CLASS_ROOT);
	if (!xserver.root) {
		log_set("Could not create xwindow struct for root window %lu: %s",
		        rootwin, log_str());
		goto error_xwin;
	}

	return 0;

 error_xwin:
 error_negative_xdisplay:
 error_rootwin:
	return -1;
}

static char const *
_request_type_str(enum xserver_request_type reqtype)
{
	switch (reqtype) {
	case XSERVER_REQUEST_CONFIGURE: return "XSERVER_REQUEST_CONFIGURE";
	case XSERVER_REQUEST_MAP: return "XSERVER_REQUEST_MAP";
	}

	FATAL_BUG("xserver::_request_str() called with unexpected enum xserver_request %d",
	          reqtype);
}

static int
_scan_server(void)
{
	unsigned int i, nwins;
	Window p, r, *wins = NULL;
	struct xwindow *xwin;
	bool or;

	if (!XQueryTree(xserver.display, xserver.root->id, &r, &p,
	                &wins, &nwins))
	{
		log_set("XQueryTree() failed");
		return -1;
	}

	DEBUG("XQueryTree() returned %u pre-existing X windows", nwins);
	for (i = 0; i < nwins; ++i) {
		xwin = list_find(xserver.windows,
		                 &wins[i], (list_eqfunc) xwindow_has_id);
		if (xwin) {
			WARN("X server: scanned X window %lu twice", xwin->id);
			continue;
		}
		if (_xwindow_has_override_redirect(wins[i], &or) < 0) {
			WARN("X server: Could not determine override_redirect for X window %lu; assuming yes (%s)",
			     wins[i], log_str());
			or = true;
		}
		xwin = xwindow_alloc(wins[i], NULL, 0x0LU, or,
		                     XWINDOW_CLASS_CLIENT);
		if (!xwin) {
			ERROR("Could not create X window struct for %lu: %s",
			      wins[i], log_str());
			continue;
		}
		list_append(xserver.windows, xwin);
	}

	XFree(wins);
	return 0;
}

static int
_xwindow_has_override_redirect(Window id, bool *override_redirect)
{
	XWindowAttributes xwa;

	if (!XGetWindowAttributes(xserver.display, id, &xwa)) {
		log_set("Could not get X window attributes");
		return -1;
	}

	*override_redirect = (bool) xwa.override_redirect;
	return 0;
}
