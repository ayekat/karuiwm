#include <karuiwm/size.h>

bool
size_equals(struct size const *s1, struct size const *s2)
{
	return s1->width == s2->width && s1->height == s2->height;
}
