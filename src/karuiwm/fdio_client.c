#include <karuiwm/fdio_client.h>
#include <karui/cstr.h>
#include <karui/list.h>
#include <karui/log.h>
#include <karui/memory.h>

struct fdio_client *
fdio_client_alloc(char const *name, int (*callback)(int, void *), void *data)
{
	struct fdio_client *c;

	c = memory_alloc(sizeof(struct fdio_client), "FDIO client struct");
	if (!c)
		goto error_alloc;

	c->name = cstr_dup(name);
	if (!c->name) {
		log_set("Could not duplicate name: %s", log_str());
		goto error_name;
	}
	c->callback = callback;
	c->fds = list_alloc();
	if (!c->fds) {
		log_set("Could not create file descriptor list: %s", log_str());
		goto error_fds;
	}
	c->data = data;

	return c;

 error_fds:
	log_push();
	cstr_free(c->name);
	log_pop();
 error_name:
	log_push();
	memory_free(c);
	log_pop();
 error_alloc:
	return NULL;
}

void
fdio_client_free(struct fdio_client *c)
{
	/* FDs managed by client themselves, the list just holds references */
	list_clear(c->fds, NULL);
	list_free(c->fds);

	cstr_free(c->name);

	memory_free(c);
}

int
fdio_client_add_fd(struct fdio_client *c, int *fd)
{
	list_append(c->fds, fd);
	return 0;
}

bool
fdio_client_has_fd(struct fdio_client const *c, int const *fd)
{
	return list_contains(c->fds, fd, NULL);
}

int
fdio_client_notify(struct fdio_client const *c, int const *fd)
{
	return c->callback(*fd, c->data);
}

void
fdio_client_remove_fd(struct fdio_client *c, int *fd)
{
	list_remove(c->fds, fd, NULL);
}
