#ifndef _KWM_KEY_H
#define _KWM_KEY_H

#include <X11/Xlib.h>

struct key {
	int unsigned mod;
	KeySym sym;
};

#include <stdbool.h>

bool key_equals(struct key const *k1, struct key const *k2);
int key_parse(struct key *k, char const *str);
int key_parse_modifier(int unsigned *mod, char const *str);
int key_parse_symbol(KeySym *sym, char const *str);

#endif /* ndef _KWM_KEY_H */
