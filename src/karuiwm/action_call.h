#ifndef _KWM_ACTION_CALL_H
#define _KWM_ACTION_CALL_H

struct action_call {
	char *name;
	struct action const *action;
	struct list *args;
	struct buttonbind *buttonbind;
};

#include <karuiwm/button_event.h>
#include <stddef.h>

struct action_call *action_call_alloc(struct action const *action,
                                      struct list const *args);
void action_call_free(struct action_call *ac);

struct action_call *action_call_copy(struct action_call const *ac);
int action_call_invoke(struct action_call const *ac,
                       struct button_event const *bev,
                       struct list **results);
struct action_call *action_call_parse(char const *str);
void action_call_unlink(struct action_call *ac);

#endif /* ndef _KWM_ACTION_CALL_H */
