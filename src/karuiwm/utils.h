#ifndef _KWM_UTILS_H
#define _KWM_UTILS_H

#define LENGTH(ARR) (sizeof(ARR)/sizeof(ARR[0]))
#define MAX(X, Y) ((X) < (Y) ? (Y) : (X))
#define MIN(X, Y) ((X) < (Y) ? (X) : (Y))

#endif /* ndef _KWM_UTILS_H */
