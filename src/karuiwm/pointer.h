#ifndef _KWM_POINTER_H
#define _KWM_POINTER_H

#include <karuiwm/position.h>

struct pointer {
	struct position position;
};

#endif /* ndef _KWM_POINTER_H */
