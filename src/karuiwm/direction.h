#ifndef _KWM_DIRECTION_H
#define _KWM_DIRECTION_H

enum direction { DIR_NONE, DIR_LEFT, DIR_RIGHT, DIR_UP, DIR_DOWN };

#include <karuiwm/position.h>

enum direction direction_opposite(enum direction dir);
void direction_relative(struct position start, struct position *end,
                        enum direction dir);
int direction_parse(enum direction *dir, char const *str);
char const *direction_str(enum direction dir);

#endif /* ndef _KWM_DIRECTION_H */
