#include <karuiwm/useraction.h>
#include <karuiwm/action.h>
#include <karuiwm/action_call.h>
#include <karuiwm/buttonbind.h>
#include <karuiwm/button_event.h>
#include <karuiwm/buttonmap.h>
#include <karuiwm/client.h>
#include <karuiwm/cursor.h>
#include <karuiwm/input.h>
#include <karuiwm/karuiwm.h>
#include <karuiwm/keybind.h>
#include <karuiwm/keymap.h>
#include <karuiwm/layout.h>
#include <karuiwm/registry.h>
#include <karuiwm/seat.h>
#include <karuiwm/system.h>
#include <karuiwm/utils.h>
#include <karuiwm/workspace.h>
#include <karuiwm/value.h>
#include <karuiwm/xwindow.h>
#include <karui/cstr.h>
#include <karui/log.h>
#include <assert.h>

enum _resize_type {
	RESIZE_BOTTOM,
	RESIZE_BOTTOM_LEFT,
	RESIZE_BOTTOM_RIGHT,
	RESIZE_LEFT,
	RESIZE_NONE, /* if attempting to resize by clicking in centre area */
	RESIZE_RIGHT,
	RESIZE_TOP,
	RESIZE_TOP_LEFT,
	RESIZE_TOP_RIGHT,
};

struct _useraction {
	char const *name;
	int (*func)(struct list **, struct list const *);
	size_t nargs;
	enum value_type const *arg_types;
};

static int _action_button_bind(struct list **results, struct list const *args);
static int _action_button_unbind(struct list **results, struct list const *args);
static int _action_buttonmap_create(struct list **results, struct list const *args);
static int _action_buttonmap_destroy(struct list **results, struct list const *args);
static int _action_exec(struct list **results, struct list const *args);
static int _action_focus_set_workspace(struct list **results, struct list const *args);
static int _action_focus_step_monitor(struct list **results, struct list const *args);
static int _action_focus_step_window(struct list **results, struct list const *args);
static int _action_key_bind(struct list **results, struct list const *args);
static int _action_key_unbind(struct list **results, struct list const *args);
static int _action_keymap_create(struct list **results, struct list const *args);
static int _action_keymap_destroy(struct list **results, struct list const *args);
static int _action_mouse_move_window(struct list **results, struct list const *args);
static int _action_mouse_resize_window(struct list **results, struct list const *args);
static int _action_quit(struct list **results, struct list const *args);
static int _action_restart(struct list **results, struct list const *args);
static int _action_scratchpad_set_margin(struct list **results, struct list const *args);
static int _action_scratchpad_toggle(struct list **results, struct list const *args);
static int _action_scratchpad_toggle_window(struct list **results, struct list const *args);
static int _action_state_list_workspaces(struct list **results, struct list const *args);
static int _action_window_close(struct list **results, struct list const *args);
static int _action_window_send(struct list **results, struct list const *args);
static int _action_window_set_border_colour_focused(struct list **results, struct list const *args);
static int _action_window_set_border_colour_unfocused(struct list **results, struct list const *args);
static int _action_window_set_border_width(struct list **results, struct list const *args);
static int _action_window_shift(struct list **results, struct list const *args);
static int _action_window_toggle_float(struct list **results, struct list const *args);
static int _action_window_zoom(struct list **results, struct list const *args);
static int _action_workspace_rename(struct list **results, struct list const *args);
static int _action_workspace_set_mfact(struct list **results, struct list const *args);
static int _action_workspace_set_nmaster(struct list **results, struct list const *args);
static int _action_workspace_step_layout(struct list **results, struct list const *args);
static struct workspace *_get_or_create_workspace(char const *name);
static int _step_window(int posdiff, bool sticky);

static struct registry_session *_regses;

#define _(...) (enum value_type const[]) { __VA_ARGS__ }
static struct _useraction const _useractions[] = {
	{ "button_bind",              _action_button_bind,
	  3, _(VALUE_STRING, VALUE_BUTTON, VALUE_ACTION_CALL) },
	{ "button_unbind",            _action_button_unbind,
	  2, _(VALUE_STRING, VALUE_BUTTON) },
	{ "buttonmap_create",         _action_buttonmap_create,
	  1, _(VALUE_STRING) },
	{ "buttonmap_destroy",        _action_buttonmap_destroy,
	  1, _(VALUE_STRING) },

	{ "exec",                     _action_exec,
	  1, _(VALUE_STRING) },

	{ "focus_set_workspace",      _action_focus_set_workspace,
	  1, _(VALUE_STRING) },
	{ "focus_step_monitor",       _action_focus_step_monitor,
	  1, _(VALUE_INTEGER) },
	{ "focus_step_window",        _action_focus_step_window,
	  1, _(VALUE_INTEGER) },

	{ "key_bind",                 _action_key_bind,
	  3, _(VALUE_STRING, VALUE_KEY,
	       VALUE_ACTION_CALL) },
	{ "key_unbind",               _action_key_unbind,
	  2, _(VALUE_STRING, VALUE_KEY) },
	{ "keymap_create",            _action_keymap_create,
	  2, _(VALUE_STRING, VALUE_BOOLEAN) },
	{ "keymap_destroy",           _action_keymap_destroy,
	  1, _(VALUE_STRING) },

	{ "mouse_move_window",        _action_mouse_move_window,
	  1, _(VALUE_BUTTON_EVENT) },
	{ "mouse_resize_window",      _action_mouse_resize_window,
	  1, _(VALUE_BUTTON_EVENT) },

	{ "quit",                     _action_quit,
	  0, NULL },
	{ "restart",                  _action_restart,
	  0, NULL },

	{ "scratchpad_set_margin",    _action_scratchpad_set_margin,
	  1, _(VALUE_UNSIGNED_INTEGER) },
	{ "scratchpad_toggle",        _action_scratchpad_toggle,
	  0, NULL },
	{ "scratchpad_toggle_window", _action_scratchpad_toggle_window,
	  0, NULL },

	{ "state_list_workspaces",    _action_state_list_workspaces,
	  0, NULL },

	{ "window_close",             _action_window_close,
	  0, NULL },
	{ "window_send",              _action_window_send,
	  2, _(VALUE_STRING, VALUE_BOOLEAN) },
	{ "window_set_border_colour_focused",
	                              _action_window_set_border_colour_focused,
	  1, _(VALUE_COLOUR) },
	{ "window_set_border_colour_unfocused",
	                              _action_window_set_border_colour_unfocused,
	  1, _(VALUE_COLOUR) },
	{ "window_set_border_width",  _action_window_set_border_width,
	  1, _(VALUE_UNSIGNED_INTEGER) },
	{ "window_shift",             _action_window_shift,
	  1, _(VALUE_INTEGER) },
	{ "window_toggle_float",      _action_window_toggle_float,
	  0, NULL },
	{ "window_zoom",              _action_window_zoom,
	  0, NULL },

	{ "workspace_rename",         _action_workspace_rename,
	  1, _(VALUE_STRING) },
	{ "workspace_set_mfact",      _action_workspace_set_mfact,
	  1, _(VALUE_FLOAT) },
	{ "workspace_set_nmaster",    _action_workspace_set_nmaster,
	  1, _(VALUE_INTEGER) },
	{ "workspace_step_layout",    _action_workspace_step_layout,
	  1, _(VALUE_INTEGER) },
};
#undef _

int
useraction_init(void)
{
	int unsigned i;
	char const *name;
	int (*func)(struct list **, struct list const *);
	size_t nargs;
	enum value_type const *arg_types;

	_regses = registry_connect("useraction");
	if (!_regses) {
		log_set("Could not open registry: %s", log_str());
		goto error_registry;
	}

	for (i = 0; i < LENGTH(_useractions); ++i) {
		name = _useractions[i].name;
		func = _useractions[i].func;
		nargs = _useractions[i].nargs;
		arg_types = _useractions[i].arg_types;

		if (registry_create_action(_regses,
		                           name, func, nargs, arg_types) < 0)
		{
			log_set("Could not register action '%s': %s",
			        name, log_str());
			goto error_register_action;
		}
	}

	return 0;

 error_register_action:
	log_push();
	registry_disconnect(_regses);
	log_pop();
	_regses = NULL;
 error_registry:
	return -1;
}

void
useraction_deinit(void)
{
	registry_disconnect(_regses);
	_regses = NULL;
}

static int
_action_button_bind(struct list **results, struct list const *args)
{
	char const *bmname;
	struct button const *b;
	struct action_call const *aac;
	struct action_call *ac;
	struct buttonmap *bm;
	struct buttonbind *bb;
	(void) results;

	bmname = ((struct value *) args->elements[0])->data.str;
	b = &((struct value *) args->elements[1])->data.button;
	aac = ((struct value *) args->elements[2])->data.ac;

	bm = input_find_buttonmap_by_name(bmname);
	if (!bm) {
		log_set("Buttonmap '%s' not found", bmname);
		return -1;
	}

	ac = action_call_copy(aac);
	if (!ac) {
		log_set("Could not copy action call '%s': %s",
		        aac->name, log_str());
		goto error_actioncall_copy;
	}

	bb = buttonbind_alloc(b, ac);
	if (!bb) {
		log_set("Could not create buttonbind: %s", log_str());
		/* XXX buttonbind would have owned action_call */
		log_push();
		action_call_free(ac);
		log_pop();
		goto error_buttonbind;
	}

	if (buttonmap_add_buttonbind(bm, bb, true) < 0) {
		log_set("Could not add buttonbind to buttonmap: %s", log_str());
		goto error_buttonmap_add;
	}

	return 0;

 error_buttonmap_add:
	log_push();
	buttonbind_free(bb);
	log_pop();
 error_buttonbind:
 error_actioncall_copy:
	return -1;
}

static int
_action_button_unbind(struct list **results, struct list const *args)
{
	struct button const *b;
	char const *bmname;
	(void) results;

	bmname = ((struct value *) args->elements[0])->data.str;
	b = &((struct value *) args->elements[1])->data.button;

	if (input_unbind_button(bmname, b) < 0) {
		log_set("Could not unbind button %u+%u in buttonmap '%s': %s",
		        b->mod, b->button, bmname, log_str());
		return -1;
	}
	return 0;
}

static int
_action_buttonmap_create(struct list **results, struct list const *args)
{
	char const *bmname;
	(void) results;

	bmname = ((struct value *) args->elements[0])->data.str;

	if (input_create_buttonmap(bmname) < 0) {
		log_set("Could not create buttonmap '%s': %s",
		        bmname, log_str());
		return -1;
	}
	return 0;
}

static int
_action_buttonmap_destroy(struct list **results, struct list const *args)
{
	char const *bmname;
	(void) results;

	bmname = ((struct value *) args->elements[0])->data.str;

	if (input_destroy_buttonmap(bmname) < 0) {
		log_set("Could not destroy buttonmap '%s': %s",
		        bmname, log_str());
		return -1;
	}
	return 0;
}

static int
_action_exec(struct list **results, struct list const *args)
{
	char const *cmdstr;
	struct list *cmdline;
	(void) results;

	cmdstr = ((struct value *) args->elements[0])->data.str;

	cmdline = cstr_split_smart(cmdstr);
	if (!cmdline) {
		log_set("Could not split string '%s': %s", cmdstr, log_str());
		goto error_cmdline;
	}

	if (system_fork_exec(cmdline) < 0) {
		log_set("Could not fork and exec command: %s", log_str());
		goto error_fork_exec;
	}

	list_clear(cmdline, (list_delfunc) cstr_free);
	list_free(cmdline);
	return 0;

 error_fork_exec:
	log_push();
	list_clear(cmdline, (list_delfunc) cstr_free);
	list_free(cmdline);
	log_pop();
 error_cmdline:
	return -1;
}

static int
_action_focus_set_workspace(struct list **results, struct list const *args)
{
	char const *wsname;
	struct workspace *ws;
	(void) results;

	wsname = ((struct value *) args->elements[0])->data.str;

	ws = _get_or_create_workspace(wsname);
	if (!ws) {
		log_set("Could not get workspace '%s': %s", wsname, log_str());
		goto error_get;
	}

	monitor_show_workspace(seat.selmon, ws);

	return 0;

 error_get:
	return -1;
}

static int
_action_focus_step_monitor(struct list **results, struct list const *args)
{
	struct monitor *srcmon, *dstmon;
	unsigned int srcpos, dstpos;
	int posdiff;
	(void) results;

	posdiff = ((struct value *) args->elements[0])->data.i;

	if (seat.monitors->size == 0) {
		log_set("No monitors to focus");
		return 1;
	}

	/* find index of source monitor */
	assert(seat.selmon);
	srcmon = seat.selmon;
	if (srcmon) {
		srcpos = list_find_index(seat.monitors, srcmon, NULL);
		assert(srcpos < seat.monitors->size);

		/* find destination monitor */
		dstpos = (int unsigned) ((int signed) (seat.monitors->size
		                                       + srcpos)
		                         + posdiff)
		         % (int unsigned) seat.monitors->size;
		DEBUG("Stepping monitors %u -> %u", srcpos, dstpos);
	} else {
		BUG("Seat does not focus any monitor");
		dstpos = 0;
	}
	dstmon = seat.monitors->elements[dstpos];

	/* focus destination monitor */
	seat_focus_monitor(dstmon);

	return 0;
}

static int
_action_focus_step_window(struct list **results, struct list const *args)
{
	int posdiff = ((struct value *) args->elements[0])->data.i;
	bool sticky = false;
	(void) results;

	return _step_window(posdiff, sticky);
}

static int
_action_key_bind(struct list **results, struct list const *args)
{
	char const *kmname;
	struct key const *k;
	struct action_call const *aac;
	struct action_call *ac;
	struct keymap *km;
	struct keybind *kb;
	(void) results;

	kmname = ((struct value *) args->elements[0])->data.str;
	k = &((struct value *) args->elements[1])->data.key;
	aac = ((struct value *) args->elements[2])->data.ac;

	km = input_find_keymap_by_name(kmname);
	if (!km) {
		log_set("Keymap '%s' not found", kmname);
		return -1;
	}

	ac = action_call_copy(aac);
	if (!ac) {
		log_set("Could not copy action call '%s': %s",
		        aac->name, log_str());
		goto error_actioncall_copy;
	}

	kb = keybind_alloc(k, ac);
	if (!kb) {
		log_set("Could not create keybind: %s", log_str());
		/* XXX keybind would have owned action_call */
		log_push();
		action_call_free(ac);
		log_pop();
		goto error_keybind;
	}

	if (keymap_add_keybind(km, kb, true) < 0) {
		log_set("Could not add keybind to keymap '%s': %s",
		        km->name, log_str());
		goto error_keymap_add;
	}

	return 0;

 error_keymap_add:
	log_push();
	keybind_free(kb);
	log_pop();
 error_keybind:
 error_actioncall_copy:
	return -1;
}

static int
_action_key_unbind(struct list **results, struct list const *args)
{
	struct key const *k;
	char const *kmname;
	(void) results;

	kmname = ((struct value *) args->elements[0])->data.str;
	k = &((struct value *) args->elements[1])->data.key;

	if (input_unbind_key(kmname, k) < 0) {
		log_set("Could not unbind key %u+%s in keymap '%s': %s",
		        k->mod, XKeysymToString(k->sym), kmname, log_str());
		return -1;
	}
	return 0;
}

static int
_action_keymap_create(struct list **results, struct list const *args)
{
	char const *kmname;
	bool grab;
	(void) results;

	kmname = ((struct value *) args->elements[0])->data.str;
	grab = ((struct value *) args->elements[1])->data.b;

	if (input_create_keymap(kmname, grab) < 0) {
		log_set("Could not create keymap '%s': %s", kmname, log_str());
		return -1;
	}
	return 0;
}

static int
_action_keymap_destroy(struct list **results, struct list const *args)
{
	char const *kmname;
	(void) results;

	kmname = ((struct value *) args->elements[0])->data.str;

	if (input_destroy_keymap(kmname) < 0) {
		log_set("Could not destroy keymap '%s': %s", kmname, log_str());
		return -1;
	}
	return 0;
}

static int
_action_mouse_move_window(struct list **results, struct list const *args)
{
	static struct client *c;
	static struct position pointerpos_old;

	struct workspace *ws;
	struct button_event *bev;
	struct position pointerpos_new;
	struct position clientpos_old, clientpos_new, clientpos_delta;
	(void) results;

	/* get button event information */
	bev = &((struct value *) args->elements[0])->data.bev;
	pointerpos_new = bev->position;

	/* restore client information for ongoing move */
	if (bev->type == BUTTON_PRESS) {
		c = bev->client;
		pointerpos_old = pointerpos_new;
	} else if (!c) {
		log_set("Received %s, but no client grabbed; ignoring",
		        button_event_type_str(bev->type));
		return -1;
	}

	/* check conditions for moving */
	if (c->fullscreen) {
		DEBUG("Client is fullscreen; not moving");
		return 0;
	}
	ws = c->ws;
	if (ws->nofloat) {
		DEBUG("Client is on non-floating workspace '%s'; not moving",
		      ws->name);
		return 0;
	}

	/* float and move under cursor */
	if (!c->floating) {
		DEBUG("Client is not floating, floating");
		if (workspace_set_floating(ws, c, true) < 0) {
			log_set("Could not float client %lu on workspace '%s': %s",
			        c->xwindow->id, ws->name, log_str());
			return -1;
		}
		clientpos_new.x = pointerpos_new.x
		                  - (int signed) (c->dim_floating.width / 2
		                                  + c->border.width);
		clientpos_new.y = pointerpos_new.y
		                  - (int signed) (c->dim_floating.height / 2
		                                  + c->border.width);
		if (client_move(c, clientpos_new) < 0)
			ERROR("Could not move floating client under cursor: %s",
			      log_str());
	} else {
		workspace_raise_floating(ws, c);
	}

	switch (bev->type) {
	case BUTTON_PRESS:
		cursor_grab(CURSOR_SHAPE_MOVE);
		pointerpos_old = pointerpos_new;
		break;
	case BUTTON_MOVE:
		clientpos_old = c->dim_floating.pos;
		clientpos_delta = (struct position) {
			pointerpos_new.x - pointerpos_old.x,
			pointerpos_new.y - pointerpos_old.y,
		};
		clientpos_new = (struct position) {
			clientpos_old.x + clientpos_delta.x,
			clientpos_old.y + clientpos_delta.y,
		};
		if (client_move(c, clientpos_new) < 0)
			ERROR("Could not mouse-move client to %+d%+d: %s",
			      clientpos_new.x, clientpos_new.y, log_str());
		pointerpos_old = pointerpos_new;
		break;
	case BUTTON_RELEASE:
		cursor_ungrab();
		c = NULL;
		break;
	case BUTTON_EVENT_CURRENT:
		FATAL_BUG("Invalid button event: BUTTON_EVENT_CURRENT");
	}
	return 0;
}

static int
_action_mouse_resize_window(struct list **results, struct list const *args)
{
	static struct client *c = NULL;
	static struct rectangle clientdim;
	static struct position pointerpos_old;
	static enum _resize_type resize_type = RESIZE_NONE;

	struct workspace *ws;
	struct rectangle clientdim_real;
	struct button_event *bev;
	struct position pointerpos_new, pointerpos_delta;
	int cdx, cdy, cdw, cdh; /* client dimentions delta */
	enum rectangle_area area;
	enum cursor_shape cursor_shape = CURSOR_SHAPE_NORMAL;

	(void) results;

	/* get button event information */
	bev = &((struct value *) args->elements[0])->data.bev;
	pointerpos_new = bev->position;

	/* restore client information for ongoing resize */
	if (bev->type == BUTTON_PRESS) {
		c = bev->client;
	} else if (!c) {
		log_set("Received %s, but no client grabbed; ignoring",
		        button_event_type_str(bev->type));
		return -1;
	}

	/* determine monitor, workspace, client */
	if (c->fullscreen) {
		DEBUG("Client is fullscreen; not resizing");
		return 0;
	}
	ws = c->ws;
	if (ws->nofloat) {
		DEBUG("Client is on non-floating workspace '%s'; not moving",
		      ws->name);
		return 0;
	}

	/* float and adopt to current size as floating size */
	if (!c->floating) {
		DEBUG("Client is not floating, floating");
		c->dim_floating = c->xwindow->dimension;
		if (workspace_set_floating(ws, c, true) < 0) {
			log_set("Could not float client %lu on workspace '%s': %s",
			        c->xwindow->id, ws->name, log_str());
			return -1;
		}
	} else {
		workspace_raise_floating(ws, c);
	}

	switch (bev->type) {
	case BUTTON_PRESS:
		if (rectangle_get_ninth(c->xwindow->dimension, pointerpos_new, &area) < 0) {
			log_set("Could not determine area of pointer inside client: %s",
			        log_str());
			return -1;
		}

		switch (area) {
		case RECT_BOTTOM:
			resize_type = RESIZE_BOTTOM;
			cursor_shape = CURSOR_SHAPE_RESIZE_BOTTOM;
			break;
		case RECT_BOTTOM_LEFT:
			resize_type = RESIZE_BOTTOM_LEFT;
			cursor_shape = CURSOR_SHAPE_RESIZE_BOTTOM_LEFT;
			break;
		case RECT_BOTTOM_RIGHT:
			resize_type = RESIZE_BOTTOM_RIGHT;
			cursor_shape = CURSOR_SHAPE_RESIZE_BOTTOM_RIGHT;
			break;
		case RECT_LEFT:
			resize_type = RESIZE_LEFT;
			cursor_shape = CURSOR_SHAPE_RESIZE_LEFT;
			break;
		case RECT_RIGHT:
			resize_type = RESIZE_RIGHT;
			cursor_shape = CURSOR_SHAPE_RESIZE_RIGHT;
			break;
		case RECT_TOP:
			resize_type = RESIZE_TOP;
			cursor_shape = CURSOR_SHAPE_RESIZE_TOP;
			break;
		case RECT_TOP_LEFT:
			resize_type = RESIZE_TOP_LEFT;
			cursor_shape = CURSOR_SHAPE_RESIZE_TOP_LEFT;
			break;
		case RECT_TOP_RIGHT:
			resize_type = RESIZE_TOP_RIGHT;
			cursor_shape = CURSOR_SHAPE_RESIZE_TOP_RIGHT;
			break;
		case RECT_CENTRE:
			return 0;
		}
		cursor_grab(cursor_shape);
		clientdim = c->dim_floating;
		pointerpos_old = pointerpos_new;
		break;
	case BUTTON_MOVE:
		if (resize_type == RESIZE_NONE)
			return 0;

		/* determine pointer movement */
		pointerpos_delta = (struct position) {
			.x = pointerpos_new.x - pointerpos_old.x,
			.y = pointerpos_new.y - pointerpos_old.y,
		};
		pointerpos_old = pointerpos_new;

		/* determine new dimensions (theoretical) */
		switch (resize_type) {
		case RESIZE_TOP_LEFT:
		case RESIZE_LEFT:
		case RESIZE_BOTTOM_LEFT:
			cdw = -pointerpos_delta.x;
			cdx = pointerpos_delta.x;
			break;
		case RESIZE_TOP_RIGHT:
		case RESIZE_RIGHT:
		case RESIZE_BOTTOM_RIGHT:
			cdw = pointerpos_delta.x;
			cdx = 0;
			break;
		default:
			cdw = 0;
			cdx = 0;
			break;
		}
		switch (resize_type) {
		case RESIZE_TOP_LEFT:
		case RESIZE_TOP:
		case RESIZE_TOP_RIGHT:
			cdh = -pointerpos_delta.y;
			cdy = pointerpos_delta.y;
			break;
		case RESIZE_BOTTOM_LEFT:
		case RESIZE_BOTTOM:
		case RESIZE_BOTTOM_RIGHT:
			cdh = pointerpos_delta.y;
			cdy = 0;
			break;
		default:
			cdh = 0;
			cdy = 0;
			break;
		}
		clientdim = (struct rectangle) {
			.x = clientdim.x + cdx,
			.y = clientdim.y + cdy,
			.width = (int unsigned)
			         MAX(((int signed) clientdim.width + cdw), 1),
			.height = (int unsigned)
			          MAX(((int signed) clientdim.height + cdh), 1),
		};

		/* determine new dimensions (real, with size hints applied) */
		clientdim_real = clientdim;
		client_get_sizehints(c, &clientdim_real.size);
		switch (resize_type) {
		case RESIZE_TOP_LEFT:
		case RESIZE_LEFT:
		case RESIZE_BOTTOM_LEFT:
			cdx = -((int signed)
			        (clientdim_real.width - clientdim.width));
			break;
		default:
			cdx = 0;
			break;
		}
		switch (resize_type) {
		case RESIZE_TOP_LEFT:
		case RESIZE_TOP:
		case RESIZE_TOP_RIGHT:
			cdy = -((int signed)
			        (clientdim_real.height - clientdim.height));
			break;
		default:
			cdy = 0;
			break;
		}
		clientdim_real.pos = (struct position) {
			.x = clientdim_real.x + cdx,
			.y = clientdim_real.y + cdy,
		};

		/* resize client to new real size (but keep calculating with
		 * theoretical size):
		 */
		if (client_moveresize(c, clientdim_real) < 0)
			ERROR("Could not redimension client %lu to %ux%u%+d%+d: %s",
			      c->xwindow->id,
			      clientdim_real.width, clientdim_real.height,
			      clientdim_real.x, clientdim_real.y, log_str());
		break;
	case BUTTON_RELEASE:
		if (resize_type == RESIZE_NONE)
			return 0;
		cursor_ungrab();
		c = NULL;
		break;
	case BUTTON_EVENT_CURRENT:
		FATAL_BUG("Invalid button event: BUTTON_EVENT_CURRENT");
	}

	pointerpos_old = pointerpos_new;
	return 0;
}

static int
_action_quit(struct list **results, struct list const *args)
{
	(void) results;
	(void) args;

	karuiwm.runstate = STOPPING;
	return 0;
}

static int
_action_restart(struct list **results, struct list const *args)
{
	(void) results;
	(void) args;

	karuiwm.runstate = RESTARTING;
	return 0;
}

static int
_action_scratchpad_set_margin(struct list **results, struct list const *args)
{
	int unsigned pad_margin;
	(void) results;

	pad_margin = ((struct value *) args->elements[0])->data.ui;

	return seat_set_scratchpad_margin(pad_margin);
}

static int
_action_scratchpad_toggle(struct list **results, struct list const *args)
{
	int retval = 0;
	(void) results;
	(void) args;

	if (!seat.selmon)
		return 0;

	if (state_set_clientmask(false) < 0) {
		log_set("Could not unset state mask on state: %s", log_str());
		return -1;
	}
	if (state.scratchpad->mon == seat.selmon) {
		/* shown on this monitor, hide it */
		if (monitor_show_scratchpad(seat.selmon, NULL) < 0) {
			log_set("Could not hide scratchpad: %s", log_str());
			retval = -1;
		}
	} else {
		/* hidden or somewhere else, attempt to show it */
		if (workspace_isempty(state.scratchpad))
			return 0;
		if (monitor_show_scratchpad(seat.selmon,
		                            state.scratchpad) < 0) {
			log_set("Could not show scratchpad: %s", log_str());
			retval = -1;
		}
	}
	log_push();
	if (state_set_clientmask(true) < 0)
		ERROR("Could not set client mask on state: %s", log_str());
	log_pop();

	return retval;
}

static int
_action_scratchpad_toggle_window(struct list **results, struct list const *args)
{
	struct workspace *srcws, *dstws;
	struct client *c;
	(void) results;
	(void) args;

	if (!seat.selmon)
		return 0;

	/* determine involved workspaces (regular & scratchpad) */
	srcws = seat.selmon->selws;
	dstws = srcws == state.scratchpad ? seat.selmon->workspace
	                                  : state.scratchpad;
	if (!srcws || !srcws->selcli)
		return 0;
	c = srcws->selcli;

	if (srcws == state.scratchpad) {
		/* client on scratchpad, move to regular workspace */
		if (workspace_transfer_client(dstws, c) < 0) {
			log_set("Could not transfer client for X window %lu from scratchpad to workspace '%s': %s",
			        c->xwindow->id, dstws->name, log_str());
			goto error_transfer;
		}
	} else {
		/* client not on scratchpad, attach it (and show scratchpad) */
		if (workspace_transfer_client(dstws, c) < 0) {
			log_set("Could not transfer client for X window %lu from workspace '%s' to scratchpad: %s",
			        c->xwindow->id, srcws->name, log_str());
			goto error_transfer;
		}
		if (monitor_show_scratchpad(seat.selmon, dstws) < 0)
			ERROR("Could not show scratchpad: %s", log_str());
	}

	return 0;

 error_transfer:
	return -1;
}

static int
_action_state_list_workspaces(struct list **results, struct list const *args)
{
	struct list *wsnames;
	int unsigned i;
	struct workspace const *ws;
	struct value *val;
	(void) args;

	if (!results)
		return 0;

	wsnames = list_alloc();
	if (!wsnames) {
		log_set("Could not create workspace names list: %s", log_str());
		goto error_list;
	}

	LIST_FOR (state.workspaces, i, ws) {
		val = value_alloc_string(ws->name);
		if (!val) {
			log_set("Could not create string value for '%s': %s",
			        ws->name, log_str());
			goto error_val;
		}
		list_append(wsnames, val);
	}

	*results = wsnames;
	return (int) (*results)->size;

 error_val:
	log_push();
	list_clear(wsnames, (list_delfunc) value_free);
	list_free(wsnames);
	log_pop();
 error_list:
	return -1;
}

static int
_action_window_close(struct list **results, struct list const *args)
{
	(void) results;
	(void) args;

	if (!seat.selmon
	|| !seat.selmon->selws
	|| workspace_isempty(seat.selmon->selws))
		return 0;

	client_kill(seat.selmon->selws->selcli);
	return 0;
}

static int
_action_window_send(struct list **results, struct list const *args)
{
	struct client *c;
	struct workspace *ws;
	char const *wsname = ((struct value *) args->elements[0])->data.str;
	bool follow = ((struct value *) args->elements[1])->data.b;
	(void) results;

	/* get client and workspace */
	if (!seat.selmon
	|| !seat.selmon->selws
	|| workspace_isempty(seat.selmon->selws))
		return 0;
	c = seat.selmon->selws->selcli;
	if (!state_locate_workspace(&ws, wsname)) {
		log_set("Workspace named '%s' not found", wsname);
		return -1;
	}

	/* transfer to new workspace */
	if (workspace_transfer_client(ws, c) < 0) {
		log_set("Could not transfer client for X window %lu from workspace '%s' to workspace '%s': %s",
		        c->xwindow->id, c->ws->name, ws->name, log_str());
		goto error_transfer;
	}

	/* rearrange workspaces */
	if (monitor_arrange(seat.selmon) < 0)
		ERROR("Could not rearrange clients on monitor showing source workspace '%s': %s",
		      seat.selmon->selws->name, log_str());
	if (ws->mon) {
		if (monitor_arrange(ws->mon) < 0)
			ERROR("Could not rearrange clients on monitor showing destination workspace '%s': %s",
			      ws->name, log_str());
		monitor_restack(ws->mon);
	}

	/* follow if needed */
	if (follow)
		monitor_show_workspace(seat.selmon, ws);

	return 0;

 error_transfer:
	return -1;
}

static int
_action_window_set_border_colour_focused(struct list **results, struct list const *args)
{
	uint32_t col = ((struct value *) args->elements[0])->data.col;
	(void) results;

	return state_set_client_border_colour_focused(col);
}

static int
_action_window_set_border_colour_unfocused(struct list **results, struct list const *args)
{
	uint32_t col = ((struct value *) args->elements[0])->data.col;
	(void) results;

	return state_set_client_border_colour_unfocused(col);
}

static int
_action_window_set_border_width(struct list **results, struct list const *args)
{
	int unsigned bw = ((struct value *) args->elements[0])->data.ui;
	(void) results;

	if (state_set_client_border_width(bw) < 0)
		ERROR("Could not set client border width in state: %s", log_str());
	if (seat_set_client_border_width(bw) < 0)
		ERROR("Could not set client border width in seat: %s", log_str());
	return 0;
}

static int
_action_window_shift(struct list **results, struct list const *args)
{
	int posdiff = ((struct value *) args->elements[0])->data.i;
	bool sticky = true;
	(void) results;

	return _step_window(posdiff, sticky);
}

static int
_action_window_toggle_float(struct list **results, struct list const *args)
{
	struct client *c;
	struct workspace *ws;
	(void) results;
	(void) args;

	if (!seat.selmon)
		return 0;
	ws = seat.selmon->selws;
	if (!ws)
		return 0;

	if (ws == state.scratchpad) {
		DEBUG("Scratchpad active, not toggling floating state");
		return 0;
	}

	c = ws->selcli;
	if (!c)
		return 0;

	if (workspace_set_floating(ws, c, !c->floating) < 0) {
		log_set("Could not toggle floating for client %lu on workspace '%s': %s",
		        c->xwindow->id, ws->name, log_str());
		return -1;
	}

	return 0;
}

static int
_action_window_zoom(struct list **results, struct list const *args)
{
	unsigned int thispos, otherpos;
	struct workspace *ws = seat.selmon->workspace;
	struct list *clients = ws->tiled;
	struct client *thisc = ws->selcli, *otherc;
	(void) results;
	(void) args;

	if (clients->size < 2)
		return 0;

	if (thisc == clients->elements[0]) {
		/* top window, swap with the next below */
		_step_window(+1, true);
		_step_window(-1, false);
		return 0;
	}

	/* find positions */
	otherpos = 0;
	otherc = clients->elements[otherpos];
	thispos = list_find_index(clients, thisc, NULL);

	/* swap clients */
	clients->elements[thispos] = otherc;
	clients->elements[otherpos] = thisc;
	monitor_restack(seat.selmon);
	if (monitor_arrange(seat.selmon) < 0)
		ERROR("Could not rearrange clients on monitor: %s", log_str());

	return 0;
}

static int
_action_workspace_rename(struct list **results, struct list const *args)
{
	char const *wsname = ((struct value *) args->elements[0])->data.str;
	(void) results;

	if (state_rename_workspace(seat.selmon->workspace, wsname) < 0)
		ERROR("Could not rename workspace: %s", log_str());
	return 0;
}

static int
_action_workspace_set_mfact(struct list **results, struct list const *args)
{
	float mfact = ((struct value *) args->elements[0])->data.f;
	(void) results;

	layout_setmfact(seat.selmon->selws->layout, mfact);
	if (monitor_arrange(seat.selmon) < 0)
		ERROR("Could not rearrange clients on monitor: %s", log_str());
	return 0;
}

static int
_action_workspace_set_nmaster(struct list **results, struct list const *args)
{
	int nmaster = ((struct value *) args->elements[0])->data.i;
	(void) results;

	layout_setnmaster(seat.selmon->selws->layout, (ssize_t) nmaster);
	if (monitor_arrange(seat.selmon) < 0)
		ERROR("Could not rearrange clients on monitor: %s", log_str());
	return 0;
}

static int
_action_workspace_step_layout(struct list **results, struct list const *args)
{
	enum layout_type next_type = 0;
	struct workspace *ws;
	int step = ((struct value *) args->elements[0])->data.i;

	(void) results;

	if (step == 0)
		return 0;

	ws = seat.selmon->selws;

	switch (ws->layout->type) {
	case LAYOUT_RSTACK:
		next_type = step > 0 ? LAYOUT_BSTACK : LAYOUT_LFIXED;
		break;
	case LAYOUT_BSTACK:
		next_type = step > 0 ? LAYOUT_LFIXED : LAYOUT_RSTACK;
		break;
	case LAYOUT_LFIXED:
		next_type = step > 0 ? LAYOUT_RSTACK : LAYOUT_BSTACK;
		break;
	}

	if (workspace_set_layout(ws, next_type) < 0) {
		log_set("Could not set layout on workspace '%s': %s",
		        ws->name, log_str());
		goto error_layout_set;
	}

	return 0;

 error_layout_set:
	return -1;
}

static struct workspace *
_get_or_create_workspace(char const *name)
{
	struct workspace *ws;

	if (state_locate_workspace(&ws, name))
		return ws;

	DEBUG("Workspace '%s' not found in state, creating new", name);

	ws = workspace_alloc();
	if (!ws) {
		log_set("Could not create new workspace: %s", log_str());
		goto error_alloc;
	}
	workspace_rename(ws, name);
	if (state_attach_workspace(ws) < 0) {
		log_set("Could not attach new workspace '%s' to state: %s",
		        name, log_str());
		goto error_attach;
	}

	return ws;

 error_attach:
	log_push();
	workspace_free(ws);
	log_pop();
 error_alloc:
	return NULL;
}

static int
_step_window(int posdiff, bool sticky)
{
	int unsigned i_src, i_dst;
	struct list *clients;
	struct workspace *ws;
	struct client *c;

	if (!seat.selmon)
		return 0;

	ws = seat.selmon->selws;
	if (!ws || workspace_isempty(ws))
		return 0;

	c = ws->selcli;
	assert(c);
	if (c->floating)
		/* doesn't work for floating clients */
		return 0;

	clients = ws->tiled;
	if (clients->size < 2)
		/* nothing to shift */
		return 0;

	/* find source and destination indexes */
	i_src = list_find_index(clients, c, NULL);
	i_dst = (int unsigned) ((int signed) (i_src + clients->size) + posdiff)
	        % (int unsigned) clients->size;

	if (sticky) {
		/* swap clients and restack */
		clients->elements[i_src] = clients->elements[i_dst];
		clients->elements[i_dst] = c;
		monitor_restack(seat.selmon);
		if (monitor_arrange(seat.selmon) < 0)
			ERROR("Could not rearrange clients on monitor: %s", log_str());
	} else {
		/* focus new client */
		workspace_focus_client(ws, clients->elements[i_dst]);
		monitor_restack(seat.selmon);
	}

	return 0;
}
