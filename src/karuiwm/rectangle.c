#include <karuiwm/rectangle.h>
#include <karuiwm/utils.h>
#include <karui/log.h>

int
rectangle_centre(struct rectangle outer, struct rectangle *inner)
{
	int ocx = outer.x + (int signed) (outer.width / 2);
	int ocy = outer.y + (int signed) (outer.height / 2);

	if (rectangle_fit(outer, inner) < 0)
		return -1;

	inner->x = ocx - (int signed) (inner->width / 2);
	inner->y = ocy - (int signed) (inner->height / 2);
	return 0;
}

bool
rectangle_contains_point(struct rectangle r, struct position pos)
{
	return pos.x >= r.x && pos.x < r.x + (int signed) r.width
	    && pos.y >= r.y && pos.y < r.y + (int signed) r.height;
}

bool
rectangle_equals(struct rectangle const *r1, struct rectangle const *r2)
{
	return position_equals(&r1->pos, &r2->pos)
	    && size_equals(&r1->size, &r2->size);
}

int
rectangle_fit(struct rectangle outer, struct rectangle *inner)
{
	int ox = outer.x, oy = outer.y, ix = inner->x, iy = inner->y;
	int unsigned ow = outer.width, oh = outer.height,
	             iw = inner->width, ih = inner->height;

	inner->x = MAX(MIN(ix, ox + (int signed) (ow - iw)), ox);
	inner->y = MAX(MIN(iy, oy + (int signed) (oh - ih)), oy);
	inner->width = MIN(iw, ow);
	inner->height = MIN(ih, oh);
	return 0;
}

int
rectangle_get_quadrant(struct rectangle r, struct position pos,
                       enum rectangle_area *area)
{
	int xmiddle, ymiddle;

	if (!rectangle_contains_point(r, pos)) {
		log_set("Point (%d, %d) not contained in rectangle %ux%u%+d%+d",
		        pos.x, pos.y, r.width, r.height, r.x, r.y);
		return -1;
	}

	xmiddle = r.x + (int signed) (r.width  / 2);
	ymiddle = r.y + (int signed) (r.height / 2);

	*area = pos.x < xmiddle ? pos.y < ymiddle ? RECT_TOP_LEFT
	                                          : RECT_BOTTOM_LEFT
	                        : pos.y < ymiddle ? RECT_TOP_RIGHT
	                                          : RECT_BOTTOM_RIGHT;
	return 0;
}

int
rectangle_get_ninth(struct rectangle r, struct position pos,
                    enum rectangle_area *area)
{
	int xleft, xright, ytop, ybottom;

	if (!rectangle_contains_point(r, pos)) {
		log_set("Point (%d, %d) not contained in rectangle %ux%u%+d%+d",
		        pos.x, pos.y, r.width, r.height, r.x, r.y);
		return -1;
	}

	xleft   = r.x + (int signed) (1 * r.width  / 3);
	xright  = r.x + (int signed) (2 * r.width  / 3);
	ytop    = r.y + (int signed) (1 * r.height / 3);
	ybottom = r.y + (int signed) (2 * r.height / 3);

	*area = pos.x < xleft  ? pos.y < ytop    ? RECT_TOP_LEFT
	                       : pos.y < ybottom ? RECT_LEFT
	                                         : RECT_BOTTOM_LEFT
	      : pos.x < xright ? pos.y < ytop    ? RECT_TOP
	                       : pos.y < ybottom ? RECT_CENTRE
	                                         : RECT_BOTTOM
	                       : pos.y < ytop    ? RECT_TOP_RIGHT
	                       : pos.y < ybottom ? RECT_RIGHT
	                                         : RECT_BOTTOM_RIGHT;
	return 0;
}

int unsigned
rectangle_intersection(struct rectangle r1, struct rectangle r2)
{
	int left = MAX(r1.x, r2.x);
	int right = MIN(r1.x + (int signed) r1.width,
	                r2.x + (int signed) r2.width);
	int top = MAX(r1.y, r2.y);
	int bottom = MIN(r1.y + (int signed) r1.height,
	                 r2.y + (int signed) r2.height);

	if (left > right || top > bottom)
		/* no intersection */
		return 0;

	return (int unsigned) ((right - left) * (bottom - top));
}
