#include <karuiwm/position.h>
#include <karui/cstr.h>
#include <karui/log.h>
#include <stdbool.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

bool
position_equals(struct position const *pos1, struct position const *pos2)
{
	return pos1->x == pos2->x && pos1->y == pos2->y;
}

int
position_parse(struct position *pos, char const *str)
{
	struct position tpos;
	char *endptr;

	if (cstr_parse_int_signed(&tpos.x, str, &endptr) < 0) {
		log_set("Could not parse X position value from '%s': %s",
		        str, log_str());
		return -1;
	}
	if (cstr_parse_int_signed(&tpos.y, endptr, NULL) < 0) {
		log_set("Could not parse Y position value from '%s': %s",
		        str, log_str());
		return -1;
	}
	*pos = tpos;
	return 0;
}
