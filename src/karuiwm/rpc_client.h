#ifndef _KWM_RPC_CLIENT_H
#define _KWM_RPC_CLIENT_H

struct rpc_client {
	int fd;
	char *name;
	struct fdio_client *fdio;
};

#include <stdbool.h>

struct rpc_client *rpc_client_alloc(int sockfd);
void rpc_client_free(struct rpc_client *c);

bool rpc_client_has_fd(struct rpc_client const *c, int const *fd);

#endif /* ndef _KWM_RPC_CLIENT_H */
