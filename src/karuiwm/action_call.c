#include <karuiwm/action_call.h>
#include <karuiwm/action.h>
#include <karuiwm/buttonbind.h>
#include <karuiwm/keybind.h>
#include <karuiwm/registry.h>
#include <karuiwm/value.h>
#include <karui/cstr.h>
#include <karui/log.h>
#include <karui/memory.h>
#include <assert.h>
#include <stdlib.h>

struct action_call *
action_call_alloc(struct action const *action, struct list const *args)
{
	struct action_call *ac;

	ac = memory_alloc(sizeof(struct action_call), "action call structure");
	if (!ac)
		goto error_struct_alloc;

	ac->name = cstr_dup(action->name);
	if (!ac->name) {
		log_set("Could not copy action name: %s", log_str());
		goto error_name;
	}

	ac->action = action;
	list_append(action->action_calls, ac);

	ac->args = list_copy(args, (list_copyfunc) value_copy,
	                     (list_delfunc) value_free);
	if (!ac->args) {
		log_set("Could not create arguments list: %s", log_str());
		goto error_args_copy;
	}

	ac->buttonbind = NULL;

	return ac;

 error_args_copy:
	log_push();
	cstr_free(ac->name);
	log_pop();
	log_push();
	action_call_unlink(ac);
	log_pop();
 error_name:
	log_push();
	memory_free(ac);
	log_pop();
 error_struct_alloc:
	return NULL;
}

void
action_call_free(struct action_call *ac)
{
	assert(!ac->buttonbind);

	list_clear(ac->args, (list_delfunc) value_free);
	list_free(ac->args);
	if (ac->action)
		action_call_unlink(ac);
	cstr_free(ac->name);
	memory_free(ac);
}

struct action_call *
action_call_copy(struct action_call const *ac)
{
	if (!ac->action) {
		log_set("Action call for '%s' is unlinked", ac->name);
		return NULL;
	}
	return action_call_alloc(ac->action, ac->args);
}

int
action_call_invoke(struct action_call const *ac, struct button_event const *bev,
                   struct list **results)
{
	int unsigned i;
	struct value *val;

	if (!bev || bev->type != BUTTON_MOVE)
		DEBUG("%s action '%s' (%zu elements)",
		      bev ? (bev->type == BUTTON_PRESS ? "Starting" :
		             bev->type == BUTTON_RELEASE ? "Ending" :
		                                       "Continuing")
		          : "Invoking", ac->name, ac->args->size);

	if (!ac->action) {
		log_set("Action call for '%s' is unlinked", ac->name);
		return -1;
	}

	LIST_FOR (ac->args, i, val) {
		/* substitute button event arguments with button event data */
		if (val->type == VALUE_BUTTON_EVENT) {
			if (!bev) {
				log_set("Action '%s' requires button event, but no button event information given",
				        ac->action->name);
				goto error_button_event;
			}
			((struct value *) ac->args->elements[i])->data.bev = *bev;
		}
	}

	return ac->action->function(results, ac->args);

 error_button_event:
	return -1;
}

struct action_call *
action_call_parse(char const *str)
{
	char *aname;
	struct action_call *ac;
	struct list *strargs;
	struct action const *a;
	int unsigned i;
	char const *strarg;
	struct value *arg;
	struct list *args;

	strargs = cstr_split_smart(str);
	if (!strargs) {
		log_set("Could not split action call string '%s': %s",
		        str, log_str());
		goto error_splitstr;
	}

	if (strargs->size == 0) {
		log_set("Action call string contains 0 elements");
		goto error_emptystr;
	}
	aname = strargs->elements[0];
	list_remove(strargs, strargs->elements[0], NULL);

	a = registry_find_action_by_name(aname);
	if (!a) {
		log_set("Action '%s' not found", aname);
		goto error_findaction;
	}
	if (strargs->size != a->nargs) {
		log_set("Action '%s' takes %zu arguments, but only %zu given",
		        a->name, a->nargs, strargs->size);
		goto error_nargs;
	}

	args = list_alloc();
	if (!args) {
		log_set("Could not create list of arguments: %s", log_str());
		goto error_argslist;
	}

	LIST_FOR (strargs, i, strarg) {
		arg = value_parse(a->arg_types[i], strarg);
		if (!arg) {
			log_set("Could not parse argument '%s': %s",
			        strarg, log_str());
			goto error_arg_parse;
		}
		list_append(args, arg);
	}

	ac = action_call_alloc(a, args);
	if (!ac) {
		log_set("Could not create action call: %s", log_str());
		goto error_ac;
	}

	list_clear(args, (list_delfunc) value_free);
	list_free(args);
	cstr_free(aname);
	list_clear(strargs, (list_delfunc) cstr_free);
	list_free(strargs);
	return ac;

 error_ac:
 error_arg_parse:
	log_push();
	list_clear(args, (list_delfunc) value_free);
	list_free(args);
	log_pop();
 error_argslist:
 error_nargs:
 error_findaction:
	log_push();
	cstr_free(aname);
	log_pop();
 error_emptystr:
	log_push();
	list_clear(strargs, (list_delfunc) cstr_free);
	list_free(strargs);
	log_pop();
 error_splitstr:
	return NULL;
}

void
action_call_unlink(struct action_call *ac)
{
	assert(ac->action);

	list_remove(ac->action->action_calls, ac, NULL);
	ac->action = NULL;
}
