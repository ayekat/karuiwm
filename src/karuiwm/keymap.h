#ifndef _KWM_KEYMAP_H
#define _KWM_KEYMAP_H

#include <stdbool.h>

struct keymap {
	char *name;
	bool grab_keyboard;
	bool active;
	struct list *keybinds;
};

#include <karuiwm/key.h>
#include <karuiwm/keybind.h>

struct keymap *keymap_alloc(char const *name, bool grab_keyboard);
void keymap_free(struct keymap *km);

int keymap_activate(struct keymap *km);
int keymap_add_keybind(struct keymap *km, struct keybind *kb, bool overwrite);
int keymap_deactivate(struct keymap *km);
bool keymap_has_action(struct keymap const *km, struct action const *a);
bool keymap_has_action_with_name(struct keymap const *km, char const *name);
bool keymap_has_name(struct keymap const *km, char const *name);
int keymap_resolve_key(struct keymap const *km, struct key const *k);
int keymap_unbind_key(struct keymap *km, struct key const *k);

#endif /* ndef _KWM_KEYMAP_H */
