#ifndef _KWM_COMMANDLINE_H
#define _KWM_COMMANDLINE_H

int commandline_init(int argc, char const *const *argv);
void commandline_deinit(void);

char const *commandline_get(char const *key);
struct list *commandline_serialise(void);
int commandline_set(char const *key, char const *value);

#endif /* ndef _KWM_COMMANDLINE_H */
