#ifndef _KWM_REGISTRY_SESSION_H
#define _KWM_REGISTRY_SESSION_H

struct registry_session {
	char *name;
	struct list *actions;
};

#include <karuiwm/action.h>
#include <stdbool.h>

struct registry_session *registry_session_alloc(char const *name);
int registry_session_init(struct registry_session *rs, char const *name);
void registry_session_deinit(struct registry_session *rs);
void registry_session_free(struct registry_session *rs);

int registry_session_add_action(struct registry_session *rs, struct action *a);
struct action *registry_session_find_action_by_name(
                           struct registry_session const *rs, char const *name);
void registry_session_remove_action(struct registry_session *rs,
                                    struct action *a);

#endif /* ndef _KWM_REGISTRY_SESSION_H */
