#include <karuiwm/commandline.h>
#include <karuiwm/commandline_argument.h>
#include <karui/cstr.h>
#include <karui/list.h>
#include <karui/log.h>
#include <karui/memory.h>

static int _set_argument(struct commandline_argument *cla);

static struct {
	struct list *arguments;
} _cmdline;

int
commandline_init(int argc, char const *const *argv)
{
	struct commandline_argument *cla;
	int unsigned i;

	_cmdline.arguments = list_alloc();
	if (!_cmdline.arguments) {
		log_set("Could not create arguments list: %s", log_str());
		goto error_args;
	}

	for (i = 0; i < (int unsigned) argc; ++i) {
		cla = commandline_argument_parse(argv[i]);
		if (!cla) {
			log_set("Could not parse argument '%s': %s",
			        argv[i], log_str());
			goto error_arg;
		}
		if (_set_argument(cla) < 0) {
			log_set("Could not add argument %s=%s to commandline: %s",
			        cla->key, cla->value, log_str());
			commandline_argument_free(cla);
			goto error_arg;
		}
	}

	return 0;

 error_arg:
	log_push();
	list_clear(_cmdline.arguments,
	           (list_delfunc) commandline_argument_free);
	list_free(_cmdline.arguments);
	log_pop();
 error_args:
	return -1;
}

void
commandline_deinit(void)
{
	list_clear(_cmdline.arguments,
	           (list_delfunc) commandline_argument_free);
	list_free(_cmdline.arguments);
}

char const *
commandline_get(char const *key)
{
	struct commandline_argument *cla;

	cla = list_find(_cmdline.arguments,
	                key, (list_eqfunc) commandline_argument_has_key);
	return cla ? cla->value : NULL;
}

struct list *
commandline_serialise(void)
{
	struct list *serialised;
	int unsigned i;
	struct commandline_argument *cla;
	char *cla_str;

	serialised = list_alloc();
	if (!serialised) {
		log_set("Could not create list for serialised arguments: %s",
		        log_str());
		goto error_list;
	}

	LIST_FOR (_cmdline.arguments, i, cla) {
		cla_str = cstr_format("%s=%s", cla->key, cla->value);
		if (!cla_str) {
			log_set("Could not serialise argument %s=%s: %s",
			        cla->key, cla->value, log_str());
			goto error_arg;
		}
		list_append(serialised, cla_str);
	}

	return serialised;

 error_arg:
	log_push();
	list_clear(serialised, (list_delfunc) cstr_free);
	list_free(serialised);
	log_pop();
 error_list:
	return NULL;
}

int
commandline_set(char const *key, char const *value)
{
	struct commandline_argument *cla;

	cla = commandline_argument_alloc(key, value);
	if (!cla) {
		log_set("Could not create commandline argument: %s", log_str());
		goto error_arg;
	}

	if (_set_argument(cla) < 0) {
		log_set("Could not add argument to commandline: %s", log_str());
		goto error_append;
	}

	return 0;

 error_append:
	log_push();
	commandline_argument_free(cla);
	log_pop();
 error_arg:
	return -1;
}

static int
_set_argument(struct commandline_argument *cla)
{
	struct commandline_argument *cla_found;

	cla_found = list_find(_cmdline.arguments, cla->key,
	                      (list_eqfunc) commandline_argument_has_key);
	if (cla_found) {
		DEBUG("Replacing previous commandline argument %s=%s with %s=%s",
		      cla_found->key, cla_found->value, cla->key, cla->value);
		list_remove(_cmdline.arguments, cla_found, NULL);
		commandline_argument_free(cla_found);
	}

	list_append(_cmdline.arguments, cla);

	return 0;
}
