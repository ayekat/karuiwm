#include <karuiwm/system.h>
#include <karuiwm/karuiwm.h>
#include <karui/cstr.h>
#include <karui/envvars.h>
#include <karui/log.h>
#include <karui/memory.h>
#include <karui/runtimedir.h>
#include <karui/userdirs.h>
#include <errno.h>
#include <locale.h>
#include <signal.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

static int _install_sigchld(void);
static void _sigchld(int sig);

int
system_init(void)
{
	if (envvars_init() < 0) {
		log_set("Could not initialise environment variables: %s",
		        log_str());
		goto error_envvars;
	}

	if (userdirs_init() < 0) {
		log_set("Could not initialise user directory variables: %s",
		        log_str());
		goto error_userdirs;
	}

	if (runtimedir_init(APPNAME) < 0) {
		log_set("Could not initialise runtime directory: %s",
		        log_str());
		goto error_runtimedir;
	}

	if (_install_sigchld() < 0)
		goto error_signal;

	karuiwm.locale = setlocale(LC_ALL, "");
	if (!karuiwm.locale) {
		log_set("Could not set locale");
		goto error_locale;
	}
	DEBUG("Set locale %s", karuiwm.locale);

	return 0;

 error_locale:
 error_signal:
	log_push();
	runtimedir_deinit();
	log_pop();
 error_runtimedir:
	log_push();
	userdirs_deinit();
	log_pop();
 error_userdirs:
	log_push();
	envvars_deinit();
	log_pop();
 error_envvars:
	return -1;
}

void
system_deinit(void)
{
	runtimedir_deinit();
	userdirs_deinit();
	envvars_deinit();
}

int
system_exec(struct list const *args)
{
	char **argv;
	int unsigned i;
	char const *arg;

	/* create non-const variant of argument list */
	argv = memory_alloc((args->size + 1) * sizeof(char *),
	                    "arguments array");
	if (!argv)
		goto error_argv;

	/* copy strings */
	LIST_FOR (args, i, arg) {
		argv[i] = cstr_dup(arg);
		if (!argv[i]) {
			log_set("Could not duplicate argument string %u: %s",
			        i, log_str());
			goto error_argdup;
		}
	}
	argv[args->size] = NULL;

	/* execute! */
	(void) execvp(argv[0], argv);

	/* if we reach here, execvp failed */
	log_set("execvp() failed: %s", strerror(errno));

 error_argdup:
	log_push();
	LIST_FOR (args, i, arg) {
		if (!argv[i])
			break;
		cstr_free(argv[i]);
	}
	memory_free(argv);
	log_pop();
 error_argv:
	return (pid_t) -1;
}

pid_t
system_fork_exec(struct list const *args)
{
	pid_t pid;

	pid = fork();
	if (pid == 0) {
		setsid();
		(void) system_exec(args);
		FATAL("Could not execute command in child process: %s",
		      log_str());
	} else if (pid < 0) {
		log_set("fork() failed: %s", strerror(errno));
		goto error_fork;
	}
	return pid;

 error_fork:
	return -1;
}

static int
_install_sigchld(void)
{
	if (signal(SIGCHLD, _sigchld) == SIG_ERR) {
		log_set("Could not install SIGCHLD signal handler: %s",
		        strerror(errno));
		return -1;
	}

	return 0;
}

static void
_sigchld(int sig)
{
	(void) sig;

	if (_install_sigchld() < 0) {
		log_flush(LOG_CRITICAL);
		return;
	}

	/* pid -1 makes it equivalent to wait() (wait for all children);
	 * here we just add WNOHANG */
	while (0 < waitpid(-1, NULL, WNOHANG));
}
