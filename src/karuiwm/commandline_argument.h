#ifndef _KWM_COMMANDLINE_ARGUMENT_H
#define _KWM_COMMANDLINE_ARGUMENT_H

struct commandline_argument {
	char *key;
	char *value;
};

#include <stdbool.h>

struct commandline_argument *commandline_argument_alloc(char const *key,
                                                        char const *value);
void commandline_argument_free(struct commandline_argument *cla);

bool commandline_argument_has_key(struct commandline_argument const *cla,
                                  char const *key);
struct commandline_argument *commandline_argument_parse(char const *str);

#endif /* ndef _KWM_COMMANDLINE_ARGUMENT_H */
