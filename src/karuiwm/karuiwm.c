#include <karuiwm/karuiwm.h>
#include <karuiwm/bus.h>
#include <karuiwm/commandline.h>
#include <karuiwm/fdio.h>
#include <karuiwm/modman.h>
#include <karuiwm/mod_init.h>
#include <karuiwm/registry.h>
#include <karuiwm/rpc.h>
#include <karuiwm/seat.h>
#include <karuiwm/state.h>
#include <karuiwm/system.h>
#include <karuiwm/useraction.h>
#include <karuiwm/utils.h>
#include <karuiwm/wsmap.h>
#include <karuiwm/xserver.h>
#include <karui/list.h>
#include <karui/log.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>

static void _init(void);
static void _run(void);
static void _deinit(void);

static int _parse_options(int argc, char **argv);
static void _print_help(void);
static void _print_missing_optarg(int opt);
static void _print_unknown_opt(int opt);

struct karuiwm karuiwm;

/* TODO: make this function arg for _parse_options() */
static bool _debugflag;

static void
_init(void)
{
	/* base OS */
	if (system_init() < 0)
		FATAL("Could not initialise base system: %s", log_str());

	/* registry, bus and I/O */
	if (registry_init() < 0)
		FATAL("Could not initialise registry: %s", log_str());
	if (bus_init() < 0)
		FATAL("Could not initialise event bus: %s", log_str());
	if (fdio_init() < 0)
		FATAL("Could not initialise filedescriptor I/O: %s", log_str());

	/* future module (TODO): backend (X server) */
	if (xserver_init() < 0)
		FATAL("Could not initialise X server connection: %s",
		      log_str());

	/* state and seat */
	if (state_init() < 0)
		FATAL("Could not initialise state: %s", log_str());
	if (seat_init() < 0)
		FATAL("Could not initialise seat: %s", log_str());

	/* modules */
	if (modman_init() < 0)
		FATAL("Could not initialise modules manager: %s", log_str());

	/* module (TODO, make configurable): statusbar */
	if (modman_load("statusbar") < 0)
		FATAL("Could not load module 'statusbar': %s", log_str());

	/* future module (TODO): workspace map */
	if (wsmap_init() < 0)
		FATAL("Could not initialise workspace map: %s",
		      log_str());

	/* future module (TODO): rpc */
	if (rpc_init() < 0)
		FATAL("Could not initialise RPC module: %s", log_str());

	/* future module (TODO): generic user actions */
	if (useraction_init() < 0)
		FATAL("Could not initialise pseudo-module useractions: %s",
		      log_str());

	/* future module (TODO): init */
	if (mod_init_init() < 0)
		FATAL("Could not load init module: %s", log_str());
}

static void
_run(void)
{
	karuiwm.runstate = RUNNING;
	while (karuiwm.runstate == RUNNING) {
		if (fdio_wait() < 0) {
			ERROR("Could not wait for filedescriptor I/O: %s",
			      log_str());
			break;
		}

		if (rpc_clean_up() < 0)
			ERROR("RPC cleanup failed: %s", log_str());
		if (state_clean_up() < 0)
			ERROR("State cleanup failed: %s", log_str());
	}
}

static void
_deinit(void)
{
	/* future module (TODO): init */
	mod_init_term();

	/* future module (TODO): generic user actions */
	useraction_deinit();

	/* future module (TODO): rpc */
	rpc_deinit();

	/* future module (TODO): workspace map */
	wsmap_deinit(karuiwm.runstate == RESTARTING);

	/* unload all modules */
	modman_deinit();

	/* seat and state */
	seat_deinit();
	state_deinit(karuiwm.runstate == RESTARTING);

	/* future module (TODO): backend (X server) */
	xserver_deinit();

	/* bus, registry and I/O */
	fdio_deinit();
	bus_deinit();
	registry_deinit();

	/* base OS */
	system_deinit();
}

static int
_parse_options(int argc, char **argv)
{
	int opt;

	_debugflag = false;
	optind = 1;
	while ((opt = getopt(argc, argv, ":dh")) >= 0) {
		switch (opt) {
		case 'd':
			_debugflag = true;
			log_set_level(LOG_DEBUG);
			break;
		case 'h':
			_print_help();
			return -1;
		case ':':
			_print_missing_optarg(optopt);
			return -1;
		case '?':
			_print_unknown_opt(optopt);
			return -1;
		default:
			FATAL_BUG("Unhandled getopt case: %c [0x%02X]",
			          opt, opt);
		}
	}
	return optind;
}

static void
_print_help(void)
{
	(void) printf("%s - window manager for X11\n\n", APPNAME);

	(void) printf("\nUsage: %s -h\n", APPNAME);
	(void) printf("       %s [KEY1=VALUE1 [KEY2=VALUE2 ...]]\n", APPNAME);

	(void) printf("\nOptions:\n");
	(void) printf("  -d    Display debug messages.\n");
	(void) printf("  -h    Print this help output and abort.\n");

	/* TODO: Add manpage. */
	(void) printf("\nArguments:\n");
	(void) printf("See %s(1) for more information on the KEY=VALUE pairs.\n",
	              APPNAME);
}

static void
_print_missing_optarg(int opt)
{
	(void) fprintf(stderr, "Missing argument to option -%c\n", opt);
}

static void
_print_unknown_opt(int opt)
{
	(void) fprintf(stderr, "Unknown option: -%c\n", opt);
}

int
main(int argc, char **argv)
{
	int optind;
	struct list *arglist;
	char dbgflg[] = "-d";

	if (log_init(LOG_INFO, APPNAME) < 0)
		return 255;

	optind = _parse_options(argc, argv);
	if (optind < 0)
		FATAL("Could not parse options: %s", log_str());

	if (commandline_init(argc - optind,
	                     (char const *const *) (argv + optind)) < 0)
		FATAL("Could not initialise commandline: %s", log_str());

	INFO("Starting %s...", APPNAME);
	_init();

	_run();

	_deinit();

	if (karuiwm.runstate == RESTARTING) {
		INFO("Restarting %s...", APPNAME);
		arglist = commandline_serialise();
		if (!arglist) {
			CRITICAL("Could not serialise argument lists; restarting without any arguments");
			arglist = list_alloc();
			if (!arglist)
				FATAL("Could not create empty argument list: %s",
				      log_str());
		}
		commandline_deinit();
		if (_debugflag)
			list_prepend(arglist, dbgflg);
		list_prepend(arglist, argv[0]);
		if (system_exec(arglist) < 0)
			FATAL("Could not reexecute %s with %zu arguments: %s",
			      argv[0], arglist->size, log_str());
	} else {
		INFO("shutting down %s...", APPNAME);
		commandline_deinit();
		return EXIT_SUCCESS;
	}
}
