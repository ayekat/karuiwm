#include <karuiwm/bus_session.h>
#include <karuiwm/bus_subscription.h>
#include <karui/cstr.h>
#include <karui/list.h>
#include <karui/log.h>
#include <karui/memory.h>

struct bus_session *
bus_session_alloc(char const *name,
                  void (*cb)(struct bus_subscription const *, void *, void *),
                  void *ctx)
{
	struct bus_session *bs;

	bs = memory_alloc(sizeof(struct bus_session), "bus session structure");
	if (!bs)
		goto error_alloc;

	bs->name = cstr_dup(name);
	if (!bs->name) {
		log_set("Could not duplicate name string: %s", log_str());
		goto error_name;
	}

	bs->context = ctx;

	bs->subscriptions = list_alloc();
	if (!bs->subscriptions) {
		log_set("Could not create list for subscriptions: %s",
		        log_str());
		goto error_subs;
	}

	bs->callback = cb;
	return bs;

 error_subs:
	log_push();
	cstr_free(bs->name);
	log_pop();
 error_name:
	log_push();
	memory_free(bs);
	log_pop();
 error_alloc:
	return NULL;
}

void
bus_session_free(struct bus_session *bs)
{
	struct bus_subscription *sub;

	while (bs->subscriptions->size > 0) {
		sub = bs->subscriptions->elements[0];
		bus_subscription_free(sub); /* will remove itself from list */
	}
	list_free(bs->subscriptions);

	cstr_free(bs->name);
	memory_free(bs);
}

int
bus_session_join_channel(struct bus_session *bs,
                         struct bus_channel *ch, void *mask)
{
	struct bus_subscription *sub;

	sub = list_find(bs->subscriptions,
	                ch, (list_eqfunc) bus_subscription_has_channel);
	if (sub) {
		log_set("Bus session is already in channel");
		return -1;
	}

	/* subscription attaches itself to session and channel */
	sub = bus_subscription_alloc(bs, ch, mask);
	if (!sub) {
		log_set("Could not create subscription: %s", log_str());
		goto error_sub;
	}

	return 0;

 error_sub:
	return -1;
}

void
bus_session_leave_channel(struct bus_session *bs, struct bus_channel *ch)
{
	struct bus_subscription *sub;

	sub = list_find(bs->subscriptions,
	                ch, (list_eqfunc) bus_subscription_has_channel);
	if (!sub) {
		BUG("Attempt to remove event bus session '%s' from unjoined channel '%s'",
		    bs->name, ch->name);
		return;
	}

	/* subscription detaches itself from session and channel */
	bus_subscription_free(sub);
}
