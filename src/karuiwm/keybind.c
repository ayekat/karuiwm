#include <karuiwm/keybind.h>
#include <karuiwm/action_call.h>
#include <karuiwm/keymap.h>
#include <karui/log.h>
#include <karui/memory.h>
#include <assert.h>

struct keybind *
keybind_alloc(struct key const *k, struct action_call *ac)
{
	struct keybind *kb;

	kb = memory_alloc(sizeof(struct keybind), "key binding structure");
	if (!kb)
		return NULL;

	kb->key = *k;
	kb->actioncall = ac;
	kb->keymap = NULL;

	return kb;
}

void
keybind_free(struct keybind *kb)
{
	if (kb->keymap)
		FATAL_BUG("Attempt to free keybind %u+%s -> '%s' while still part of keymap '%s'",
		          kb->key.mod, XKeysymToString(kb->key.sym),
		          kb->actioncall->name, kb->keymap->name);

	action_call_free(kb->actioncall);
	memory_free(kb);
}

int
keybind_execute(struct keybind const *kb)
{
	return action_call_invoke(kb->actioncall, NULL, NULL);
}

bool
keybind_has_action(struct keybind const *kb, struct action const *a)
{
	return kb->actioncall->action == a;
}

bool
keybind_has_action_with_name(struct keybind const *kb, char const *name)
{
	return action_has_name(kb->actioncall->action, name);
}

bool
keybind_has_key(struct keybind const *kb, struct key const *k)
{
	return key_equals(&kb->key, k);
}
