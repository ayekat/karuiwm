<HTML>
<HEAD>
<TITLE>ICCCM - Revision History</TITLE>
</HEAD>

<BODY>
<H1><A NAME="s-A">A.</A> Revision History</H1>
This appendix describes the revision history of this document and
summarizes the incompatibilities between this and earlier versions.

<H2><A NAME="s-A.1">A.1.</A> The X11R2 Draft</H2>
The February 25, 1988 draft that was distributed as part of X Version 11,
Release 2 was clearly labeled as such,
and many areas were explicitly labeled as liable to change.
Nevertheless, in the revision work since then,
we have been very careful not to introduce gratuitous incompatibility.
As far as possible,
we have tried to ensure that clients obeying the conventions
in the X11R2 draft would still work.

<H2><A NAME="s-A.2">A.2.</A> The July 27, 1988 Draft</H2>
The Consortium review was based on a draft dated July 27, 1988.  This draft
included several areas in which incompatibilities with the X11R2 draft were
necessary:
<UL>
<LI> The use of property 
<B>None</B>
in 
<B>ConvertSelection</B>
requests is no longer allowed.
Owners that receive them are free to use the target atom as the property 
to respond with,
which will work in most cases.
<LI> The protocol for INCREMENTAL type properties as selection replies has changed,
and the name has been changed to INCR.
Selection requestors are free to implement the earlier protocol 
if they receive properties of type INCREMENTAL.
<LI> The protocol for INDIRECT type properties as selection replies has changed,
and the name has been changed to MULTIPLE.
Selection requestors are free to implement the earlier protocol 
if they receive properties of type INDIRECT.
<LI> The protocol for the special CLIPBOARD client has changed.
The earlier protocol is subject to race conditions and should not be used.
<LI> The set of state values in WM_HINTS.initial_state has been reduced,
but the values that are still valid are unchanged.
Window managers should treat the other values sensibly.
<LI> The methods an application uses to change the state of its top-level window
have changed but in such a way that cases that used to work will still work.
<LI> The x, y, width, and height fields have been removed from the WM_NORMAL_HINTS
property and replaced by pad fields.
Values set into these fields will be ignored.
The position and size of the window should be set by setting the appropriate
window attributes.
<LI> A pair of base fields and a win_gravity field have been added 
to the WM_NORMAL_HINTS property.
Window managers will assume values for these fields if the client
sets a short property.

</UL>
<H2><A NAME="s-A.3">A.3.</A> The Public Review Drafts</H2>
The Consortium review resulted in several incompatible changes.  These
changes were included in drafts that were distributed for public review
during the first half of 1989.
<UL>
<LI> The messages field of the WM_HINTS property was found to be unwieldy
and difficult to evolve.
It has been replaced by the WM_PROTOCOLS property,
but clients that use the earlier mechanism can be detected 
because they set the messages bit in the flags field of the WM_HINTS property,
and window managers can provide a backwards-compatibility mode.
<LI> The mechanism described in the earlier draft by which clients installed
their own subwindow colormaps could not be made to work reliably
and mandated some features of the look and feel.
It has been replaced by the WM_COLORMAP_WINDOWS property.
Clients that use the earlier mechanism can be detected by the WM_COLORMAPS
property they set on their top-level window,
but providing a reliable backwards compatibility mode is not possible.
<LI> The recommendations for window manager treatment of top-level window borders
have been changed as those in the earlier draft produced problems
with Visibility events.
For nonwindow manager clients,
there is no incompatibility.
<LI> The pseudoroot facility in the earlier draft has been removed.
Although it has been successfully implemented,
it turns out to be inadequate to support the uses envisaged.
An extension will be required to support these uses fully,
and it was felt that the maximum freedom should be left to the designers
of the extension.
In general,
the previous mechanism was invisible to clients and no incompatibility
should result.
<LI> The addition of the WM_DELETE_WINDOW protocol (which prevents the danger
that multi-window clients may be terminated unexpectedly)
has meant some changes in the WM_SAVE_YOURSELF protocol,
to ensure that the two protocols are orthogonal.
Clients using the earlier protocol can be detected (see WM_PROTOCOLS above)
and supported in a backwards-compatibility mode.
<LI> The conventions in Section 1<A HREF="sec-4.html#s-4.3.1">4.3.1</A>. of <I>Xlib - C Language X Interface</I>
regarding properties of type RGB_COLOR_MAP have been changed,  
but clients that use the earlier conventions can be detected 
because their properties are four bytes shorter.
These clients will work correctly if the server supports only a single Visual
or if they use only the Visual of the root.
These are the only cases in which they would have worked, anyway.

</UL>
<H2><A NAME="s-A.4">A.4.</A> Version 1.0, July 1989</H2>
The public review resulted in a set of mostly editorial changes.  The
changes in version <A HREF="sec-1.html#s-1.0">1.0</A> that introduced some degree of incompatibility with
the earlier drafts are:
<UL>
<LI> A new section (<A HREF="sec-6.html#s-6.3">6.3</A>) was added covering the window manager's
use of Grabs.
The restrictions it imposes should affect only window managers.
<LI> The TARGETS selection target has been clarified,
and it may be necessary for clients to add some entries to their replies.
<LI> A selection owner using INCR transfer should no longer replace targets in
a MULTIPLE property with the atom INCR.
<LI> The contents of the 
<B>ClientMessage</B>
event sent by a client to iconify itself has been clarified,
but there should be no incompatibility because the earlier contents
would not in fact have worked.
<LI> The border-width in synthetic 
<B>ConfigureNotify</B>
events is now specified,
but this should not cause any incompatibility.
<LI> Clients are now asked to set a border-width on all 
<B>ConfigureWindow</B>
requests.
<LI> Window manager properties on icon windows now will be ignored,
but there should be no incompatibility 
because there was no specification that they be obeyed previously.
<LI> The ordering of real and synthetic 
<B>ConfigureNotify</B>
events is now specified,
but any incompatibility should affect only window managers.
<LI> The semantics of WM_SAVE_YOURSELF have been clarified and restricted to
be a checkpoint operation only.
Clients that were using it as part of a shutdown sequence may need to
be modified,
especially if they were interacting with the user during the shutdown.
<LI> A kill_id field has been added to RGB_COLOR_MAP properties.
Clients using earlier conventions can be detected by the size of their
RGB_COLOR_MAP properties,
and the cases that would have worked will still work.

</UL>
<H2><A NAME="s-A.5">A.5.</A> Version 1.1</H2>
Version <A HREF="sec-1.html#s-1.1">1.1</A> was released with X11R5 in September, 1991.  In addition to some
minor editorial changes, there were a few semantic changes since Version
<A HREF="sec-1.html#s-1.0">1.0</A>:
<UL>
<LI> The section on Device Color Characterization was added.
<LI> The meaning of the NULL property type was clarified.
<LI> Appropriate references to Compound Text were added.

</UL>
<H2><A NAME="s-A.6">A.6.</A> Public Review Draft, December 1993</H2>
The following changes have been made in preparing the public review draft
for Version <A HREF="sec-2.html#s-2.0">2.0</A>.
<UL>
<LI> [P01] Addition of advice to clients on how to keep track of a top-level
window's absolute position on the screen.
<LI> [P03] A technique for clients to detect when it is safe to re-use a
top-level window has been added.
<LI> [P06] <A HREF="sec-4.html#s-4.1.8">Section 4.1.8</A>, on colormaps, has been rewritten.  A new feature that
allows clients to install their own colormaps has also been added.
<LI> [P08] The LENGTH target has been deprecated.
<LI> [P11] The manager selections facility was added.
<LI> [P17] The definition of the aspect ratio fields of the WM_NORMAL_HINTS
property has been changed to include the base size.
<LI> [P19]
<B>StaticGravity</B>
has been added to the list of values allowed for the win_gravity field of
the WM_HINTS property.  The meaning of the
<B>CenterGravity</B>
value has been clarified.
<LI> [P20] A means for clients to query the ICCCM compliance level of the window
manager has been added.
<LI> [P22] The definition of the MULTIPLE selection target has been clarified.
<LI> [P25] A definition of &quot;top-level window&quot; has been added.  The WM_STATE
property has been defined and exposed to clients.
<LI> [P26] The definition of window states has been clarified and the wording
regarding window state changes has been made more consistent.
<LI> [P27] Clarified the rules governing when window managers are required to send
synthetic
<B>ConfigureNotify</B>
events.
<LI> [P28] Added a recommended technique for setting the input focus to a window
as soon as it is mapped.
<LI> [P29] The required lifetime of resource IDs named in window manager
properties has been specified.
<LI> [P30] Advice for dealing with keystrokes and override-redirect windows has
been added.
<LI> [P31] A statement on the ownership of resources transferred through the
selection mechanism has been added.
<LI> [P32] The definition of the CLIENT_WINDOW target has been clarified.
<LI> [P33] A rule about requiring the selection owner to re-acquire the
selection under certain circumstances has been added.
<LI> [P42] Added several new selection targets.
<LI> [P44] Ambiguous wording regarding the withdrawal of top-level windows
has been removed.
<LI> [P45] A facility for requestors to pass parameters during a selection
request has been added.
<LI> [P49] A convention on discrimated names has been added.
<LI> [P57] The C_STRING property type was added.
<LI> [P62] An ordering requirement on processing selection requests was added.
<LI> [P63] The
<B>VisibleHint</B>
flag was added.
<LI> [P64] The session management section has been updated to align with the new
session management protocol.  The old session management conventions have
been moved to Appendix C.
<LI> References to the never-forthcoming <I>Window and Session Manager
Conventions Manual</I> have been removed.
<LI> Information on the X Registry and references to the session management and
ICE documents have been added.
<LI> Numerous editorial and typographical improvements have been made.

</UL>
<H2><A NAME="s-A.7">A.7.</A> Version 2.0, April 1994</H2>
The following changes have been made in preparation for releasing
the final edition of Version <A HREF="sec-2.html#s-2.0">2.0</A> with X11R6.
<UL>
<LI> The PIXMAP selection target has been revised to return a property of type
PIXMAP instead of type DRAWABLE.
<LI> The session management section has been revised slightly to correspond with
the changes to the <I>X Session Management Protocol</I>.
<LI> Window managers are now prohibited from placing
<B>CurrentTime</B>
in the timestamp field of WM_TAKE_FOCUS messages.
<LI> In the WM_HINTS property, the
<B>VisibleHint</B>
flag has been renamed to
<B>UrgencyHint .</B>
Its semantics have also been defined more thoroughly.
<LI> Additional editorial and typographical changes have been made.
</UL>
<HR>
<A HREF="sec-8.html"><IMG SRC="/images/left.gif" WIDTH=31 HEIGHT=31 ALT="<"></A><A HREF="./"><IMG SRC="/images/up.gif" WIDTH=31 HEIGHT=31 ALT="^"></A><A HREF="sec-B.html"><IMG SRC="/images/right.gif" WIDTH=31 HEIGHT=31 ALT=">"></A>
<P><HR><ADDRESS><A HREF="http://tronche.com/">Christophe Tronche</A>, <A HREF="mailto:ch.tronche@computer.org">ch.tronche@computer.org</A></ADDRESS>
</BODY>
</HTML>
