<HTML>
<HEAD>
<TITLE>ICCCM - Device Color Characterization</TITLE>
</HEAD>

<BODY>
<H1><A NAME="s-7">7.</A> Device Color Characterization</H1>

The X protocol provides explicit RGB values,
which are used to directly drive a monitor, and color names.  RGB values
provide a mechanism for accessing the full capabilities of the display
device, but at the expense of having the color perceived by the user remain
unknowable through the protocol.  Color names were originally designed to
provide access to a device-independent color database by having the server
vendor tune the definitions of the colors in that textual database.
Unfortunately, this still does not provide the client any way of using
an existing device-independent color, nor for the client to get
device-independent color information back about colors that it has selected.
<P>
Furthermore, the client must be able to discover which set of colors are
displayable by the device (the device gamut), both to allow colors to be
intelligently modified to fit within the device capabilities (gamut
compression) and to enable the user interface to display a representation of
the reachable color space to the user (gamut display).
<P>
Therefore, a system is needed that will provide full access to
device-independent color spaces for X clients.  This system should use a
standard mechanism for naming the colors, be able to provide names for
existing colors, and provide means by which unreachable colors can be
modified to fall within the device gamut.
<P>
We are fortunate in this area to have a seminal work, the 1931 CIE color
standard, which is nearly universally agreed upon as adequate for describing
colors on CRT devices.  This standard uses a tri-stimulus model called CIE
XYZ in which each perceivable color is specified as a triplet of numbers.
Other appropriate device-independent color models do exist, but most of them
are directly traceable back to this original work.
<P>
X device color characterization
provides device-independent color spaces to X clients.  It does this by
providing the barest possible amount of information to the client that
allows the client to construct a mapping between CIE XYZ and the regular X
RGB color descriptions.
<P>
Device color characterization is defined by
the name and contents of two window properties that,
together, permit converting between CIE XYZ space and
linear RGB device space (such as standard CRTs).
Linear RGB devices require just two
pieces of information to completely characterize them:
<UL>
<LI> 
A <I>3 &#215; 3</I> matrix <I>M</I> and its inverse <I>M<SUP>-1</SUP></I>, which convert between
XYZ and RGB intensity (<I>RGB<SUB>intensity</SUB></I>):
<P ALIGN=center><I>RGB<SUB>intensity</SUB> = M &#215; XYZ</I><BR>
<I>XYZ = M<SUP>-1</SUP> &#215; RGB<SUB>intensity</SUB></I>
<LI> A way of mapping between RGB intensity and RGB protocol value.  XDCCC
supports three mechanisms which will be outlined below.
</UL>
If other device types are eventually necessary, additional
properties will be required to describe them.

<H2><A NAME="s-7.1">7.1.</A> XYZ <-> RGB Conversion Matrices</H2>
Because of the limited dynamic range of both XYZ and RGB intensity,
these matrices will be encoded using a fixed-point representation of a
32-bit two's complement number scaled by <I>2<SUP>27</SUP></I>, giving a range of <I>-16</I> to
<I>16 - &epsilon;</I>, where <I>&epsilon; = 2 <SUP>-27</SUP></I>.
<P>
These matrices will be packed into an 18-element list of 32-bit values,
XYZ -> RGB matrix first, in row major order and stored in the
XDCCC_LINEAR_RGB_MATRICES properties (format = 32) on the root window of
each screen, using values appropriate for that screen.
<P>
This will be encoded as shown in the following table:
<TABLE>
<CAPTION>XDCCC_LINEAR_RGB_MATRICES property contents</CAPTION>
<TR><TH COLSPAN=3><HR>
<TR><TH ALIGN=left>Field	<TH ALIGN=left>Type	<TH ALIGN=left>Comments
<TR><TH COLSPAN=3><HR>
<TR><TD><I>M<SUB>0,0</SUB></I>	<TD>INT32	<TD>Interpreted as a fixed-point number <I>-16 &lt;= x < 16</I>
<TR><TD><I>M<SUB>0,1</SUB></I>	<TD>INT32	<TD>
<TR><TD>...	<TD>	<TD>
<TR><TD><I>M<SUB>3,3</SUB></I>	<TD>INT32	<TD>
<TR><TD><I>M<SUP>-1</SUP><SUB>0,0</SUB></I>	<TD>INT32	<TD>
<TR><TD><I>M<SUP>-1</SUP><SUB>0,1</SUB></I>	<TD>INT32	<TD>
<TR><TD>...	<TD>	<TD>
<TR><TD><I>M<SUP>-1</SUP><SUB>3,3</SUB></I>	<TD>INT32	<TD>
<TR><TH COLSPAN=3><HR>
</TABLE>


<H2><A NAME="s-7.2">7.2.</A> Intensity <-> RGB Value Conversion</H2>
XDCCC provides two representations for describing the conversion
between RGB intensity and the actual X protocol RGB values:
<OL>
<LI> RGB value/RGB intensity level pairs
<LI> RGB intensity ramp
</OL>
<P>
In both cases, the relevant data will be stored in the
XDCCC_LINEAR_RGB_CORRECTION properties on the root window of each screen,
using values appropriate for that screen, in whatever format provides
adequate resolution.  Each property can consist of multiple entries
concatenated together, if different visuals for the screen require different
conversion data.  An entry with a VisualID of 0 specifies data for all
visuals of the screen that are not otherwise explicitly listed.
<P>
The first representation is an array of RGB value/intensity level pairs, with
the RGB values in strictly increasing order.  When converting, the client must
linearly interpolate between adjacent entries in the table to compute the
desired value.  This allows the server to perform gamma correction
itself and encode that fact in a short two-element correction table.  The
intensity will be encoded as an unsigned number to be interpreted as a value
between 0 and 1 (inclusive).  The precision of this value will depend on the
format of the property in which it is stored (8, 16 or 32 bits).  For 16-bit
and 32-bit formats, the RGB value will simply be the value stored in the
property.  When stored in 8-bit format, the RGB value can be computed from
the value in the property by:
<I><TABLE>
<TR><TD ROWSPAN=3 VALIGN=center>RGB<SUB>value</SUB> =	<TD>Property Value &#215; 65535
<TR>						<TD><HR>
<TR>						<TD ALIGN=center>255
</TABLE></I>

<P>
Because the three electron guns in the device may not be exactly alike in
response characteristics, it is necessary to allow for three separate
tables, one each for red, green, and blue.  Therefore, each table will be
preceded by the number of entries in that table, and the set of tables will be
preceded by the number of tables.
When three tables are provided, they will be in red, green, blue order.
<P>
This will be encoded as shown in the following table:
<TABLE>
<CAPTION>XDCCC_LINEAR_RGB_CORRECTION Property Contents for Type 0 Correction</CAPTION>
<TR><TH COLSPAN=3><HR>
<TR><TH ALIGN=left>Field	<TH ALIGN=left>Type	<TH ALIGN=left>Comments
<TR><TH COLSPAN=3><HR>
<TR><TD>VisualID0	<TD>CARD	<TD>Most-significant portion of VisualID
<TR><TD>VisualID1	<TD>CARD	<TD>Exists if and only if the property format is 8
<TR><TD>VisualID2	<TD>CARD	<TD>Exists if and only if the property format is 8
<TR><TD>VisualID3	<TD>CARD	<TD>Least-significant portion, exists if and only if the property format is 8 or 16
<TR><TD>type	<TD>CARD	<TD>0 for this type of correction
<TR><TD>count	<TD>CARD	<TD>Number of tables following (either 1 or 3)
<TR><TD>length	<TD>CARD	<TD>Number of pairs &endash; 1 following in this table
<TR><TD>value	<TD>CARD	<TD>X Protocol RGB value
<TR><TD>intensity	<TD>CARD	<TD>Interpret as a number <I>0 &lt;= intensity &lt;= 1</I>
<TR><TD>...	<TD>...	<TD>Total of <I>length+1</I> pairs of value/intensity values
<TR><TD>lengthg	<TD>CARD	<TD>Number of pairs &endash; 1 following in this table (if and only if <I>count</I> is 3)
<TR><TD>value	<TD>CARD	<TD>X Protocol RGB value
<TR><TD>intensity	<TD>CARD	<TD>Interpret as a number <I>0 &lt;= intensity &lt;= 1</I>
<TR><TD>...	<TD>...	<TD>Total of <I>lengthg+1</I> pairs of value/intensity values
<TR><TD>lengthb	<TD>CARD	<TD>Number of pairs &endash; 1 following in this table (if and only if <I>count</I> is 3)
<TR><TD>value	<TD>CARD	<TD>X Protocol RGB value
<TR><TD>intensity	<TD>CARD	<TD>Interpret as a number <I>0 &lt;= intensity &lt;= 1</I>
<TR><TD>...	<TD>...	<TD>Total of <I>lengthb+1</I> pairs of value/intensity values
<TR><TH COLSPAN=3><HR>
</TABLE>

<P>
The VisualID is stored in 4, 2, or 1 pieces, depending on whether
the property format is 8, 16, or 32, respectively.  The VisualID is always
stored most-significant piece first.
Note that the length fields are stored as one less than the actual length,
so 256 entries can be stored in format 8.
<P>
The second representation is a simple array of intensities for a linear subset
of RGB values.  The expected size of this table is the bits-per-rgb-value of
the screen, but it can be any length.  This is similar to the first mechanism,
except that the RGB value numbers are implicitly defined by the index in the
array (indices start at 0):
<I><TABLE>
<TR><TD ROWSPAN=3 VALIGN=center>RGB<SUB>value</SUB> =	<TD>Array Index &#215; 65535
<TR>						<TD><HR>
<TR>						<TD ALIGN=center>Array Size - 1
</TABLE></I>

When converting, the client may linearly interpolate between entries in this
table.  The intensity values will be encoded just as in the first
representation.
<P>
This will be encoded as shown in the following table:
<TABLE>
<CAPTION>XDCCC_LINEAR_RGB_CORRECTION Property Contents for Type 1 Correction</CAPTION>
<TR><TH COLSPAN=3><HR>
<TR><TH ALIGN=left>Field	<TH ALIGN=left>Type	<TH ALIGN=left>Comments
<TR><TH COLSPAN=3><HR>
<TR><TD>VisualID0	<TD>CARD	<TD>Most-significant portion of VisualID
<TR><TD>VisualID1	<TD>CARD	<TD>Exists if and only if the property format is 8
<TR><TD>VisualID2	<TD>CARD	<TD>Exists if and only if the property format is 8
<TR><TD>VisualID3	<TD>CARD	<TD>Least-significant portion, exists if and only if the property format is 8 or 16
<TR><TD>type	<TD>CARD	<TD>1 for this type of correction
<TR><TD>count	<TD>CARD	<TD>Number of tables following (either 1 or 3)
<TR><TD>length	<TD>CARD	<TD>Number of elements &endash; 1 following in this table
<TR><TD>intensity	<TD>CARD	<TD>Interpret as a number <I>0 &lt;= intensity &lt;= 1</I>
<TR><TD>...	<TD>...	<TD>Total of <I>length+1</I> intensity elements
<TR><TD>lengthg	<TD>CARD	<TD>Number of elements &endash; 1 following in this table (if and only if <I>count</I> is 3)
<TR><TD>intensity	<TD>CARD	<TD>Interpret as a number <I>0 &lt;= intensity &lt;= 1</I>
<TR><TD>...	<TD>...	<TD>Total of <I>lengthg+1</I> intensity elements
<TR><TD>lengthb	<TD>CARD	<TD>Number of elements &endash; 1 following in this table (if and only if <I>count</I> is 3)
<TR><TD>intensity	<TD>CARD	<TD>Interpret as a number <I>0 &lt;= intensity &lt;= 1</I>
<TR><TD>...	<TD>...	<TD>Total of <I>lengthb+1</I> intensity elements
<TR><TH COLSPAN=3><HR>
</TABLE>


<HR>
<A HREF="sec-6.html"><IMG SRC="/images/left.gif" WIDTH=31 HEIGHT=31 ALT="<"></A><A HREF="./"><IMG SRC="/images/up.gif" WIDTH=31 HEIGHT=31 ALT="^"></A><A HREF="sec-8.html"><IMG SRC="/images/right.gif" WIDTH=31 HEIGHT=31 ALT=">"></A>
<P><HR><ADDRESS><A HREF="http://tronche.com/">Christophe Tronche</A>, <A HREF="mailto:ch.tronche@computer.org">ch.tronche@computer.org</A></ADDRESS>
</BODY>
</HTML>
